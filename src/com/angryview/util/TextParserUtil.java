package com.angryview.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Properties;




public class TextParserUtil {  
	   
    private static String defaultPropertiesPath = null;
    
    private static TextParserUtil config = new TextParserUtil();
	
    /**
     * 생성자
     */
    private TextParserUtil(){
		URL url =  TextParserUtil.class.getResource("/properties/textparser.properties"); 
       	defaultPropertiesPath = url.getPath();
    } 
	  
    /**
     * <PRE>
     * instance
     * </PRE>
     * @return TextParserUtil
     */
    public static TextParserUtil getInstance(){
    	if(config == null){
    		synchronized(TextParserUtil.class ){
    			if(config == null)
    				config = new TextParserUtil();
    		}
    	}
    	return config;
    }
	 
    /**
     *  property 의 key에 mapping 되는 값을 리턴.
     * @param key
     * @return String
     */
    public static String getKey(String key) {    
   
        String value = null;    
        InputStream is = null;    
        Properties p = null;    
        try {    
        	is = new FileInputStream(defaultPropertiesPath);
            p = new Properties();    
            p.load(is);    
            value = p.getProperty(key);    
        } catch (FileNotFoundException e) {    
            e.printStackTrace();    
        } catch (IOException e) {    
            e.printStackTrace();    
        } finally {    
            try {    
                is.close();    
            } catch (IOException e) {    
            }    
        }    
        return value;    
    }
    
    /**
     * @return defaultPropertiesPath
     */
    public static String getDefaultPropertiesPath() {    
        return defaultPropertiesPath;    
    }   
    
    /**
     * @param defaultPropertiesPath
     */
    public static void setDefaultPropertiesPath(String defaultPropertiesPath) {    
    	TextParserUtil.defaultPropertiesPath = defaultPropertiesPath;    
    }
    
    
	//byte 배열 startIndex 부터 길이만큼 파싱 
	public static  String getString (byte textb[], int startIdx, int len) throws Exception{
		byte value[]  = new byte[len];
		try{
			for(int i = 0; i<len; i++) {
				value[i] = textb[startIdx + i];
			}
		}catch (Exception e ) {
			value = "".getBytes();
		}
		return new String (value, "euc-kr");
	
	}    
    
	//title명 배열 리턴
	public String[] getTitleList(String titleNm) {

        String title[] = null;
        try{
        	String titleStr = getKey(titleNm);
        	title = titleStr != null ? getKey(titleNm).split(",") : null;
        }catch(Exception e) {
        	e.printStackTrace();
        }
		return title;
	}	
	
	//position 배열 리턴
	public int[] getPosList(String posNm) {
        int postionList[] = null;
        try{
        	String postStr = getKey(posNm);
        	String posList[] = postStr != null ? getKey(posNm).split(",") : null;
        	if(posList != null) {
        	postionList = new int[posList.length];
	        	for(int i = 0; i<posList.length; i++) {
	        		postionList[i] = Integer.parseInt(posList[i]);
	        	}
        	}
        	
        }catch(Exception e) {
        	e.printStackTrace();
        }
		return postionList;
	}		
	
}  
