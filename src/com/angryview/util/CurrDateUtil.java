package com.angryview.util;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CurrDateUtil
{

	private String releaseVersion = "1.0";

    public CurrDateUtil()
    {
    }

    public static String getCurrentDateString()
    {
        return getCurrentDateString("yyyyMMdd");
    }

    public static String getCurrentTimeString()
    {
        return getCurrentDateString("HHmmss");
    }
    public static String getCurrentTimeMsString()
    {
        return getCurrentDateString("HHmmssms");
    }
    public static String getCurrentTime()
    {
        return getCurrentDateString("yyyyMMddHHmmssSSS");
    }

    public static String getCurrentTimeFormat()
    {
        return getCurrentDateString("yyyy-MM-dd HH:mm:ss.SSS");
    }

    public static String getCurrentDateString(String pattern)
    {
        return convertToString(getCurrentTimeStamp(), pattern);
    }

    public static String convertFormat(String dateData)
    {
        if(dateData == null)
            return "";
        else
            return convertFormat(dateData, "yyyy/MM/dd");
    }

    public static String convertFormat(String dateData, String format)
    {
        if(dateData == null)
            return "";
        else
            return convertToString(convertToTimestamp(dateData), format);
    }
    public static String convertFormatExchange(String dateData, String format)
    {
        if(dateData == null)
            return "";
        else
            return convertToString(convertToTimestampExchange(dateData), format);
    }
    public static Timestamp getCurrentTimeStamp()
    {
        try
        {
            Calendar cal = new GregorianCalendar();
            Timestamp result = new Timestamp(cal.getTime().getTime());
            return result;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static String convertToString(Timestamp dateData)
    {
        return convertToString(dateData, "yyyy-MM-dd");
    }

    public static String convertToString(Timestamp dateData, String pattern)
    {
        return convertToString(dateData, pattern, Locale.KOREA);
    }

    public static String convertToString(Timestamp dateData, String pattern, Locale locale)
    {
        try
        {
            if(dateData == null)
            {
                return "";
            }
            else
            {
                SimpleDateFormat formatter = new SimpleDateFormat(pattern, locale);
                return formatter.format(dateData);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static Timestamp convertToTimestamp(String dateData)
    {
        try
        {
            if(dateData == null)
                return null;
            if(dateData.trim().equals(""))
                return null;
            int dateObjLength = dateData.length();
            String yearString = "2002";
            String monthString = "01";
            String dayString = "01";
            if(dateObjLength >= 4)
                yearString = dateData.substring(0, 4);
            if(dateObjLength >= 6)
                monthString = dateData.substring(4, 6);
            if(dateObjLength >= 8)
                dayString = dateData.substring(6, 8);
            int year = Integer.parseInt(yearString);
            int month = Integer.parseInt(monthString) - 1;
            int day = Integer.parseInt(dayString);
            Calendar cal = new GregorianCalendar();
            cal.set(year, month, day);
            return new Timestamp(cal.getTime().getTime());
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	return null;
        }
    }
    public static Timestamp convertToTimestampExchange(String dateData)
    {
//    	2006-09-07T02:00:00.000Z ��ȯ�ؾ� �� date data
    	try
    	{
//    		dateData = "2006-09-07T02:11:00.000Z";

    		if(dateData == null)
    			return null;
    		if(dateData.trim().equals(""))
    			return null;
    		int dateObjLength = dateData.length();
    		String yearString = "2002";
    		String monthString = "01";
    		String dayString = "01";
    		String hourString = "00";
    		String minString = "00";

    		if(dateObjLength >= 4)
    			yearString = dateData.substring(0, 4);
    		if(dateObjLength >= 7)
    			monthString = dateData.substring(5, 7);
    		if(dateObjLength >= 10)
    			dayString = dateData.substring(8, 10);
    		if(dateObjLength >= 13)
    			hourString = dateData.substring(11, 13);
    		if(dateObjLength >= 16)
    			minString = dateData.substring(14, 16);

    		int year = Integer.parseInt(yearString);
    		int month = Integer.parseInt(monthString) - 1;
    		int day = Integer.parseInt(dayString);
    		int hour = Integer.parseInt(hourString);
    		int min = Integer.parseInt(minString);
    		Calendar cal = new GregorianCalendar();
//    		cal.set(year, month, day);
    		cal.set(year, month, day, hour, min);
    		return new Timestamp(cal.getTime().getTime());
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    		return null;
    	}
    }
    public static Timestamp convertToTimestampHMS(String dateData)
    {
        try
        {
            if(dateData == null)
                return null;
            if(dateData.trim().equals(""))
            {
                return null;
            }
            else
            {
                String yearString = dateData.substring(0, 4);
                String monthString = dateData.substring(4, 6);
                String dayString = dateData.substring(6, 8);
                String hourString = dateData.substring(8, 10);
                String minString = dateData.substring(10, 12);
                String secString = dateData.substring(12, 14);
                int year = Integer.parseInt(yearString);
                int month = Integer.parseInt(monthString) - 1;
                int day = Integer.parseInt(dayString);
                int hour = Integer.parseInt(hourString);
                int min = Integer.parseInt(minString);
                int sec = Integer.parseInt(secString);
                Calendar cal = new GregorianCalendar();
                cal.set(year, month, day, hour, min, sec);
                return new Timestamp(cal.getTime().getTime());
            }
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	return null;
        }
    }

    /**
     * 주어진 날짜 스트링 s를 Date 타입으로 변환하여 리턴 하거나 패턴이 yyyyMMdd 가 아니면 Exception
     * @param s
     * @return
     * @throws ParseException
     */
    public static Date check(String s)
        throws ParseException
    {
        return check(s, "yyyyMMdd");
    }

    public static Date check(String s, String format) throws ParseException
    {
        if(s == null)
            throw new ParseException("date string to check is null", 0);
        if(format == null)
            throw new ParseException("format string to check date is null", 0);
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.KOREA);
       
        Date date = null;
        try
        {
            date = formatter.parse(s);
        }
        catch(ParseException e)
        {
            throw new ParseException(" wrong date:\"" + s + "\" with format \"" + format + "\"", 0);
        }
        if(!formatter.format(date).equals(s))
            throw new ParseException("Out of bound date:\"" + s + "\" with format \"" + format + "\"", 0);
        else
            return date;
    }

    
    
    
    /**
     * 주어진 파라미터 s가 패턴 yyyyMMdd 형태인지 확인
     * @param s
     * @return
     */
    public static boolean isValid(String s)
    {
        return isValid(s, "yyyyMMdd");
    }

    
    
    
    
    public static boolean isValid(String s, String format)
    {
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.KOREA);
            Date date = null;
            try
            {
                date = formatter.parse(s);
            }
            catch(ParseException e)
            {
                return false;
            }
            return formatter.format(date).equals(s);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    
    
    
    /**
     * 주어진 문자열이 무슨 요일인지를 판별 
     * 1: 일요일  2: 월요일 3: 화요일 4: 수용일 5: 목요일  6: 금요일  7: 토요일
     * @param s
     * @return
     */
    public static int whichDay(String s)
    {
        return whichDay(s, "yyyyMMdd");
    }

    public static int whichDay(String s, String format)
    {
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.KOREA);
            Date date = check(s, format);
            Calendar calendar = formatter.getCalendar();
            calendar.setTime(date);
            return calendar.get(7);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return -10000000;
        }
    }

    
    /**
     * 주어진 파라미터 사이의 날짜 수를 리턴
     * @param from
     * @param to
     * @return
     */
    public static int daysBetween(String from, String to)
    {
        return daysBetween(from, to, "yyyyMMdd");
    }

    public static int daysBetween(String from, String to, String format)
    {
        try
        {
            Date d1 = check(from, format);
            Date d2 = check(to, format);
            long duration = d2.getTime() - d1.getTime();
            return (int)(duration / 0x5265c00L);
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	return -1000000;
        }
    }


    public static int yearsBetween(String from, String to)
    {
        return yearsBetween(from, to, "yyyyMMdd");
    }

    public static int yearsBetween(String from, String to, String format)
    {
        return daysBetween(from, to, format) / 365;
    }

    
    
    
    /**
     * 주어진 날짜 문자열(yyyyMMdd) s에 일수(day) 를 더한 날짜의 문자열을 리턴한다.
     * 
     * @param s
     * @param day
     * @return
     */
    public static String addDays(String s, int day)
    {
        return addDays(s, day, "yyyyMMdd");
    }


    
    
    public static String addDays(String s, int day, String format)
    {
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.KOREA);
            Date date = check(s, format);
            date.setTime(date.getTime() + (long)day * 1000L * 60L * 60L * 24L);
            return formatter.format(date);
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	return null;
        }
    }

    public static String addMonths(String s, int month)
    {
        return addMonths(s, month, "yyyyMMdd");
    }

    public static String addMonths(String s, int addMonth, String format)
    {
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.KOREA);
            Date date = check(s, format);
            SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy", Locale.KOREA);
            SimpleDateFormat monthFormat = new SimpleDateFormat("MM", Locale.KOREA);
            SimpleDateFormat dayFormat = new SimpleDateFormat("dd", Locale.KOREA);
            int year = Integer.parseInt(yearFormat.format(date));
            int month = Integer.parseInt(monthFormat.format(date));
            int day = Integer.parseInt(dayFormat.format(date));
            month += addMonth;
            if(addMonth > 0)
                while(month > 12)
                {
                    month -= 12;
                    year++;
                }

            else
                while(month <= 0)
                {
                    month += 12;
                    year--;
                }

            DecimalFormat fourDf = new DecimalFormat("0000");
            DecimalFormat twoDf = new DecimalFormat("00");
            String tempDate = String.valueOf(fourDf.format(year)) + String.valueOf(twoDf.format(month)) + String.valueOf(twoDf.format(day));
            Date targetDate = null;
            try
            {
                targetDate = check(tempDate, "yyyyMMdd");
            }
            catch(ParseException pe)
            {
                day = lastDay(year, month);
                tempDate = String.valueOf(fourDf.format(year)) + String.valueOf(twoDf.format(month)) + String.valueOf(twoDf.format(day));
                targetDate = check(tempDate, "yyyyMMdd");
            }
            return formatter.format(targetDate);
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	return null;
        }
    }

    public static String addYears(String s, int year)
    {
        return addYears(s, year, "yyyyMMdd");
    }

    public static String addYears(String s, int year, String format)
    {
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.KOREA);
            Date date = check(s, format);
            date.setTime(date.getTime() + (long)year * 1000L * 60L * 60L * 24L * 365L);
            return formatter.format(date);
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	return null;
        }
    }

    public static int monthsBetween(String from, String to)
    {
        return monthsBetween(from, to, "yyyyMMdd");
    }

    public static int monthsBetween(String from, String to, String format)
    {
        try
        {
            Date fromDate = check(from, format);
            Date toDate = check(to, format);
            if(fromDate.compareTo(toDate) == 0)
                return 0;
            SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy", Locale.KOREA);
            SimpleDateFormat monthFormat = new SimpleDateFormat("MM", Locale.KOREA);
            SimpleDateFormat dayFormat = new SimpleDateFormat("dd", Locale.KOREA);
            int fromYear = Integer.parseInt(yearFormat.format(fromDate));
            int toYear = Integer.parseInt(yearFormat.format(toDate));
            int fromMonth = Integer.parseInt(monthFormat.format(fromDate));
            int toMonth = Integer.parseInt(monthFormat.format(toDate));
            int fromDay = Integer.parseInt(dayFormat.format(fromDate));
            int toDay = Integer.parseInt(dayFormat.format(toDate));
            int result = 0;
            result += (toYear - fromYear) * 12;
            result += toMonth - fromMonth;
            if(toDay - fromDay > 0)
                result += toDate.compareTo(fromDate);
            return result;
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	return -10000000;
        }
    }

    public static String lastDayOfMonth(String src)
    {
        return lastDayOfMonth(src, "yyyyMMdd");
    }

    public static String lastDayOfMonth(String src, String format)
    {
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.KOREA);
            Date date = check(src, format);
            SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy", Locale.KOREA);
            SimpleDateFormat monthFormat = new SimpleDateFormat("MM", Locale.KOREA);
            int year = Integer.parseInt(yearFormat.format(date));
            int month = Integer.parseInt(monthFormat.format(date));
            int day = lastDay(year, month);
            DecimalFormat fourDf = new DecimalFormat("0000");
            DecimalFormat twoDf = new DecimalFormat("00");
            String tempDate = String.valueOf(fourDf.format(year)) + String.valueOf(twoDf.format(month)) + String.valueOf(twoDf.format(day));
            Date targetDate = check(tempDate, "yyyyMMdd");
            return formatter.format(targetDate);
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	return null;
        }
    }

    private static int lastDay(int year, int month)
        throws ParseException
    {
        int day = 0;
        switch(month)
        {
        case 1: // '\001'
        case 3: // '\003'
        case 5: // '\005'
        case 7: // '\007'
        case 8: // '\b'
        case 10: // '\n'
        case 12: // '\f'
            day = 31;
            break;

        case 2: // '\002'
            if(year % 4 == 0)
            {
                if(year % 100 == 0 && year % 400 != 0)
                    day = 28;
                else
                    day = 29;
            }
            else
            {
                day = 28;
            }
            break;

        case 4: // '\004'
        case 6: // '\006'
        case 9: // '\t'
        case 11: // '\013'
        default:
            day = 30;
            break;

        }
        return day;
    }

    public static String getYearMonthDateString(String datestr)
    {
        String datestring = "";
        if(datestr == null || datestr.trim().equals(""))
            datestring = "";
        else
        if(datestr.trim().length() < 6)
            datestring = datestr;
        else
        if(datestr.length() >= 6)
            datestring = datestr.substring(0, 4) + "-" + datestr.substring(4, 6);
        return datestring;
    }

    /********���� �״��� ���������� ����********/
    public static String CheckDate(String from)
    {
         String[] last_days = {"31","28","31","30","31","30","31","31","30","31","30","31"};
         String month_lastday="";
         int year = Integer.parseInt(from.substring(0, 4));
         String month = from.substring(4,6);

         if (month.equals("02"))
         {
             //���� ����
             if (((year%4 == 0) && (year%100 != 0)) || (year%400 == 0))
                     last_days[1]="29";
             else
                     last_days[1]="28";
         }
         for(int i=1; i<13; i++)
         {
                 if(i==Integer.parseInt(month))
                   month_lastday=last_days[i-1];

          }
         return month_lastday;

     }

   /**
    * 
    * @param date
    * @return
    */
    public static boolean CheckWeekDate(int date)
    {
        String strDay =String.valueOf(date);
    	boolean flag= false;
    	if( !((Integer.parseInt(strDay.substring(6,8)) > Integer.parseInt(CheckDate(strDay))
    		  || strDay.substring(6,8).equals("00")	))
    	   ) {
    		if (!(CurrDateUtil.whichDay(strDay)==1 || CurrDateUtil.whichDay(strDay)==7)) flag=true;
    	}


        return flag;

     }

    //�ָ��� ������ ��¥�� ��¥ ������ �����Ѵ�.
    public static int daysSkipWeekDayBetween(String from, String to)
    {
    	  int restDate =0;
    	try
        {
            int fromNumber = Integer.parseInt(from);
            int toNumber = Integer.parseInt(to);

    		if(fromNumber<toNumber){
	            for(int date=fromNumber+1; date<toNumber+1; date++){
	            	//System.out.println("---"+date+"flag=="+CheckWeekDate(date));
	    			if(CheckWeekDate(date)) restDate++;
	            }
    		}
    		else if(fromNumber>toNumber){
	            for(int date=toNumber; date<fromNumber; date++){
	            	//System.out.println("---"+date+"flag=="+CheckWeekDate(date));
	    			if(CheckWeekDate(date)) restDate--;
	            }
    		}

        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	return -1000000;
        }
        return restDate;
    }
    
    
    
    
    public static ArrayList getHours(){
    	ArrayList hours = new ArrayList();

    	for(int i = 0 ; i < 24 ; i++){
    		String j = Integer.toString(i).length()==1?"0"+Integer.toString(i):Integer.toString(i);
    		hours.add(j);
    	}
    	return hours;
    }
    
    
    
    
    public static ArrayList getMinuites(){
    	ArrayList minuites = new ArrayList();

    	for(int i = 0 ; i < 60 ; i++){
    		String j = Integer.toString(i).length()==1?"0"+Integer.toString(i):Integer.toString(i);
    		minuites.add(j);
    	}
    	return minuites;
    }

    // <------------------------------------------------------------------

   /**
    * 현재 시간에 시간을 더한다.
    * @param time
    * @return
    */
	public static String addTime(int time) {
		String result = null;

		try{
			String format = "yyyyMMddHH";
			SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.KOREA);

			Date today = new Date(System.currentTimeMillis());

			today.setTime(today.getTime() + (time*3600000));
			result = formatter.format(today);

		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	    * 현재 시간에 분을 더한다.
	    * @param time
	    * @return
	    */
		public static String addMinute(int m) {
			String result = null;

			try{
				String format = "yyyyMMddHHmm";
				SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.KOREA);

				Date today = new Date(System.currentTimeMillis());

				today.setTime(today.getTime() + (m*60*1000));
				result = formatter.format(today);

			}catch(Exception e){
				e.printStackTrace();
			}
			return result;
		}
	

	/**
	 * 주어진 시간에 타임을 더한다.
	 * @param targetTime
	 * @param time
	 * @return
	 */
	public static String addTime(String targetTime, int time) {
		String result = null;

		try{
			String format = "yyyyMMddHH";
			SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.KOREA);

			Date targetDate = formatter.parse(targetTime);

			targetDate.setTime(targetDate.getTime() + (time*3600000));
			result = formatter.format(targetDate);

		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 주어진 기간이 몇시간인지 리턴
	 * @param from
	 * @param to
	 * @return
	 */
	public static int betweenTime(String from, String to){
		int result = 0;

		try{
			String format = "yyyyMMddHH";
			SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.KOREA);

			Date fromDate = formatter.parse(from);
			Date toDate = formatter.parse(to);

			long duration = toDate.getTime() - fromDate.getTime();
			result = (int)(duration / 3600000);

		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}


    public static void main(String args[]){
    	System.out.println(getCurrentTimeString());
    }

    /**
     *  현재 년도 가져오기
     * @return
     */
    public static String getCurrentYear() {
    	Calendar today = Calendar.getInstance();

    	return String.valueOf(today.get(Calendar.YEAR));
    }

    /**
     *  현재 월 가져오기
     * @return
     */
    public static String getCurrentMonth() {
    	Calendar temp=Calendar.getInstance ( );
    	return String.valueOf(temp.get(Calendar.MONTH) + 1);
    }

    
    
    /**
     * 일자/요일 가져오기 (List)
     * @param year
     * @param month
     * @return
     */
    public static List<Map<Object, Object>> getDailyList(int year, int month) {
    	List<Map<Object, Object>> result = new ArrayList<Map<Object, Object>>();
    	Calendar oCalendar = Calendar.getInstance();
    	oCalendar.set(year, month - 1, 1);

    	String[] weeks={"일","월","화","수","목","금","토"};
    	int week = oCalendar.get(Calendar.DAY_OF_WEEK);
    	int maxDay = oCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);

    	for (int i = 1; i <= maxDay; i++) {
    		Map<Object, Object> calendarMap = new HashMap<Object, Object>();
    		if (week <= 7) {
    			calendarMap.put("day", String.valueOf(i));
    			calendarMap.put("week", weeks[week - 1]);

    		} else {
    			week = 1; // 다시 월요일 부터 시작
    			calendarMap.put("day", String.valueOf(i));
    			calendarMap.put("week", weeks[week - 1]);
    		}
    		week += 1;
    		result.add(calendarMap);
    	}

    	return result;
    }
    
    
    /**
     * 현재날짜를 파라미터로 받아 날짜가 월의 몇번째 주인지를 리턴한다.
     * @param currDate
     * @return
     */
    public static String getCurrWeekOrderOfTheMonth(Date currDate){
    	SimpleDateFormat sdf = new SimpleDateFormat("W");
    	return sdf.format(currDate);
    }

    /**
     * 주어진 년,월에 몇번째 주까지 있는지 조회 (해당월의 마지막날에 몇번째 주인지 조회^^)
     * @param py  :파라미터 년도
     * @param pm  : 파라미터 월
     * @return
     */
	public static String getHowManyWeeksAreTheMonth(String py, String pm) {
		StringBuffer sb = new StringBuffer(py);
		sb.append(pm);
		sb.append("01");
		
		int pms = Integer.parseInt(pm) -1;
		String lastDate = lastDayOfMonth(sb.toString()).substring(6);//월의 마지막 일자

//		System.out.println("py = " +Integer.parseInt(py));
//		System.out.println("pm = " + pms);
//		System.out.println("Integer.parseInt(lastDate) : " + Integer.parseInt(lastDate));
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, Integer.parseInt(py));
		cal.set(Calendar.MONTH, pms);
		cal.set(Calendar.DATE, Integer.parseInt(lastDate));


		return getCurrWeekOrderOfTheMonth(cal.getTime());
	}

	


}
