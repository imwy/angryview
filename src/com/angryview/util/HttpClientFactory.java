package com.angryview.util;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;

public class HttpClientFactory {
	private static HttpClient client;
	private static MultiThreadedHttpConnectionManager mgr;

	public synchronized static HttpClient getThreadSafeClient() {
		if (client != null)
			return client;

		mgr = new MultiThreadedHttpConnectionManager();
		HttpConnectionManagerParams params = new HttpConnectionManagerParams();
		params.setMaxTotalConnections(500);
		params.setConnectionTimeout(8000);
		params.setSoTimeout(8000);

		mgr.setParams(params);
		mgr.closeIdleConnections(0);

		client = new HttpClient(mgr);

		return client;
	}

	public static int getConnectionsInPool() {
		return mgr.getConnectionsInPool();
	}

}
