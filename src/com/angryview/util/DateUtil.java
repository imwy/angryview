package com.angryview.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

public class DateUtil {
	
	private Logger log = Logger.getLogger(this.getClass());
	
	
	Date today = new Date();
	public java.util.Date now;
	
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMdd");
	SimpleDateFormat formatter3 = new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat formatter4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat formatter5 = new SimpleDateFormat("yyyyMMddHHmmss");
	SimpleDateFormat formatter6 = new SimpleDateFormat("yyyyMMDDHHmmssSSS");
	
	long startTime = 0;
	
	/**
	 * yyyy-MM-dd HH:mm:ss
	 * @return fommatter
	 */
	public String getCurrentDate4() {
		return formatter4.format(today);
	}
	
	public String getCurrentDate5(){
		return formatter5.format(today);
	}
	
	public String getCurrentDate6(){
		return formatter6.format(today);
	}
	
	public static String getexpiryDate(String calStr) {
		try {
			Calendar nowCal = Calendar.getInstance();
			Calendar regCal = Calendar.getInstance();
			regCal.set(Integer.parseInt(calStr.substring(0,4)), Integer.parseInt(calStr.substring(4,6)), Integer.parseInt(calStr.substring(6,8)), Integer.parseInt(calStr.substring(8,10)), Integer.parseInt(calStr.substring(10,12)), Integer.parseInt(calStr.substring(12,14)));
			long min = ((regCal.getTimeInMillis() - nowCal.getTimeInMillis())/(1000*60))%60;
			long sec = ((regCal.getTimeInMillis() -nowCal.getTimeInMillis())/1000)%60;
			sec = (min*60)+sec;
			return Long.toString(sec);
		} catch (Exception e) {
			return "error";
			// TODO: handle exception
		}
	}
	
	public static String getexpiryDate(int calInt){
		try {
			Calendar nowCal = Calendar.getInstance();
			Calendar regCal = Calendar.getInstance();
			// 현재 셋팅된 시/분/초 에 + 매개변수로 받은 값을 더할 수 있다.
			regCal.add( nowCal.SECOND, + calInt );  
			long min = ((regCal.getTimeInMillis() - nowCal.getTimeInMillis())/(1000*60))%60;
			long sec = ((regCal.getTimeInMillis() -nowCal.getTimeInMillis())/1000)%60;
			sec = (min*60)+sec;
			return Long.toString(sec);
		} catch (Exception e) {
			return "error";
			// TODO: handle exception
		}
		
	}
	
	public void getEndTime(String str){
		
		long endTime = System.currentTimeMillis();
		log.info("############ "+str+" END_TIME ##############");
		log.info("## "+endTime);
		log.info("############ "+str+" END_TIME ##############");
		double iTime = endTime - this.startTime;

//		System.out.println("TIME :: "+iTime+"(초)");
		log.info("############ "+str+" 총 수행시간 ##############");
		log.info("## "+iTime/1000+"(초)");
		log.info("############ "+str+" 총 수행시간 ##############");

	}
	
	public void getStartTime(){
		this.startTime = System.currentTimeMillis();
		log.info("############ START_TIME ##############");
		log.info("## "+startTime);
		log.info("############ START_TIME ##############");
	}
	
	public static void main(String[] args) {
		//System.out.println("ssssssss");
		//System.out.println("201404021130");
		System.out.println(getexpiryDate(9));
		System.out.println(getexpiryDate("20140402120000"));
	}
}
