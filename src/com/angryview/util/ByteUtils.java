/*------------------------------------------------------------------------------
 * 파일명 : ByteUtils
 * 설  명 : Byte에 대한 유틸 성 클래스
 * 버  전 : v1.0
 * 프로젝트 : 스마트 마케팅 플랫폼 구축 - 자바라
 *------------------------------------------------------------------------------
 *                  변         경         사         항
 *------------------------------------------------------------------------------
 *    날짜             작성자                      변경내용
 * ----------  ------  ---------------------------------------------------------
 * 2010.02.01  조민훈D  최초 프로그램 작성
 * 2012.11.01  조민훈D  toBytesFromHexString 메소드 추가
 *------------------------------------------------------------------------------
*/
package com.angryview.util;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.SocketException;

/**
 * 클래스명 : ByteUtils
 * 설명 : Byte에 대한 유틸 성 클래스
 * 주의사항 :
 */
public class ByteUtils {

	//public static Byte DEFAULT_BYTE = new Byte((byte) 0);

	 /**
	   * byte 메시지 수신 함수
	  * 
	   * @param dataLength
	   * @param reader
	   * @return
	   */
	  public static byte[] receiveByteArray(int dataLength, DataInputStream reader)
	  {
	   byte [] receiveData = new byte[dataLength];
	   int tempLength = 0;
	   int readLength = 0;
	   while (readLength < dataLength)
	   {
	    try
	    {
	     tempLength = reader.read(receiveData, readLength, dataLength - readLength);
	     if(tempLength >= 0)
	      readLength = readLength + tempLength;
	     else
	      break;
	    }
	    catch (SocketException se){
	     receiveData = null;
	     break;
	    }
	    catch (IOException se){
	     receiveData = null;
	     break;
	    }
	    catch (Exception se){
	     receiveData = null;
	     break;
	    }
	   }
	   return receiveData;
	  }
	
	
	/**
	 * <p>unsigned byte(바이트) 배열을 16진수 문자열로 바꾼다.</p>
	 *
	 * <pre>
	 * ByteUtils.toHexString(null)                   = null
	 * ByteUtils.toHexString([(byte)1, (byte)255])   = "01ff"
	 * </pre>
	 *
	 * @param bytes unsigned byte's array
	 * @return
	 * @see HexUtils.toString(byte[])
	 */
	public static String toHexString(byte[] bytes) {
		if (bytes == null) {
			return null;
		}

		StringBuffer result = new StringBuffer();
		for (byte b : bytes) {
			result.append(Integer.toString((b & 0xF0) >> 4, 16));
			result.append(Integer.toString(b & 0x0F, 16));
		}
		return result.toString();
	}

	/**
	 * <p>8, 10, 16진수 문자열을 바이트 배열로 변환한다.</p>
	 * <p>8, 10진수인 경우는 문자열의 3자리가, 16진수인 경우는 2자리가, 하나의 byte로 바뀐다.</p>
	 *
	 * <pre>
	 * ByteUtils.toBytes(null)     = null
	 * ByteUtils.toBytes("0E1F4E", 16) = [0x0e, 0xf4, 0x4e]
	 * ByteUtils.toBytes("48414e", 16) = [0x48, 0x41, 0x4e]
	 * </pre>
	 *
	 * @param digits 문자열
	 * @param radix 진수(8, 10, 16만 가능)
	 * @return
	 * @throws NumberFormatException
	 */
	public static byte[] toBytes(String digits, int radix) throws IllegalArgumentException, NumberFormatException {
		if (digits == null) {
			return null;
		}
		if (radix != 16 && radix != 10 && radix != 8) {
			throw new IllegalArgumentException("For input radix: \"" + radix + "\"");
		}
		int divLen = (radix == 16) ? 2 : 3;
    	int length = digits.length();
    	if (length % divLen == 1) {
    		throw new IllegalArgumentException("For input string: \"" + digits + "\"");
    	}
    	length = length / divLen;
    	byte[] bytes = new byte[length];
    	System.out.println("###### bytes.length ####### :: "+bytes.length);
    	for (int i = 0; i < length; i++) {
    		int index = i * divLen;
    		bytes[i] = (byte)(Short.parseShort(digits.substring(index, index+divLen), radix));
    	}
    	return bytes;
	}

	/**
	 * <p>16진수 문자열을 바이트 배열로 변환한다.</p>
	 * <p>문자열의 2자리가 하나의 byte로 바뀐다.</p>
	 *
	 * <pre>
	 * ByteUtils.toBytesFromHexString(null)     = null
	 * ByteUtils.toBytesFromHexString("0E1F4E") = [0x0e, 0xf4, 0x4e]
	 * ByteUtils.toBytesFromHexString("48414e") = [0x48, 0x41, 0x4e]
	 * </pre>
	 *
	 * @param digits 16진수 문자열
	 * @return
	 * @throws NumberFormatException
	 * @see HexUtils.toBytes(String)
	 */
	public static byte[] toBytesFromHexString(String digits) throws IllegalArgumentException, NumberFormatException {
		if (digits == null) {
			return null;
		}
    	int length = digits.length();
    	if (length % 2 == 1) {
    		throw new IllegalArgumentException("For input string: \"" + digits + "\"");
    	}
    	length = length / 2;
    	byte[] bytes = new byte[length];
    	for (int i = 0; i < length; i++) {
    		int index = i * 2;
    		bytes[i] = (byte)(Short.parseShort(digits.substring(index, index+2), 16));
    	}
    	return bytes;
	}
	
	
	/**
	 * 기준보다 크면 false, 작거나 같으면 true
	 * @param txt 체크할 text
	 * @param standardByte 기준 바이트수
	 * @return
	 */
	public static boolean byteCheck(String txt, int standardByte) throws IllegalArgumentException, NumberFormatException {
		
		if(txt == null || "".equals(txt)) {
			return true;
		}
		
		// 바이트 체크 (en : 영문, ko : 한글, etc : 특수문자)
		int en = 0;
		int ko = 0;
		int etc = 0;
		
		char[] txtChar = txt.toCharArray();
		for(int j=0; j< txtChar.length; j++) {
			if(txtChar[j] >= 'A' && txtChar[j] <= 'z') {
				en++;
			} else if(txtChar[j] >= '\uAC00' && txtChar[j] <= '\uD7A3') {
				ko++;
				ko++;
			} else {
				etc++;
			}
		}
		
		int txtByte = en+ko+etc;
		if(txtByte > standardByte) {
			return false;
		} else {
			return true;
		}
	}
}
