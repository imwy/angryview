package com.angryview.util;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class SendUtil {

	public static String SendSMS(String receiver, String msg) {

		String resultCode = "0";
		
		try {

			HttpClient client = HttpClientFactory.getThreadSafeClient();

			PostMethod method = new PostMethod("https://apis.aligo.in/send/");

			method.addParameter("key", "uo4shfu97xk0dwuw43yn3mpe1cmf31y0");
			method.addParameter("user_id", "angryview");
			method.addParameter("sender", "01026229404");
			method.addParameter("receiver", receiver);
			method.addParameter("msg", "[앵그리뷰]" + msg);
			//method.addParameter("testmode_yn", "Y");

			// method.addParameter("destination", "01063030088");
			// method.addParameter("title", "제목입력");
			
			method.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");

			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				System.out.println(method.getStatusLine());
			}
			byte[] responseBody = method.getResponseBody();
			String responseText = new String(responseBody, "utf-8");

			//System.out.println(responseText);

			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(responseText);

			System.out.println(jsonObject);
			resultCode = (jsonObject.get("result_code")).toString();
			System.out.println(resultCode);

		} catch (Exception e) {
			e.printStackTrace();
			resultCode = "9999";
		}
		return resultCode;

	}

	public static void main(String[] args) {

		try {
			String code = SendSMS("01063030088", "내용");
			System.out.println(code);
			
		} catch (Exception e) {

		}

	}

}
