package com.angryview.util;

import java.util.Random;
import java.util.UUID;

/**
 * @author 	: namyong
 * @date		: 2013. 10. 17.
 */
public class Utils {
	
	/**
	 * Random key 생성
	 * Default(32)
	 * @return
	 */
	public String makeRandomKey() {
		return makeRandomKey(16);
	}	
	
	public String makeRandomKey(int length){
		HashUtil hu = new HashUtil(); 
		String rtnVal = "";

		Random random = new Random(System.currentTimeMillis());
		Integer rRandom = Math.abs(random.nextInt());

		rtnVal = hu.hashEncode(Integer.toString(rRandom)).substring(0, length);
		
		return rtnVal;		
	}
	
	public String makeRandomInt(int lenght){
		int rtn;
		
		Random random = new Random(System.currentTimeMillis());
		rtn = Math.abs(random.nextInt());		
		return Integer.toString(rtn).substring(0,21);
	}
	
	
	//카드사(농협쪽에서 넘어오는것중 금액이의 앞자리가 0으로 패딩되어있어서 패딩을 제거하고 금액부분만
	/**
	 * 패딩제거 
	 * 왼쪽부터 0으로된 패딩제거
	 * @return String
	 */
	public String removePadding(String str){
		return removePadding(str,"0");
	}
	/**
	 * 패딩제거
	 * 왼쪽부터 두번째인자값(인자값중 첫번째 문자)으로 된 패딩제거
	 * @return String
	 */
	public String removePadding(String str,String ext){
		String rtnStr="";
		int index=0;
		char ch = ext.charAt(0);
		for(int i=0;i<str.length();i++){
			ch = str.charAt(i);
			if(ch!='0'){
				index = i;
				break;
			}
		}
		rtnStr=str.substring(index);
		return rtnStr;
	}
	
	
	
	public static String makeSessionHash(int length){
		HashUtil hu = new HashUtil(); 
		String rtnVal = "";
		
		for(int i = 0 ; i <10 ; i++){
			rtnVal = hu.hashEncode(UUID.randomUUID().toString()).substring(0, length);
		}
		
		return rtnVal;		
	}
	/*
	//패딩
	public String padding(String str,int count){
		return padding(str," ",count);
	}
	
	//패딩
	public String padding(String str,String ext,int count){
		String rtn="";
		String padding="";
		if(str.length()>=count){
			rtn = str;
		}else{
			for(int i=0; i<count - str.length();i++){
				padding = ext+padding;
			}
			rtn = padding + str; 
		}
		
		return rtn;
		
	}
	*/


}
