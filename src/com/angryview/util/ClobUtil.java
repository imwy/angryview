package com.angryview.util;

import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;

import oracle.sql.CLOB;

public class ClobUtil {

	public static String objToStr(CLOB cl) {

		String str ="";
		try {
			
			if(cl == null){
				return str;
			}
			
			Reader reader = cl.getCharacterStream(); 
			
			char buffer[] = new char[(int)cl.length()];
			int totalByte = 0;
			int readByte = 0;
			while ((readByte = reader.read(buffer,0,(int)cl.length())) != -1 ) {
				totalByte += readByte;
				str += new String(buffer,0,readByte);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			
		}
		
		return str;
	}
}
