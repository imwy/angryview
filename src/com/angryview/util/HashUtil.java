package com.angryview.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * HASH Enc/Dec Util 
 * @author 	: namyong
 * @date		: 2013. 11. 5.
 */
public class HashUtil {
	/**
	 * SHA-512 해쉬값 생성
	 * @param str
	 * @return SHA
	 */
	public String hashEncode(String str){
		
//		System.out.println("hashEncode str 값 : "+str);
		String SHA = ""; 

		try{

			MessageDigest sh = MessageDigest.getInstance("SHA-512"); 

			sh.update(str.getBytes("UTF-8"));
		
			byte byteData[] = sh.digest();
			
			StringBuffer sb = new StringBuffer(); 

			for(int i = 0 ; i < byteData.length ; i++){
				sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
//				System.out.println("sb.toString("+i+") : "+sb.toString());
			}

			SHA = sb.toString();

		}catch(NoSuchAlgorithmException e){
			e.printStackTrace(); 
			SHA = null; 
		}catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return SHA;
	}
	
	/**
	 * SHA-256 해쉬값 생성
	 * @param str
	 * @return SHA
	 */
	public String hashEncode256(String str){
		
		System.out.println("hashEncode str 값 : "+str);
		String SHA = ""; 

		try{

			MessageDigest sh = MessageDigest.getInstance("SHA-256"); 

			sh.update(str.getBytes()); 
			byte byteData[] = sh.digest();
			
			StringBuffer sb = new StringBuffer(); 

			for(int i = 0 ; i < byteData.length ; i++){
				sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
//				System.out.println("sb.toString("+i+") : "+sb.toString());
			}

			SHA = sb.toString();

		}catch(NoSuchAlgorithmException e){
			e.printStackTrace(); 
			SHA = null; 
		}
		return SHA;
	}
}
