package com.angryview.util;

import java.util.ArrayList;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;
import java.io.UnsupportedEncodingException;

/**
 * StringUtil.java
 *
 * @version 1.0
 * @Modifier Joo, Charles
 * @Creator Joo, Charles
 * @date 2008. 11. 28
 */
public class StringUtil {
    public static String[] split(String source,String delim) {

//        GCLog.debug.println("StringUtil.split:source="+source+",delim:"+delim);
        String srcString = source;
        String  token="";
        String[] result =  new String[0];
        ArrayList toKenList = null;
        try {
            toKenList =new ArrayList(); 
            int indexDelim = 0;
            while((indexDelim = srcString.indexOf(delim)) > -1) {
                token = srcString.substring(0,indexDelim);
                toKenList.add(token);
                srcString = srcString.substring(indexDelim+delim.length());
            }
            toKenList.add(srcString);
            result = new String[toKenList.size()];
            toKenList.toArray(result);
        } catch (Exception e) {
//            GCLog.debug.println("StringUtil.split:e="+e.toString());
        	e.printStackTrace();
        } finally {
            toKenList = null;
        }
        for(int i=0;i<result.length;i++) {
//            GCLog.debug.println("StringUtil.split:result["+i+"]="+result[i]);
	        	System.out.println("StringUtil.split:result["+i+"]="+result[i]);
        }
        return result;
    }
    
    public static String changeCharset(String str, String charset) {
        try {
            byte[] bytes = str.getBytes(charset);
            return new String(bytes, charset);
        } catch(UnsupportedEncodingException e) { }//Exception
        return "";
    }
    
    /**********************************************************************
     * �� �� : ����ڿ�(strTarget)���� Ư�����ڿ�(strSearch)�� ã�� �������ڿ�(strReplace)�� ������ ���ڿ��� ��ȯ�Ѵ�.
     * @param strTarget ����ڿ�
     * @param strSearch �������� Ư�����ڿ�
     * @param strReplace ���� ��Ű�� �������ڿ�
     * @return ����Ϸ�� ���ڿ�
     **********************************************************************/
    public static String replace(String strTarget, String strSearch, String strReplace) {
        String result = null;

        try {
            String strCheck = new String(strTarget);
            StringBuffer strBuf = new StringBuffer();

            while (strCheck.length() != 0) {
                int begin = strCheck.indexOf(strSearch);

                if (begin == -1) {
                    strBuf.append(strCheck);

                    break;
                } else {
                    int end = begin + strSearch.length();
                    strBuf.append(strCheck.substring(0, begin));
                    strBuf.append(strReplace);
                    strCheck = strCheck.substring(end);
                }
            }

            result = strBuf.toString();
        } catch (Exception e) {
        }

        return result;
    }

    /**
     * Source String�� Delemeter String���� �����Ͽ�
     * String Array�� �����. �̶�, Delemeter�� String�� ���Ծȵȴ�.
     *
     * @param  source     Source String
     * @param  delemeter  Delemeter String
     * @return String Array
     */
    public static String[]  unString(String source, String delemeter)
    {
        Vector unStrVector = new Vector();
        String[] unStrArray  = null;

        String srcString = null;
        String tempString = null;
        int    fromIndex = 0;
        int    endIndex  = 0;

        if ( source == null || source.equals("") ) {
            tempString = "";
        } else {
            tempString = source.trim();
        }

        while ( endIndex >= 0 ) {
            srcString  = tempString.trim();
            if ( srcString.length() > 0 ) {
                endIndex   = srcString.indexOf(delemeter);

                // There is a delemeter.
                if ( endIndex >= 0 ) {
                    // There is a delemeter at start-point.
                    if ( endIndex == 0 ) {
                        tempString = "";
                    } else {
                        tempString = srcString.substring(0, endIndex);
                    }
                    unStrVector.addElement( tempString );

                    // skip to next available string.
                    endIndex  += delemeter.length();

                    if ( srcString.length() > endIndex  ) {
                        tempString = srcString.substring(endIndex);
                    } else {     // There is a delemeter at end-point.
                        tempString = "";
                        unStrVector.addElement( tempString );
                        endIndex = -1;
                    }
                } else {         // There is not a delemeter.
                    tempString = srcString;
                    unStrVector.addElement( tempString );
                }
            } else {             // There is no available string after trim().
                endIndex = -1;
            }
        }

        if ( unStrVector.size() == 0 ) {
            // There is no available string after trim().
            tempString = "";
            unStrVector.addElement( tempString );
        }

        unStrArray = new String[unStrVector.size()];
        unStrVector.copyInto(unStrArray);
        return  unStrArray;

    }

    /**
     * String Array�� Delemeter�� �����Ͽ�  String���� �����.
     *
     * @param  stringArray   Source String Array
     * @param  delimiter     Delimiter String
     * @return String
     */
    public static String  makeString(String[] stringArray, String delimiter) {
        String   rtnString = "";

        for (int i=0; i < stringArray.length; i++) {
            rtnString += stringArray[i] + delimiter;
        }

        if ( rtnString.length() > 0 ) {
            rtnString = rtnString.substring(0, rtnString.length() - delimiter.length() );  // cut the last delimiter
        }
        return  rtnString;
    }

    /**
     * Source String���� �ش�String��  Hashtable���ִ� ������ ���·� �ٲ۴�.
     * Hashtable�� Key�� ���String�̵ǰ�, Value�� �ٲ�String�� �ȴ�
     *
     * @param  source     Source String
     * @param  strHash    Hashtable(�ٲ� ������ ����)
     * @return ������ �ٲ� String
     */
    public static String  replace(String source, Hashtable strHash)
    {
        if ( (source == null) || (strHash == null) ) {
            return null;
        }

        String srcString = source;
        String tempString = null;
        String tempKey = null;
        String tempValue = null;

        Enumeration enumKeys;
        enumKeys = strHash.keys();

        while(enumKeys.hasMoreElements()) {
            tempKey = (String)enumKeys.nextElement();
            tempValue = (String)strHash.get(tempKey);
            tempString = doReplace(srcString, tempKey, tempValue);
            srcString  = tempString;
        }

        return  srcString;
    }

    /**
     * Source String�߿��� ���ǿ� �´� Ư���κ��� String���� ����´�.
     * ����, ���ǿ� ���������� null�� return�Ѵ�.
     *
     * @param  sourceString  Source String
     * @param  fromString    ����String (��� ������)
     * @param   endString    ����String (��� ������)
     * @return SubString
     */
    public static String getSubString(String sourceString, String fromString, String endString){

        int    fromIndex;
        int    endIndex;
        String returnString = null;

        if ( sourceString != null ) {
            fromIndex = sourceString.indexOf(fromString);
            if ( fromIndex >= 0 ) {
                fromIndex += fromString.length();
                endIndex  = sourceString.indexOf(endString, fromIndex);
                if ( (endIndex >= 0) && (endIndex >= fromIndex) ) {
                    returnString = sourceString.substring(fromIndex, endIndex);
                }
            }
        }

        return returnString;
    }

    /**
     * Source String�� null�̸� length�� 0 �� String�� return�Ѵ�.
     * null�� �ƴϸ�  Source String�� return�Ѵ�.
     *
     * @param  sourceString  Source String
     * @return String
     */
    public static String null2String(String sourceString) {
        return (sourceString == null) ? "" : sourceString;
    }

    /**
     * Source String�� null�̸� "0"�� String���� return�Ѵ�.
     * null�� �ƴϸ�  Source String�� return�Ѵ�.
     *
     * @param  sourceString  Source String
     * @return String
     */
    public static String null2Zero(String sourceString) {

        if ( sourceString == null || sourceString.equals("") ) {
            return  "0";
        } else {
            return  sourceString;
        }
    }

    /**
     * Source String�߿��� ���ǿ� �´� Ư���κ��� String���� ����´�.
     * ����, ���ǿ� ���������� null�� return�Ѵ�.
     * getSubString�� �ٸ����� from�� end�� pair�� �����.
     *
     * @param  sourceString  Source String
     * @param  fromString    ����String (��� ������)
     * @param   endString    ����String (��� ������)
     * @return SubString
     */
    public static String getSubStringExt(String sourceString, String fromString, String endString) {

        int    fromIndex;
        int    endIndex;
        int    beginTagCount = 0;
        String returnString = null;

        if ( sourceString == null ) {
            return  returnString;
        }

        beginTagCount = countBeginTag(sourceString, fromString, endString);

        if ( beginTagCount <= 0 ) {
            return  returnString;
        }

        endIndex = countEndTag(sourceString, endString, beginTagCount);

        if ( endIndex < 0 ) {
            return  returnString;
        }

        fromIndex = sourceString.indexOf(fromString);

        fromIndex += fromString.length();

        if ( endIndex >= fromIndex ) {
            returnString = sourceString.substring(fromIndex, endIndex);
        }

        return returnString;
    }

    /**
     * Source String�� ���ر��̺��� ������ �ش���� ��ŭ
     * "0"�� Source String�տ� padding��Ų��.
     * ����, ���ų� ũ�� Source String�� return�Ѵ�.
     *
     * @param  sourceString  Source String
     * @param  length        ���ر���
     * @return String
     */
    public static String makeNZero(int length, String  sourceString )
    {
        String  returnString = "";

        if ( length <  1 ) {
            return  sourceString;
        }

        for (int ix = 0; ix < length; ix++) {
            returnString += "0";
        }

        if ( sourceString.length() >= returnString.length() ) {
            return  sourceString;
        } else {
            returnString = returnString.substring( 0, returnString.length() - sourceString.length() );
            return  returnString + sourceString;
        }
    }

    /**
     * Source String�� ���ر��̺��� ������ �ش���� ��ŭ
     * " "�� Source String�ڿ�padding��Ų��.
     * ����, ���ų� ũ�� Source String�� return�Ѵ�.
     *
     * @param  sourceString  Source String
     * @param  length        ���ر���
     * @return String
     */
    public static String makeNSpace(int length, String  sourceString )
    {
        String  returnString = "";

        if ( length <  1 ) {
            return  sourceString;
        }

        for (int ix = 0; ix < length; ix++) {
            returnString += " ";
        }

        if ( sourceString.length() >= returnString.length() ) {
            return  sourceString;
        } else {
            returnString = returnString.substring( 0, returnString.length() - sourceString.length() );
            return  sourceString + returnString;
        }
    }

    /**
     * Source String�� ���ر��̺��� ������ �ش���� ��ŭ
     * " "�� Source String�ڿ�padding��Ų��.
     * ����, ���ų� ũ�� Source String�� return�Ѵ�.
     *
     * @param  sourceString  Source String
     * @param  length        ���ر���
     * @return String
     */
    public static String makeNSpaceBytes(int length, String  sourceString )
    {
        String  returnString = "";
        int     byteLength   = sourceString.getBytes().length;

        if ( byteLength > length ) {

            return getByteSubstring( sourceString, 0, length );

        } if ( byteLength == length ) {

        return  sourceString;

    } else {

        for (int ix = 0; ix < ( length - byteLength ); ix++) {
            returnString += " ";
        }

        return  sourceString + returnString;
    }
    }

    public static String makeNSpaceBytesR(int length, String  sourceString )
    {
        String  returnString = "";
        int     byteLength   = sourceString.getBytes().length;

        if ( byteLength > length ) {

            return getByteSubstring( sourceString, 1, length );

        } if ( byteLength == length ) {

        return  sourceString;

    } else {

        for (int ix = 0; ix < ( length - byteLength ); ix++) {
            returnString += " ";
        }

        return  returnString + sourceString;
    }
    }

    private static String getByteSubstring( String str, int startByte, int endByte ) {

        int startPos = 0;
        int endPos   = 0;

        startPos = getPos( str, startByte );
        endPos   = getPos( str, endByte   );

        return str.substring( startPos, endPos );
    }


    private static int getPos( String str, int bytePos ) {

        int    pos     = 0;
        int    inc     = 0;
        int    tempPos = 0;
        int    initPos = 0;
        int    byteLen = 0;

        initPos = (bytePos > str.length()) ? str.length() : bytePos;

        do {
            tempPos = initPos + inc;
            byteLen = str.substring(0, tempPos).getBytes().length;

            if ( byteLen <= bytePos ) {
                pos = tempPos;
            } else {
                if ((byteLen - bytePos) == 1) {
                    inc--;
                } else {
                    inc = inc - (byteLen - bytePos)/2;
                }
            }
        } while ( byteLen > bytePos );

        return pos;
    }

    /**
     * Source String�� �ѱ�CharacterSet(KSC5601)�� �ٲ۴�
     *
     * @param  english  Source String
     * @return ����� String
     */
    public static String A2U( String alllang ) {
        String utf = null;

        if (alllang == null ) return null;
        //if (english == null ) return "";

        try {
            utf = new String(new String(alllang.getBytes(), "UTF-8"));
        }catch( UnsupportedEncodingException e ){
            utf = new String(alllang);
        }

        return utf;
    }
    /**
     * Source String�� �ѱ�CharacterSet(KSC5601)�� �ٲ۴�
     *
     * @param  english  Source String
     * @return ����� String
     */
    public static String E2K( String english ) {
        String korean = null;

        if (english == null ) return null;
        //if (english == null ) return "";

        try {
            korean = new String(new String(english.getBytes("8859_1"), "KSC5601"));

        }catch( UnsupportedEncodingException e ){
            korean = new String(english);
        }

        return korean;
    }

    /**
     * Source String�� ����CharacterSet(8859_1)�� �ٲ۴�
     *
     * @param  korean  Source String
     * @return ����� String
     */
    public static String K2E( String korean ) {
        String english = null;

        if (korean == null ) return null;
        //if (korean == null ) return "";

        try {
            english = new String(new String(korean.getBytes("KSC5601"), "8859_1"));

        }catch( UnsupportedEncodingException e ){
            english = new String(korean);
        }

        return english;
    }
    // sub Methods:

    private static int countBeginTag(String srcString, String fromString, String endString) {

        int    fromIndex = 0;
        int    endIndex  = 0;
        int    endLength = 0;
        int    beginTagCount = 0;
        String tempString = null;

        endLength  = srcString.indexOf(endString, fromIndex);

        if (endLength <= 0) {
            beginTagCount = 0;
            return  beginTagCount;
        }

        tempString = srcString.substring(0, endLength);

        String delemeter = fromString;

        while ( endIndex >= 0 ) {

            if ( tempString.length() <=  fromIndex ) {
                endIndex = -1;       // There is a delemeter at end-point.
                break;
            }

            endIndex   = tempString.indexOf(delemeter, fromIndex);

            if ( endIndex >= 0 ) {
                beginTagCount++;
                fromIndex  = endIndex + delemeter.length();       // skip to next available string.
            }
        }

        return  beginTagCount;
    }

    private static int countEndTag(String srcString, String endString, int beginTagCount) {

        int    fromIndex = 0;
        int    endIndex  = 0;
        int    ix        = 0;

        String delemeter = endString;

        for (ix = 0; ix < beginTagCount; ix++) {
            if ( srcString.length() <= fromIndex  ) {
                endIndex = -1;       // There is a delemeter at end-point.
                break;
            }

            endIndex   = srcString.indexOf(delemeter, fromIndex);

            if ( endIndex >= 0 ) {
                fromIndex  = endIndex + delemeter.length();       // skip to next available string.
            } else {
                endIndex = -1;
                break;
            }
        }

        return  endIndex;
    }

    private static String  doReplace(String source, String key, String value)
    {
        int    fromIndex  = 0;
        int    endIndex   = 0;
        String srcString = source;
        String tempString = null;
        String rtnString = "";

        while( endIndex >= 0) {
            if ( srcString.length() <  fromIndex + 1 ) {
                break;
            }

            endIndex   = srcString.indexOf(key, fromIndex);
            if ( endIndex < 0 ) {
                rtnString += srcString.substring(fromIndex);
                break;
            }

            if ( fromIndex < endIndex ) {
                tempString = srcString.substring(fromIndex, endIndex);
            } else {
                tempString = "";
            }

            rtnString += tempString + value;
            fromIndex  = endIndex + key.length();
        }

        return  rtnString;
    }

    // sub Methods End:


}
