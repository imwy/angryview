/**
 * @(#) FrameException.java
 * @version  
 * Copyright  
 * All rights reserved.
 * 작성 : 
 * @author  
 *         
 */

package com.angryview.util;
import  java.io.*;

/**
 *   
 *   Exception chaining에 필요한 기본적인 작업을 수행한다.
 */
public class FrameException extends Exception {
	// Exception 
	private java.lang.Throwable rootCause; 
	private boolean isFirst = true;

    /**
     * 특정한 메시지 없이 FrameException을 생성한다.
     */
    public FrameException() {
		super();
    }
 
    /**
     * 특정한 메시지를 갖는 FrameException을 생성한다.
     * @param s 메시지
     */
    public FrameException(String s) {
		super(s);
    }

    /**
     * 특정한 메시지와 Throwable을 갖는 FrameException을 생성한다.
     * @param s 메시지
     * @param rootCause ex chaining에 필요한 Throwable
     */
    public FrameException(String message, Throwable rootCause) {
		super(message);
		this.rootCause = rootCause;
		this.isFirst = false;
    }
	
    /**
     * 특정한 Throwable을 갖는 FrameException을 생성한다.
     * @param rootCause ex chaining에 필요한 Throwable
    */
    public FrameException(Throwable rootCause) {
		this();
		this.rootCause = rootCause;
		this.isFirst = false;
    }

    /**
     * Throwable과 ex chaining한 메시지의 stack trace를
     * 지정한 error output stream인 System.err에 출력한다.
     * @see java.lang.Throwable
     */
    public void printStackTrace() {
		printStackTrace( System.err );
    }

    /**
     * Throwable과 ex chaining한 메시지의 stack trace를
     * 지정한 error output stream인 PrintStream에 출력한다.
     * @param PrintStream 
     * @see java.lang.Throwable
     */
    public void printStackTrace(java.io.PrintStream s) {
	synchronized (s) {
	    super.printStackTrace(s);

	    if (rootCause != null) {
		rootCause.printStackTrace(s);
	    }

            if (isFirst || !(rootCause instanceof FrameException)  ) {
		s.println("-----------------------------");
	    }
	}
    }

    /**
     * Throwable과 ex chaining한 메시지의 stack trace를
     * 지정한 error output stream인 PrintWriter에 출력한다.
     * @param s PrintWriter
     * @see java.lang.Throwable
     */
    public void printStackTrace(java.io.PrintWriter s) {
	synchronized (s) {
	    super.printStackTrace(s);

	    if (rootCause != null) {
		rootCause.printStackTrace(s);
	    }

            if (isFirst || !(rootCause instanceof FrameException)  ) {
		s.println("-----------------------------");
	    }
	}
    }
	
    /**
     * LException의 rootCause를 리턴하는 메소드이다.
     * @return Throwable인 rootCause를 리턴
     */
    public Throwable getRootCause() {
	  return rootCause;
    }

    /**
     * Stack Trace를 string형태로 리턴하는 메소드이다.
     * @return Stack Trace를 string 형태로 리턴
     */
    public String getStackTraceString() {
	StringWriter s = new StringWriter();
	printStackTrace( new PrintWriter(s) );
	return s.toString();
    }
}
