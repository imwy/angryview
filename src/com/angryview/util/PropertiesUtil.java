package com.angryview.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Properties;

public class PropertiesUtil {
	
	
	private Properties property = null;
	
	
	public PropertiesUtil()  throws IOException{
		ClassLoader cl;
		cl = Thread.currentThread().getContextClassLoader();
		if(cl == null){
			cl = ClassLoader.getSystemClassLoader();
		}
		URL url = cl.getResource("properties"+File.separator+"config.properties");
		
		String path = URLDecoder.decode(url.getPath());	
		
		FileInputStream fis;
		
		fis = new FileInputStream(path);
		
		property = new Properties();
		property.load(fis);
		
	}
	
	public Properties getProperty() {
		return property;
	}
	
	public Object get(Object key) {
		
		if(property == null){
			return null;
		}
		
		return property.get(key);
	}
}
