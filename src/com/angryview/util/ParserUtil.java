package com.angryview.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;



public class ParserUtil {
	private Logger log = Logger.getLogger(this.getClass());
	int total;
	int current;
	String str;
	byte[] bytes;
	
	/*
	 * 생성자
	 */
	public ParserUtil(){
		bytes=new byte[1000];
		
	}

	/*
	 * 생성자
	 */
	public ParserUtil(int i){
		total=i;
		current =0;
		bytes = new byte[total];		
	}
	
	/*
	 * 생성자
	 */	
	public ParserUtil(byte[] bytes){
		this.bytes = bytes;
		str = new String(bytes);
		total = bytes.length;
		current = 0;
	}
	/*
	 * 생성자
	 */	
	public ParserUtil(String str){
		this.str = str;
		bytes = str.getBytes();
		total = bytes.length;
		current = 0;
	}
	/*
	 * 전문추가
	 * 
	 */	
	public void setString(String str,int cnt) throws Exception{
		byte[] tmpByte = str.getBytes();
		String msg =str;
		if(tmpByte.length>cnt){
			log.debug("전문의 내용: "+str+ "사이즈는 : " +str.length() +" 허용 사이즈는" +cnt +" 입니다.");
		}else if(tmpByte.length<cnt){			
			msg = setBlank(msg, cnt-tmpByte.length);
		}
		tmpByte = msg.getBytes();
		
		//
		System.arraycopy(tmpByte, 0, bytes, current, cnt);
		current +=cnt;
	}
	/*
	 * 전문추가
	 */
	public void setString(String str){
		this.str +=str;
	}
	/*
	 * 공백세팅
	 */
	public String setBlank(String str,int cnt){
		if(cnt>0){
			for(int i=0;i<cnt;i++){
				str = " "+str;
			}
		}
		return str;
	}
	
	/*
	 * KB국민카드 공백세팅
	 */
	public String setBlankKB(String str,int cnt){
		if(cnt>0){
			for(int i=0;i<cnt;i++){
				str = str+" ";
			}
		}
		return str;
	}
	
	
	/*
	 * 전문세팅
	 */
	public void setByte(byte[] bytes){
		this.bytes =bytes;
		str = new String(bytes);
	}
	public void setByte(byte[] addByte,int cnt){
		log.debug("byteLength : " +addByte.length );
		log.debug("current : " +current );
		log.debug("current : " +cnt );
		log.debug("bytes.length : " +bytes.length );
		System.arraycopy(addByte, 0, bytes, current, cnt);
		current +=cnt;
	}
	/*
	 * 전문세팅
	 */
	public void setByte(String str,int cnt) throws Exception{
		//String fileEncoding=System.getProperty("file.encoding");
		//System.out.println("### [ParserUtil] file.encoding = "+fileEncoding);
		str = StringUtil.changeCharset(str,"EUC-KR");
		//System.out.println("### str : "+ str);
		byte[] tmpByte = str.getBytes("EUC-KR");
		//System.out.println("### tmpByte : "+ new String(tmpByte));
		
		String msg =str;
		log.debug("### tmpByte.length : "+ tmpByte.length+" ### cnt : "+cnt);
		
		if(tmpByte.length>cnt){
			log.debug("전문의 내용: "+str+ "사이즈는 : " +str.length() +" 허용 사이즈는" +cnt +" 입니다.");
		}else if(tmpByte.length<cnt){			
			msg = setBlank(msg, cnt-tmpByte.length);
		}

		tmpByte = msg.getBytes("euc-kr");
//		System.out.println("### msg : "+ msg);
		//System.out.println("### tmpByte' : "+ new String(tmpByte));
		log.debug("bytes :"+bytes.length +" tempByte :"+tmpByte.length + " tempByte STR : "  +new String(tmpByte));
		System.arraycopy(tmpByte, 0, bytes, current, cnt);
		current +=cnt;
	}
	
	
	/*
	 * 전문세팅
	 */
	public void setByteKB(byte[] bytes){
		this.bytes =bytes;
		str = new String(bytes);
	}
	public void setByteKB(byte[] addByte,int cnt){
		log.debug("byteLength : " +addByte.length );
		log.debug("current : " +current );
		log.debug("current : " +cnt );
		log.debug("bytes.length : " +bytes.length );
		System.arraycopy(addByte, 0, bytes, current, cnt);
		current +=cnt;
	}
	/*
	 * KB국민카드 전문세팅
	 */
	public void setByteKB(String str,int cnt) throws Exception{
	
		str = StringUtil.changeCharset(str,"EUC-KR");
		byte[] tmpByte = str.getBytes("EUC-KR");
		String msg =str;
		
		if(tmpByte.length>cnt){

		}else if(tmpByte.length<cnt){			
			msg = setBlankKB(msg, cnt-tmpByte.length);
		}

		tmpByte = msg.getBytes("euc-kr");
//		System.out.println("### msg : "+ msg);
		//System.out.println("### tmpByte' : "+ new String(tmpByte));
		log.debug("bytes :"+bytes.length +" tempByte :"+tmpByte.length + " tempByte STR : "  +new String(tmpByte));
		System.arraycopy(tmpByte, 0, bytes, current, cnt);
		current +=cnt;
	}
	
	/*
	 * 전문받기
	 */
	public byte[] getByte(){		
		return bytes;
	}
	
	/*
	 * 전문파싱
	 */
	public byte[] getByte(int cnt){
		byte[] rByte = new byte[cnt];
		System.arraycopy(bytes, current, rByte, 0,cnt);
		current +=cnt;
		return rByte;
	}
	
	/*
	 * 전문파싱 되돌리기
	 */
	public void getMinusByte(int cnt){
	
		current = current - cnt;
		
	}
	
	/*
	 * 전문파싱
	 */
	public byte[] getByte(int start, int cnt){
		byte[] rByte = new byte[cnt];
		System.arraycopy(bytes, start, rByte, 0, cnt);
		return rByte;
	}
	/*
	 * 바이트파싱
	 */
	public byte[] getByte(byte[] bytes, int start, int cnt){
		this.bytes = bytes;
		byte[] rByte = new byte[cnt];
		System.arraycopy(bytes,start, rByte, 0, cnt);
		return rByte;
	}
	/*
	 * String파싱
	 */
	public String getString(int cnt){	
		String rtn = "";
		//new String(euckrByte, "EUC-KR")
		try {
			rtn = new String(getByte(cnt), "EUC-KR");
		} catch (Exception e) {
			e.toString();
		}
		return rtn;		
	}
	public int getInt(int cnt){
		int rtn=0;
		String strRtn = new String(getByte(cnt));
		rtn = Integer.parseInt(strRtn.trim());
		return rtn;
	}
	/*
	 * String 파싱
	 */
	public String getString(int start, int cnt){
		return str.substring(start, start+cnt);
	}
	/*
	 * 전문받기
	 */
	public String getString(){
		return str;
	}
	
	public static void main(String args[]){	
		try{
		ParserUtil parser = new ParserUtil();
		parser.setByte("00700", 5);
		parser.setByte("00700", 10);
		parser.setByte("START"+" "+"END", 10);
		byte[] bytes = parser.getByte();
		System.out.println("바이트 :"+bytes.length);
		System.out.println("스트링 :"+new String(bytes));
			
		/*
		ParserUtil parser = new ParserUtil(30) ;
		parser.setString("00",2);
		parser.setString("nice",10);
		parser.setString("12345",10);
		parser.setString("20130116",8);
		byte[] testByte =parser.getByte();
		parser = new ParserUtil(testByte);
		parser.log.info(new String(parser.getByte()));
		
		parser.log.info("'"+parser.getString(2)+"'");
		parser.log.info("'"+parser.getString(10)+"'");
		parser.log.info("'"+parser.getString(10)+"'");
		parser.log.info("'"+parser.getString(8)+"'");
		
		parser.log.info("바이트길이 :"+testByte.length);
		parser.log.info("바이트 :"+testByte);
		parser.log.info("스트링변환:"+new String(testByte));
		*/
		 /*
		ParserUtil util = new ParserUtil();
		byte[] bytes;
		String str;
		util.setString("NICEIF",9);
		util.setString("0200");
		util.setString("73035");
		util.setString("B");
		util.setString("503");
		util.setBlank(4);
		util.setString("ID",9);
		util.setString("1223334444",10);
		util.setString("20130106101121",14);
		util.setString("1223334444",10);
		util.setString("20130106101121",14);
		util.setBlank(17);
		str =util.getString();
		util.setByte(str);
		bytes =util.getByte();
		str = new String(bytes);
		parser.log.info("스트링 "+str.length()+" : "+str);
		parser.log.info("바이트 "+bytes.length+" : "+new String(bytes));
		//parser.log.info("바이트("+new String(bytes).length()+") :"+new String(bytes));
		parser.log.info(new String(util.getByte(10)));
		parser.log.info(new String(util.getByte(9)));		
		parser.log.info(new String(util.getByte(4)));
		parser.log.info(new String(util.getByte(5)));
		parser.log.info(new String(util.getByte(1)));
		
		util.current =0;
		parser.log.info(util.getString(10));
		parser.log.info(util.getString(9));		
		parser.log.info(util.getString(4));
		parser.log.info(util.getString(5));
		parser.log.info(util.getString(1));
		*/
		
		ParserUtil util = new ParserUtil("11ABCEDFGHIJKLMNOPQRSTUVWXYZ");
		parser.log.info(util.getInt(2));
		parser.log.info(util.getString(3));
		parser.log.info(util.getString(2));
		parser.log.info(util.getString(1));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
		 
	
}
