package com.angryview.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

import org.apache.log4j.Logger;

public class ReceiveThread extends Thread{
	
	private Logger log = Logger.getLogger(this.getClass());
	BufferedReader sbr;
	Socket socket;
	
	public ReceiveThread(Socket socket){
		this.socket=socket;
	}
	
	public void run(){
		try{
			sbr = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			while(true){
				String receive =sbr.readLine();				
				log.info(socket.getInetAddress()+" : "+receive);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	
	}

}
