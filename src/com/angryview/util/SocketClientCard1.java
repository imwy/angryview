package com.angryview.util;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import org.apache.log4j.Logger;


public class SocketClientCard1 extends Thread{
	private Logger log = Logger.getLogger(this.getClass());
	Socket socket;
	OutputStream os;
	BufferedOutputStream bos;
	BufferedInputStream bis;
	DataInputStream dis;
	String ip = "";
	int port = 0;

	
	byte[] send_msg = null;
	byte[] recive_msg = null;
	int msgLength = 0;// 카드사에 전달될 전체전문
	
	public byte[] getRecive_msg() {
		return recive_msg;
	}

	public boolean getsocketIsConnected() {
		boolean returnBoolean = false;
		if(socket != null || socket.isConnected()){
			returnBoolean = true;
		}
		return returnBoolean;
	}
	
	public void setRecive_msg(byte[] recive_msg) {
		this.recive_msg = recive_msg;
	}

	public SocketClientCard1(byte[] msg,int msgL) throws Exception{
		this.send_msg = msg;
		this.msgLength = msgL;
	}
	
	public SocketClientCard1(byte[] msg,int msgL, String ip, int port) throws Exception{

		this.send_msg = msg;
		this.msgLength = msgL;
		this.ip = ip;
		this.port = port;
	}
	

	public byte[] getSocketData(byte[] msg,int msgL, String ip, int port) throws IOException, InterruptedException {
		
		byte[] byteMsg=null;
		int timeout = 1000 * 5;
		
		String s = "";
		String receive = "";
		//log.info("getSocketDatagetSocketData");
		//log.info(msg);
		//log.info(msgL);
		//log.info(ip);
		//log.info(port);
		
		try {
			SocketAddress socketAddress = new InetSocketAddress(ip, port);
			socket = new Socket();
			
			socket.setSoTimeout(timeout);			// InputStream에서 데이터읽을때의 timeout 설정
			socket.connect(socketAddress, timeout);	// socket연결 자체에 대한 timeout
			os = socket.getOutputStream();
	        bos = new BufferedOutputStream(os);
	        bos.write(msg);
	        bos.flush();
	        byteMsg = new byte[msgL]; 
	        
	        if(msgL == 0){
	        	msgL = 6;
	        	byteMsg = new byte[msgL];
	        	bis = new BufferedInputStream(socket.getInputStream());
		        for(int i = 0 ; i < msgL; i++){
		        	byteMsg[i] =(byte)bis.read();
		        }
		        msgL = Integer.parseInt(new String(byteMsg).substring(2, 6)) - 6;
		        byteMsg = new byte[msgL];
		        
		        for(int i = 0 ; i < msgL; i++){
		        	byteMsg[i] =(byte)bis.read();
		        }
	        }
	        else if(msgL >= 3500){
	        	dis = new DataInputStream(socket.getInputStream());
	        	dis.readFully(byteMsg);
	        }else{
	        	bis = new BufferedInputStream(socket.getInputStream());
		        for(int i = 0 ; i < msgL; i++){
		        	byteMsg[i] =(byte)bis.read();
		        }
	        }

		} catch (Exception e) {
			e.printStackTrace();
			log.info(e.toString() + " [TransSerial] = " + ip);
			byteMsg = "error".getBytes();
		} finally{
			try {
				if(dis!=null) dis.close();
				if(os!=null) os.close();
				if(bos!=null) bos.close();
				if(socket!=null) socket.close();
				if(bis != null) bis.close();
			} catch (Exception e2) {
				log.error("[KAKAOTAXI_ERROR]"+ "[TransSerial]=" + "["+ ip +"]" + this.getClass().getName() + " Throwable", e2);
				e2.printStackTrace();
			}
			
		}
		return byteMsg;
	}
	
	public void run(){
		try {
			if(port != 0){
				this.recive_msg = this.getSocketData(send_msg,msgLength, ip, port);
			}else{
				this.recive_msg = this.getSocketData(send_msg,msgLength);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public byte[] getSocketData(byte[] msg,int msgL) throws IOException, InterruptedException {
		byte[] byteMsg=null;
		String ip = "";
		int port = 80;
		int timeout = 1000 * 9;   //1000 * 30
		String s = "";
		String receive = "";
		try {
			SocketAddress socketAddress = new InetSocketAddress(ip, port);
			socket = new Socket();
			
			socket.setSoTimeout(timeout);			// InputStream에서 데이터읽을때의 timeout 설정
			socket.connect(socketAddress, timeout);	// socket연결 자체에 대한 timeout
			os = socket.getOutputStream();
	        bos = new BufferedOutputStream(os);
	        bos.write(msg);
	        bos.flush();
	        byteMsg = new byte[msgL]; 
	        
	        if(msgL == 0){
	        	msgL = 6;
	        	byteMsg = new byte[msgL];
	        	bis = new BufferedInputStream(socket.getInputStream());
		        for(int i = 0 ; i < msgL; i++){
		        	byteMsg[i] =(byte)bis.read();
		        }
		        msgL = Integer.parseInt(new String(byteMsg).substring(2, 6)) - 6;
		        byteMsg = new byte[msgL];
		        
		        for(int i = 0 ; i < msgL; i++){
		        	byteMsg[i] =(byte)bis.read();
		        }
	        }
	        else if(msgL >= 3500){
	        	dis = new DataInputStream(socket.getInputStream());
	        	dis.readFully(byteMsg);
	        }else{
	        	bis = new BufferedInputStream(socket.getInputStream());
		        for(int i = 0 ; i < msgL; i++){
		        	byteMsg[i] =(byte)bis.read();
		        }
	        }
		} catch (Exception e) {
			e.printStackTrace();			
			byteMsg = "error".getBytes();
		} finally{
			try {
				if(dis!=null) dis.close();
				if(os!=null) os.close();
				if(bos!=null) bos.close();
				if(socket!=null) socket.close();
				if(bis != null) bis.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return byteMsg;
	}
}
