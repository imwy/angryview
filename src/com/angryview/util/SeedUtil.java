package com.angryview.util;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class SeedUtil {

	static {
		Security.addProvider(new BouncyCastleProvider());
	}
	static byte[] iv = "ssgpay_nice_1234".getBytes();

	public static String seedEncrypt(String seedkey, String plainText) {
		if (plainText == null)
			return null;
		Cipher cipher;
		byte[] keyData = seedkey.getBytes();
		SecretKey secretKey;
		String  ret=null;
		try {
			secretKey = new SecretKeySpec(keyData, "SEED");
			cipher = Cipher.getInstance("SEED/CBC/PKCS5Padding","BC");
			//Arrays.fill(keyData, (byte) 0x00);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(iv));
			// NoPadding이 경우 블럭크기 x n에 맞추어야 한다.
			/*
			 * if ((plainText.length % cipher.getBlockSize()) != 0) { //1.6부터
			 * 지원이므로 바꿔야함 plainText = (byte[])resizeArray( plainText,
			 * plainText.length + cipher.getBlockSize() - (plainText.length %
			 * cipher.getBlockSize()) ); }
			 */
//			ret = ByteUtil.byteArryToHex(cipher.doFinal(plainText.getBytes()));
			ret = new String(toHex(cipher.doFinal(plainText.getBytes())));

		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			cipher = null;
			keyData = null;
			secretKey = null;
		}
		return ret;
	}

	
	public static String seedDecrypt(byte[] seedkey, String plainText) {
		
		if (plainText == null)
			return null;
		Cipher cipher;
		byte[] keyData = seedkey;
		SecretKey secretKey;
		String  ret=null;
		try {
			secretKey = new SecretKeySpec(keyData, "SEED");

			cipher = Cipher.getInstance("SEED/CBC/PKCS5Padding","BC");
			//keyData = new byte[cipher.getBlockSize()];
			//Arrays.fill(keyData, (byte) 0x00);
			cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
			// NoPadding이 경우 블럭크기 x n에 맞추어야 한다.
			/*
			 * if ((plainText.length % cipher.getBlockSize()) != 0) { //1.6부터
			 * 지원이므로 바꿔야함 plainText = (byte[])resizeArray( plainText,
			 * plainText.length + cipher.getBlockSize() - (plainText.length %
			 * cipher.getBlockSize()) ); }
			 */
//			ret = new String(cipher.doFinal(ByteUtil.hexToByteArray(plainText)));
			ret = new String(cipher.doFinal(toByte(plainText)));
			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}finally{
			cipher = null;
			keyData = null;
			secretKey = null;
		}
		
		
		return ret;
	}

	public static byte[] toByte(String hexString) {
		int len = hexString.length() / 2;
		String hexStringTemp;
		byte[] result = new byte[len];
		for (int i = 0; i < len; i++) {
			hexStringTemp = hexString.substring(2 * i, 2 * i + 2);
			result[i] = Integer.valueOf(hexStringTemp, 16).byteValue();
		}
		return result;
	}
	
	
	public static String toHex(byte[] buf) {
		if (buf == null) {
			return "";
		}
		StringBuffer result = new StringBuffer(2 * buf.length);
		for (int i = 0; i < buf.length; i++) {
//			System.out.println("@@"+result);
//			System.out.println("##"+buf[i]);
			appendHex(result, buf[i]);
		}
		return result.toString();
	}
	
	private static final String HEX = "0123456789ABCDEF";
	
	private static void appendHex(StringBuffer sb, byte b) {
		sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
	}
	
	public static void main(String[] args) {
		String str = "1111111111111111";
		String val = "";
		
		SeedUtil su = new SeedUtil();
//		str	= su.seedEncrypt("ssgpay_nice_1234", str);
//		System.out.println("암호화 = "+str);
//		val = su.seedDecrypt("ssgpay_nice_1234".getBytes(), str);
//		System.out.println("복호화 = "+val);
		try{
			System.out.println(str.length());
//			su.seedInit(ByteUtils.toBytes("0123456789ABCDEF0123456789ABCDEF", 16));
			
			
			su.seedInit("0123456789ABCDEF0123456789ABCDEF");
			System.out.println(su.encrypt(str));
			System.out.println(su.encrypt(str).length());
			System.out.println(su.decrypt("B95BF1BE27AB37E472FAAD9E97E27FEF"));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
		//키 지정(마스터키 용)
		private String cipherPass = "";

		//키를 Hex 방식으로 리턴
		private String sk = "";
		private Key key = null;
		private Cipher cipher = null;

		//알고리즘/모드/패딩방식 지정
		String transformation = "SEED/ECB/NoPadding";

		public void seedInit(String password) throws InvalidKeyException, NumberFormatException, NoSuchAlgorithmException, InvalidKeySpecException, IllegalArgumentException, NoSuchPaddingException {
//			sk = ByteUtils.toHexString(password);
			sk = password;
			key = generateKey("SEED", ByteUtils.toBytes(sk, 16));
			cipher = Cipher.getInstance(transformation);
		}
		
		/**
		 * 해당 알고리즘으로 암호화하여 Base64 인코딩한 후 리턴한다.
		 * @return Encrypt Encoding String
		 */
		public String encrypt(String encryptStr)throws Exception{
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plain = encryptStr.getBytes("UTF-8");
			byte[] encrypt = cipher.doFinal(plain);
//			return new String( Base64.encode(encrypt));
			return new String(toHex(encrypt));
		}

		/**
		 * 해당 알고리즘으로 디코딩하여 복호화 한 후 리턴한다.
		 * @return Decrypt String
		 */
		public String decrypt(String decryptStr) throws Exception{
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] decryptByte =  toByte(decryptStr);
			byte[] decrypt = cipher.doFinal(decryptByte);
			String plainText = new String(decrypt,"UTF-8");
			return plainText;
		}
		
		/**
		 * 주어진 데이터로, 해당 알고리즘에 사용할 비밀키(SecretKey)를 생성한다.
		 * @param algorithm DES/DESede/TripleDES/AES
		 * @param keyData
		 */
		public Key generateKey(String algorithm, byte[] keyData) throws NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException {
			String upper = algorithm.toUpperCase();
			if ("DES".equals(upper)) {
				KeySpec keySpec = new DESKeySpec(keyData);
				SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(algorithm);
				SecretKey secretKey = secretKeyFactory.generateSecret(keySpec);
				return secretKey;
			} else if ("DESede".equals(upper) || "TripleDES".equals(upper)) {
				KeySpec keySpec = new DESedeKeySpec(keyData);
				SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(algorithm);
				SecretKey secretKey = secretKeyFactory.generateSecret(keySpec);
				return secretKey;
			} else {
				SecretKeySpec keySpec = new SecretKeySpec(keyData, algorithm);
				return keySpec;
			}
		}
}
