package com.angryview.board.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.angryview.common.iBatisDaoSupport;

@Repository("boardDao")
public class BoardDao extends iBatisDaoSupport {
	
	public List<Map<String, Object>> getNotiList(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getNotiList", paramMap);
	}
	
	public String getNotiListCnt(Map<String, Object> paramMap) {
		return (String)select("getNotiListCnt", paramMap);
	}
	
	public Map<String, Object> getNoti(Map<String, Object> paramMap){
		return (Map<String, Object>)select("getNoti", paramMap);
	}
	
	public Map<String, Object> getContents(Map<String, Object> paramMap){
		return (Map<String, Object>)select("getContents", paramMap);
	}
	
	public List<Map<String, Object>> getQuestionList(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getQuestionList", paramMap);
	}
	
	public String getQuestionListCnt(Map<String, Object> paramMap) {
		return (String)select("getQuestionListCnt", paramMap);
	}
	
	
	public Map<String, Object> getQuestionDetail(Map<String, Object> paramMap){
		return (Map<String, Object>)select("getQuestionDetail", paramMap);
	}
	
	public void insertQuestion(Map<String, Object> paramMap){
		insert("insertQuestion", paramMap);
	}
	
	public void deleteQuestion(Map<String, Object> paramMap){
		update("deleteQuestion", paramMap);
	}

	public void updateQuestion(Map<String, Object> paramMap){
		update("updateQuestion", paramMap);
	}
	
	
}
