package com.angryview.board.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import sun.util.logging.resources.logging; 

public interface BoardService {
	
	public List<Map<String, Object>> getNotiList(Map<String, Object> paramMap) throws SQLException;
	public String getNotiListCnt(Map<String, Object> paramMap);	
	public Map<String, Object> getNoti(Map<String, Object> paramMap) throws SQLException;
	public Map<String, Object> getContents(Map<String, Object> paramMap) throws SQLException;
	public List<Map<String, Object>> getQuestionList(Map<String, Object> paramMap) throws SQLException;
	public String getQuestionListCnt(Map<String, Object> paramMap);
	public Map<String, Object> getQuestionDetail(Map<String, Object> paramMap) throws SQLException;
	public void insertQuestion(Map<String, Object> paramMap) throws SQLException;
	public void deleteQuestion(Map<String, Object> paramMap) throws SQLException;
	public void updateQuestion(Map<String, Object> paramMap) throws SQLException;

	
}
