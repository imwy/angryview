package com.angryview.board.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.angryview.board.dao.BoardDao;

@Service("boardService")
public class BoardServiceImpl implements BoardService{
	
	@Resource(name="boardDao")
    private BoardDao boardDao;

	public List<Map<String, Object>> getNotiList(Map<String, Object> paramMap) throws SQLException {
		return boardDao.getNotiList(paramMap);
	}
	
	public String getNotiListCnt(Map<String, Object> paramMap) {
		return boardDao.getNotiListCnt(paramMap);
	}

	public Map<String, Object> getNoti(Map<String, Object> paramMap) throws SQLException {
		return boardDao.getNoti(paramMap);
	}
	
	
	public Map<String, Object> getContents(Map<String, Object> paramMap) throws SQLException {
		return boardDao.getContents(paramMap);
	}
	
	public List<Map<String, Object>> getQuestionList(Map<String, Object> paramMap) throws SQLException {
		return boardDao.getQuestionList(paramMap);
	}
	
	public String getQuestionListCnt(Map<String, Object> paramMap) {
		return boardDao.getQuestionListCnt(paramMap);
	}
	
	public Map<String, Object> getQuestionDetail(Map<String, Object> paramMap) throws SQLException {
		return boardDao.getQuestionDetail(paramMap);
	}

	public void insertQuestion(Map<String, Object> paramMap){
		boardDao.insertQuestion(paramMap);
	}
	
	public void deleteQuestion(Map<String, Object> paramMap){
		boardDao.deleteQuestion(paramMap);
	}
	
	public void updateQuestion(Map<String, Object> paramMap){
		boardDao.updateQuestion(paramMap);
	}
	
}
