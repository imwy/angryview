package com.angryview.board.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.angryview.board.service.BoardService;
import com.angryview.common.CommonConstants;
import com.angryview.util.HashUtil;


@Controller
@RequestMapping("/*/")
public class BoardController {

	private Logger log = Logger.getLogger(this.getClass());
	
	@Resource(name = "boardService")
	private BoardService boardService;

	
	/**
	 * 공지사항 목록 불러오기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("board/getNotiList.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getNotiList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> boardList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String lastRecvNotiID = request.getParameter("lastRecvNotiID") == null ? "" : request.getParameter("lastRecvNotiID").toString();
		String reqCount = request.getParameter("reqCount") == null ? "" : request.getParameter("reqCount").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		String baseURL = "";
		
		String boardListCnt = "0";

		
		try {
			if("".equals(memberID) || "".equals(lastRecvNotiID) || "".equals(reqCount)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				if(lastRecvNotiID.equals("1")) {
					resultCode = "0800";
					resultMessage = "마지막";
				}else {
					
					if(lastRecvNotiID.equals("first")) {
						lastRecvNotiID = "1";
					}else {
						lastRecvNotiID = String.valueOf(Double.parseDouble(lastRecvNotiID) + 1);
					}
					
					paramMap.put("memberNo", memberID);
					paramMap.put("lastRecvNotiID", lastRecvNotiID);
					paramMap.put("reqCount", reqCount);
					
					boardList = boardService.getNotiList(paramMap);
					
					boardListCnt = boardService.getNotiListCnt(paramMap);
					
					model.addAttribute("boardListCnt", boardListCnt);

			
					paramMap.clear();
			
					if(boardList != null){
						resultCode = "0000";
						resultMessage = "성공";
						baseURL = CommonConstants.URL;
						
						model.addAttribute("baseURL", baseURL);
	
						//log.info("boardList : " + boardList);
						model.addAttribute("itemList", boardList);
			
					}else{
						resultCode = "0001";
						resultMessage = "요청한 데이터 없음";
					}
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	/**
	 * 공지사항 불러오기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("board/getNoti.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getNoti(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> boardMap = new HashMap<String, Object>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String seq = request.getParameter("seq") == null ? "" : request.getParameter("seq").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		String baseURL = "";
		
		try {
			if("".equals(memberID) || "".equals(seq) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("memberNo", memberID);
				paramMap.put("seq", seq);
				
				boardMap = boardService.getNoti(paramMap);
		
				paramMap.clear();
		
				if(boardMap != null){
					resultCode = "0000";
					resultMessage = "성공";
	
					model.addAttribute("baseURL", baseURL);

					//log.info("boardMap : " + boardMap);
					model.addAttribute("itemMap", boardMap);


		
				}else{
					resultCode = "0001";
					resultMessage = "요청한 데이터 없음";
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	

	
	/**
	 * 오픈 라이선스 불러오기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("board/getContents.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getContents(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> itemMap = new HashMap<String, Object>();

		//요청
		String boardType = request.getParameter("boardType") == null ? "" : request.getParameter("boardType").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if("".equals(boardType)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				paramMap.put("boardType", boardType);
				
				itemMap = boardService.getContents(paramMap);
		
				paramMap.clear();
		
				if(itemMap != null){
					resultCode = "0000";
					resultMessage = "성공";
	
					//log.info("itemMap : " + itemMap);
					model.addAttribute("itemMap", itemMap);
		
				}else{
					resultCode = "0001";
					resultMessage = "요청한 데이터 없음";
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	
	
	/**
	 * 문의 내역 리스트 불러오기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("board/getQuestionList.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getQuestionList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> questionList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String lastRecvQuestionID = request.getParameter("lastRecvQuestionID") == null ? "" : request.getParameter("lastRecvQuestionID").toString();
		String reqQuestionCount = request.getParameter("reqQuestionCount") == null ? "" : request.getParameter("reqQuestionCount").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String questionListCnt = "0";

		
		try {
			if("".equals(memberID) || "".equals(lastRecvQuestionID) || "".equals(reqQuestionCount)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				if(lastRecvQuestionID.equals("1")) {
					resultCode = "0800";
					resultMessage = "마지막";
				}else {
					
					if(lastRecvQuestionID.equals("first")) {
						lastRecvQuestionID = "1";
					}else {
						lastRecvQuestionID = String.valueOf(Double.parseDouble(lastRecvQuestionID) + 1);
					}
					
					
					paramMap.put("memberNo", memberID);
					paramMap.put("lastRecvQuestionID", lastRecvQuestionID);
					paramMap.put("reqQuestionCount", reqQuestionCount);
					
					questionList = boardService.getQuestionList(paramMap);
					questionListCnt = boardService.getQuestionListCnt(paramMap);
					
					paramMap.clear();
			
					if(questionList != null){
						resultCode = "0000";
						resultMessage = "성공";
		
						//log.info("questionList : " + questionList);
						model.addAttribute("itemList", questionList);
						model.addAttribute("itemListCnt", questionListCnt);
			
					}else{
						resultCode = "0001";
						resultMessage = "요청한 데이터 없음";
					}
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	/**
	 * 문의사항 - 상세보기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("board/getQuestionDetail.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getQuestionDetail(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> itemMap = new HashMap<String, Object>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String seq = request.getParameter("seq") == null ? "" : request.getParameter("seq").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if("".equals(memberID) || "".equals(seq) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("memberNo", memberID);
				paramMap.put("seq", seq);
				
				itemMap = boardService.getQuestionDetail(paramMap);
		
				paramMap.clear();
		
				if(itemMap != null){
					resultCode = "0000";
					resultMessage = "성공";
	
					//log.info("itemMap : " + itemMap);
					model.addAttribute("itemMap", itemMap);


		
				}else{
					resultCode = "0001";
					resultMessage = "요청한 데이터 없음";
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	/**
	 * 질문 입력
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("board/insertQuestion.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String insertQuestion(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> boardList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String writerNick = request.getParameter("writerNick") == null ? "" : request.getParameter("writerNick").toString();
		String title = request.getParameter("title") == null ? "" : request.getParameter("title").toString();
		String content = request.getParameter("content") == null ? "" : request.getParameter("content").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if("".equals(memberID) || "".equals(writerNick) || "".equals(title) || "".equals(content) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("memberNo", memberID);
				paramMap.put("writerNick", writerNick);
				paramMap.put("title", title);
				paramMap.put("content", content);
				
				boardService.insertQuestion(paramMap);
		
				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	
	/**
	 * 질문 삭제(상태값 D로 UPDATE)
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("board/deleteQuestion.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String deleteQuestion(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> itemMap = new HashMap<String, Object>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String seq = request.getParameter("seq") == null ? "" : request.getParameter("seq").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if("".equals(memberID) || "".equals(seq) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("memberNo", memberID);
				paramMap.put("seq", seq);
				
				boardService.deleteQuestion(paramMap);
				paramMap.clear();
	
				resultCode = "0000";
				resultMessage = "성공";
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	/**
	 * 질문 수정
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("board/updateQuestion.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String updateQuestion(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> boardList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String writerNick = request.getParameter("writerNick") == null ? "" : request.getParameter("writerNick").toString();
		String title = request.getParameter("title") == null ? "" : request.getParameter("title").toString();
		String content = request.getParameter("content") == null ? "" : request.getParameter("content").toString();
		String seq = request.getParameter("seq") == null ? "" : request.getParameter("seq").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if("".equals(memberID) || "".equals(writerNick) || "".equals(title) || "".equals(content) || "".equals(seq)  ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("memberNo", memberID);
				paramMap.put("writerNick", writerNick);
				paramMap.put("title", title);
				paramMap.put("content", content);
				paramMap.put("seq", seq);
				
				boardService.updateQuestion(paramMap);

				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	

}