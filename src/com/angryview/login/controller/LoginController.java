package com.angryview.login.controller;

import java.io.BufferedReader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.angryview.login.service.LoginService;
import com.angryview.util.AesUtil;
import com.angryview.util.HashUtil;

import org.apache.commons.codec.binary.Base64;




@Controller
@RequestMapping("/*/")
public class LoginController {

	private Logger log = Logger.getLogger(this.getClass());
	
	@Resource(name = "loginService")
	private LoginService loginService;
	

	
	/**
	 * 로그인
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("login/doLogin.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String doLogin(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> itemMap = new HashMap<String, Object>();

		//요청
		String email = request.getParameter("email") == null ? "" : request.getParameter("email").toString();
		String password = request.getParameter("password") == null ? "" : request.getParameter("password").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		HashUtil hu = new HashUtil();

		try {
			if("".equals(email) || "".equals(password)  ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("email", email);
				//paramMap.put("password", password);
				paramMap.put("password", hu.hashEncode(password));	// HASH-512 ENCODE


				itemMap = loginService.doLogin(paramMap);
		
				paramMap.clear();
		
				if(itemMap != null){
					resultCode = "0000";
					resultMessage = "성공";

					//System.out.println("memberID=[" + itemMap.get("memberID") + "]");
					//System.out.println("isSetConcernTopic=[" + itemMap.get("isSetConcernTopic") + "]");
					
					model.addAttribute("memberID", itemMap.get("memberID"));
					model.addAttribute("isSetConcernTopic", itemMap.get("isSetConcernTopic"));


				}else{
					resultCode = "0700";
					resultMessage = "로그인 실패";
				}
				

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	

	/**
	 * 로그인
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("login/doLogin2.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String doLogin2(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> itemMap = new HashMap<String, Object>();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String data = request.getParameter("data") == null ? "" : request.getParameter("data").toString();
		try {
			
			//System.out.println("###data###["+data+"]###");
			
			AesUtil au = new AesUtil();
			data = au.decrypt(data);
			//System.out.println("###DECdata###["+data+"]###");

	
		    JSONObject jObject = new JSONObject(data);
		    
		    String email = jObject.getString("email");
		    String password = jObject.getString("password");
		    
			HashUtil hu = new HashUtil();

//			System.out.println("###email###["+email+"]###");
//			System.out.println("###password###["+password+"]###");
//			System.out.println("###HASHpassword###["+hu.hashEncode(password)+"]###");

	
	/*
			//요청
			String email = request.getParameter("email") == null ? "" : request.getParameter("email").toString();
			String password = request.getParameter("password") == null ? "" : request.getParameter("password").toString();
	*/

			

	
			if("".equals(email) || "".equals(password)  ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("email", email);
				paramMap.put("password", hu.hashEncode(password));	// HASH-512 ENCODE

				itemMap = loginService.doLogin(paramMap);
		
				paramMap.clear();
		
				if(itemMap != null){
					resultCode = "0000";
					resultMessage = "성공";

//					System.out.println("memberID=[" + itemMap.get("memberID") + "]");
//					System.out.println("isSetConcernTopic=[" + itemMap.get("isSetConcernTopic") + "]");
					
					model.addAttribute("memberID", itemMap.get("memberID"));
					model.addAttribute("isSetConcernTopic", itemMap.get("isSetConcernTopic"));


				}else{
					resultCode = "0700";
					resultMessage = "로그인 실패";
				}
				

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	
	/**
	 * 로그인 SNS
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("login/doLoginSNS.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String doLoginSNS(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> itemMap = new HashMap<String, Object>();

		//요청
		String email = request.getParameter("email") == null ? "" : request.getParameter("email").toString();
		String snsId = request.getParameter("snsId") == null ? "" : request.getParameter("snsId").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";


		try {
			if("".equals(email) || "".equals(snsId)  ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("email", email);
				paramMap.put("snsId", snsId);


				itemMap = loginService.doLoginSNS(paramMap);
		
				paramMap.clear();
		
				if(itemMap != null){
					resultCode = "0000";
					resultMessage = "성공";

//					System.out.println("memberID=[" + itemMap.get("memberID") + "]");
//					System.out.println("isSetConcernTopic=[" + itemMap.get("isSetConcernTopic") + "]");
					
					model.addAttribute("memberID", itemMap.get("memberID"));
					model.addAttribute("isSetConcernTopic", itemMap.get("isSetConcernTopic"));


				}else{
					resultCode = "0700";
					resultMessage = "로그인 실패";
				}
				

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	public static void main(String[] args) {

		try {
			
			String jsonString = "{\"email\":\"iamwooyong@gmail.com\",\"password\":\"1234567890\"}";

			AesUtil au = new AesUtil();

			byte[] key = "0123456789abcdef".getBytes();


			String encrypt = au.encrypt(jsonString);
			//System.out.println("EncryptAES128 : " + encrypt);
		    //System.out.println("DecryptAES128 : " + au.decrypt(encrypt));

			/*
			String passwd = "asdfghjkl";
			HashUtil hu = new HashUtil();

			
			System.out.println(hu.hashEncode(passwd));
		*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

	
}