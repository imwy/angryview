package com.angryview.login.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import sun.util.logging.resources.logging; 

public interface LoginService {
	

	public Map<String, Object> doLogin(Map<String, Object> paramMap) throws SQLException;

	public Map<String, Object> doLoginSNS(Map<String, Object> paramMap) throws SQLException;

	
	
}
