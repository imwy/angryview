package com.angryview.login.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.angryview.login.dao.LoginDao;

@Service("loginService")
public class LoginServiceImpl implements  LoginService{
	
	@Resource(name="loginDao")
    private LoginDao loginDao;



	
	public Map<String, Object> doLogin(Map<String, Object> paramMap) throws SQLException{
		return loginDao.doLogin(paramMap);
	}

	public Map<String, Object> doLoginSNS(Map<String, Object> paramMap) throws SQLException{
		return loginDao.doLoginSNS(paramMap);
	}

	
	
}
