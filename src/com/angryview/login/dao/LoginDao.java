package com.angryview.login.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.angryview.common.iBatisDaoSupport;

@Repository("loginDao")
public class LoginDao extends iBatisDaoSupport {
	

	public Map<String, Object> doLogin(Map<String, Object> paramMap){
		return (Map<String, Object>)select("doLogin", paramMap);
	}
	
	public Map<String, Object> doLoginSNS(Map<String, Object> paramMap){
		return (Map<String, Object>)select("doLoginSNS", paramMap);
	}
	
	
}
