package com.angryview.member.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import sun.util.logging.resources.logging; 

public interface MemberService {
	
	public String checkDuplicatedID(Map<String, Object> paramMap) throws SQLException;
	public String checkDuplicatedNickName(Map<String, Object> paramMap) throws SQLException;
	public String checkDuplicatedSnsId(Map<String, Object> paramMap) throws SQLException;
	public String checkDuplicatedPhone(Map<String, Object> paramMap) throws SQLException;
	
	
	
	public void reqSignIn(Map<String, Object> paramMap) throws SQLException;
	public void reqSignInSNS(Map<String, Object> paramMap) throws SQLException;

	public void updateMyInfo(Map<String, Object> paramMap) throws SQLException;
	public void updatePhone(Map<String, Object> paramMap) throws SQLException;
	public void updateEvent(Map<String, Object> paramMap) throws SQLException;
	public void updateInterestReviewAlerm(Map<String, Object> paramMap) throws SQLException;
	public Map<String, Object> getMyInfo(Map<String, Object> paramMap) throws SQLException;
	public Map<String, Object> getMemberID(Map<String, Object> paramMap) throws SQLException;

	public void deleteSMS(Map<String, Object> paramMap) throws SQLException;
	public void insertSMS(Map<String, Object> paramMap) throws SQLException;
	public String getSMS(Map<String, Object> paramMap) throws SQLException;

	public int deleteMember(Map<String, Object> paramMap) throws SQLException;
	public int updateMember(Map<String, Object> paramMap) throws SQLException;
	
	
	public Map<String, Object> findId(Map<String, Object> paramMap) throws SQLException;
	public void updatePassword(Map<String, Object> paramMap) throws SQLException;
	
	public String checkSNS(Map<String, Object> paramMap) throws SQLException;
	public String checkPhone(Map<String, Object> paramMap) throws SQLException;

	

	
	
}
