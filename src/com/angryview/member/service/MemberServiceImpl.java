package com.angryview.member.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.angryview.member.dao.MemberDao;

@Service("memberService")
public class MemberServiceImpl implements MemberService{
	
	@Resource(name="memberDao")
    private MemberDao memberDao;

	public String checkDuplicatedID(Map<String, Object> paramMap) throws SQLException {
		return memberDao.checkDuplicatedID(paramMap);
	}
	
	public String checkDuplicatedNickName(Map<String, Object> paramMap) throws SQLException {
		return memberDao.checkDuplicatedNickName(paramMap);
	}

	public String checkDuplicatedSnsId(Map<String, Object> paramMap) throws SQLException {
		return memberDao.checkDuplicatedSnsId(paramMap);
	}
	
	public String checkDuplicatedPhone(Map<String, Object> paramMap) throws SQLException {
		return memberDao.checkDuplicatedPhone(paramMap);
	}
	
	
	public void reqSignIn(Map<String, Object> paramMap){
		memberDao.reqSignIn(paramMap);
	}
	
	public void reqSignInSNS(Map<String, Object> paramMap){
		memberDao.reqSignInSNS(paramMap);
	}
	
	
	public void updateMyInfo(Map<String, Object> paramMap){
		memberDao.updateMyInfo(paramMap);
	}
	
	public void updatePhone(Map<String, Object> paramMap){
		memberDao.updatePhone(paramMap);
	}
		
	public void updateEvent(Map<String, Object> paramMap){
		memberDao.updateEvent(paramMap);
	}
	public void updateInterestReviewAlerm(Map<String, Object> paramMap){
		memberDao.updateInterestReviewAlerm(paramMap);
	}
	
	public Map<String, Object> getMyInfo(Map<String, Object> paramMap) throws SQLException {
		return memberDao.getMyInfo(paramMap);
	}
	
	public Map<String, Object> getMemberID(Map<String, Object> paramMap) throws SQLException {
		return memberDao.getMemberID(paramMap);
	}
	
	
	public void deleteSMS(Map<String, Object> paramMap){
		memberDao.deleteSMS(paramMap);
	}
	public void insertSMS(Map<String, Object> paramMap){
		memberDao.insertSMS(paramMap);
	}
	
	public String getSMS(Map<String, Object> paramMap) throws SQLException {
		return memberDao.getSMS(paramMap);
	}
	
	public int deleteMember(Map<String, Object> paramMap){
		return (int) memberDao.deleteMember(paramMap);
	}
	public int updateMember(Map<String, Object> paramMap){
		return (int) memberDao.updateMember(paramMap);
	}
	
	public Map<String, Object> findId(Map<String, Object> paramMap) throws SQLException {
		return memberDao.findId(paramMap);
	}
	

	
	public void updatePassword(Map<String, Object> paramMap){
		memberDao.updatePassword(paramMap);
	}
	
	public String checkSNS(Map<String, Object> paramMap) throws SQLException {
		return memberDao.checkSNS(paramMap);
	}
	
	public String checkPhone(Map<String, Object> paramMap) throws SQLException {
		return memberDao.checkPhone(paramMap);
	}
	
	
	
}
