package com.angryview.member.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.angryview.common.CommonConstants;
import com.angryview.member.service.MemberService;
import com.angryview.util.HashUtil;
import com.angryview.util.SendUtil;


@Controller
@RequestMapping("/*/")
public class MemberController {

	private Logger log = Logger.getLogger(this.getClass());
	
	@Resource(name = "memberService")
	private MemberService memberService;

	
	/**
	 * 아이디 중복확인
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/checkDuplicatedID.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String checkDuplicatedID(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		String cnt = "";

		//요청
		String email = request.getParameter("email") == null ? "" : request.getParameter("email").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if("".equals(email) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("email", email);
				
				cnt = memberService.checkDuplicatedID(paramMap);
		
				paramMap.clear();
		
				if(cnt != null && cnt.equals("0")){
					resultCode = "0000";
					resultMessage = "사용가능한 아이디";
					
					//log.info("cnt : " + cnt);
		
				}else{
					resultCode = "0600";
					resultMessage = "중복된 아이디";
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	/**
	 * 닉네임 중복확인
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/checkDuplicatedNickName.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String checkDuplicatedNickName(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		String cnt = "";

		//요청
		String nickName = request.getParameter("nickName") == null ? "" : request.getParameter("nickName").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if("".equals(nickName) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("nickName", nickName);
				
				cnt = memberService.checkDuplicatedNickName(paramMap);
		
				paramMap.clear();
		
				if(cnt != null && cnt.equals("0")){
					resultCode = "0000";
					resultMessage = "사용가능한 닉네임";
					
					//log.info("cnt : " + cnt);
		
				}else{
					resultCode = "0600";
					resultMessage = "중복된 닉네임";
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	

	/**
	 * SNS ID 중복확인
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/checkDuplicatedSnsId.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String checkDuplicatedSnsId(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		String cnt = "";

		//요청
		String snsId = request.getParameter("snsId") == null ? "" : request.getParameter("snsId").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if("".equals(snsId) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("snsId", snsId);
				
				cnt = memberService.checkDuplicatedSnsId(paramMap);
		
				paramMap.clear();
		
				if(cnt != null && cnt.equals("0")){
					resultCode = "0000";
					resultMessage = "사용가능한 SNS ID";
					
					//log.info("cnt : " + cnt);
		
				}else{
					resultCode = "0600";
					resultMessage = "중복된 SNS ID";
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}

	/**
	 * 회원가입
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/reqSignIn.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String reqSignIn(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String email = request.getParameter("email") == null ? "" : request.getParameter("email").toString();
		String nickName = request.getParameter("nickName") == null ? "" : request.getParameter("nickName").toString();
		String password = request.getParameter("password") == null ? "" : request.getParameter("password").toString();
		String sex = request.getParameter("sex") == null ? "" : request.getParameter("sex").toString();
		String birth = request.getParameter("birth") == null ? "" : request.getParameter("birth").toString();
		String certificationYn = request.getParameter("certificationYn") == null ? "" : request.getParameter("certificationYn").toString();
		String phone = request.getParameter("phone") == null ? "" : request.getParameter("phone").toString();

			
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String emailCnt = "0";
		String nickNameCnt = "0";
		String phoneCnt = "0";
		
		try {
			if("".equals(email) || "".equals(nickName) || "".equals(password) || "".equals(sex)
					|| "".equals(birth) || "".equals(certificationYn) || "".equals(phone) ) {
				
				resultMessage = "필수 파라미터 누락 ";
				
				if("".equals(email)) {
					resultMessage = "email 누락";
				}
				if("".equals(nickName)) {
					resultMessage = "닉네임 누락";
				}
				if("".equals(password)) {
					resultMessage = "비밀번호 누락";
				}
				if("".equals(sex)) {
					resultMessage = "성별 누락";
				}
				if("".equals(birth)) {
					resultMessage = "생년월일 누락";
				}
				if("".equals(phone)) {
					resultMessage = "phone 누락";
				}
				
				resultCode = "0101";
				
			}else {
				
				if(!"".equals(email)) {
					
					paramMap.put("email", email);
				
					emailCnt = memberService.checkDuplicatedNickName(paramMap);
				}
				
				if(!"".equals(nickName)) {
					
					paramMap.put("nickName", nickName);
				
					nickNameCnt = memberService.checkDuplicatedID(paramMap);
				}
				
				
				if(!"".equals(phone)) {
					
					paramMap.put("phone", phone);
				
					phoneCnt = memberService.checkDuplicatedPhone(paramMap);
				}
				
				
				String pattern = "^[ㄱ-ㅎ가-힣a-zA-Z0-9]*$";
				boolean result = Pattern.matches(pattern, password);
				
				if(result) {
					
					resultCode = "0600";
					resultMessage = "패스워드 특수문자 미포함 입니다.";
					
				}else {

					if( emailCnt != null && !emailCnt.equals("0")){
							
						resultCode = "0600";
						resultMessage = "중복된 아이디 입니다.";	
						
					}else if( nickNameCnt != null && !nickNameCnt.equals("0")) {
						
						resultCode = "0600";
						resultMessage = "중복된 닉네임 입니다.";
						
					}else if( phoneCnt != null && !phoneCnt.equals("0")) {
						
						resultCode = "0600";
						resultMessage = "사용중인 핸드폰번호 입니다.";
						 
					}else {
		
						HashUtil hu = new HashUtil();
		
						password = hu.hashEncode(password);
					
						paramMap.put("email", email);
						paramMap.put("nickName", nickName);
						paramMap.put("password", password);
						paramMap.put("sex", sex);
						paramMap.put("birth", birth);
						paramMap.put("certificationYn", certificationYn);
						paramMap.put("phone", phone);
						
						memberService.reqSignIn(paramMap);
				
						paramMap.clear();
				
						resultCode = "0000";
						resultMessage = "성공";
					}
				}

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	

	/**
	 * SNS 회원가입
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/reqSignInSNS.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String reqSignInSNS(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String email = request.getParameter("email") == null ? "" : request.getParameter("email").toString();
		String nickName = request.getParameter("nickName") == null ? "" : request.getParameter("nickName").toString();
		String sex = request.getParameter("sex") == null ? "" : request.getParameter("sex").toString();
		String birth = request.getParameter("birth") == null ? "" : request.getParameter("birth").toString();
		String phone = request.getParameter("phone") == null ? "" : request.getParameter("phone").toString();
		String snsId = request.getParameter("snsId") == null ? "" : request.getParameter("snsId").toString();

		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String emailCnt = "0";
		String nickNameCnt = "0";
		String snsCnt = "0";
		String phoneCnt = "0";
		
		
		try {
			if("".equals(email) || "".equals(nickName) || "".equals(sex)
					|| "".equals(birth) || "".equals(snsId) || "".equals(phone) ) {
				
				resultMessage = "필수 파라미터 누락 ";
				
				if("".equals(email)) {
					resultMessage = "email 누락";
				}
				if("".equals(nickName)) {
					resultMessage = "닉네임 누락";
				}
				if("".equals(snsId)) {
					resultMessage = "SNS ID 누락";
				}
				if("".equals(sex)) {
					resultMessage = "성별 누락";
				}
				if("".equals(birth)) {
					resultMessage = "생년월일 누락";
				}
				if("".equals(phone)) {
					resultMessage = "phone 누락";
				}
				
				resultCode = "0101";
				
			}else {
				
				if(!"".equals(email)) {
					
					paramMap.put("email", email);
				
					emailCnt = memberService.checkDuplicatedNickName(paramMap);
				}
				
				if(!"".equals(nickName)) {
					
					paramMap.put("nickName", nickName);
				
					nickNameCnt = memberService.checkDuplicatedID(paramMap);
				}
				
				if(!"".equals(snsId)) {
					
					paramMap.put("snsId", snsId);
				
					snsCnt = memberService.checkDuplicatedSnsId(paramMap);
				}
				
				if(!"".equals(phone)) {
					
					paramMap.put("phone", phone);
				
					phoneCnt = memberService.checkDuplicatedPhone(paramMap);
				}
	
				
				if( emailCnt != null && !emailCnt.equals("0")){
				
					resultCode = "0600";
					resultMessage = "중복된 이메일 주소 입니다.";
				
				}else if(nickNameCnt != null && !nickNameCnt.equals("0")){
				
					resultCode = "0600";
					resultMessage = "중복된 닉네임 입니다.";
				
				}else if(snsCnt != null && !snsCnt.equals("0")){
				
					resultCode = "0600";
					resultMessage = "중복된 SNS_ID 입니다.";
				
				}else if(phoneCnt != null && !phoneCnt.equals("0")){
				
					resultCode = "0600";
					resultMessage = "중복된 전화번호 입니다.";
				
				}else {
	
					HashUtil hu = new HashUtil();
				
					paramMap.put("email", email);
					paramMap.put("nickName", nickName);
					paramMap.put("snsId", snsId);
					paramMap.put("sex", sex);
					paramMap.put("birth", birth);
					paramMap.put("certificationYn", "N");
					paramMap.put("phone", phone);
					
					memberService.reqSignInSNS(paramMap);
			
					paramMap.clear();
			
					resultCode = "0000";
					resultMessage = "성공";
				}


			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	

	/**
	 * 내 정보 수정
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/updateMyInfo.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String updateMyInfo(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String nickName = request.getParameter("nickName") == null ? "" : request.getParameter("nickName").toString();
		String password = request.getParameter("password") == null ? "" : request.getParameter("password").toString();
		String postNo = request.getParameter("postNo") == null ? "" : request.getParameter("postNo").toString();
		String address1 = request.getParameter("address1") == null ? "" : request.getParameter("address1").toString();
		String address2 = request.getParameter("address2") == null ? "" : request.getParameter("address2").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";

		String cnt = "0";
		
		String check  = "1";
		
		HashUtil hu = new HashUtil();
		
		try {
			if( "".equals(memberID) ) {
				
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
				
			}else {
				
				//닉네임 중복 확인
				if(!"".equals(nickName)) {
				
					paramMap.put("nickName", nickName);
				
					cnt = memberService.checkDuplicatedNickName(paramMap);
				}

				if(cnt != null && cnt.equals("0")){
					
					
					//패스워드 있음
					if(!"".equals(password)) {
						check  = "0";
						String pattern = "^[ㄱ-ㅎ가-힣a-zA-Z0-9]*$";
						boolean result = Pattern.matches(pattern, password);
						
						if(result) {
							resultCode = "0600";
							resultMessage = "패스워드 특수문자 미포함 입니다.";
						}else {
							password = hu.hashEncode(password);
							check  = "1";

						}
					}
					
					
					if("1".equals(check)) {
					
						paramMap.put("memberID", memberID);
						paramMap.put("nickName", nickName);
						paramMap.put("password", password);
						paramMap.put("postNo", postNo);
						paramMap.put("address1", address1);
						paramMap.put("address2", address2);
						
						memberService.updateMyInfo(paramMap);
				
						paramMap.clear();
				
						resultCode = "0000";
						resultMessage = "성공";
					}
					
				}else{
					resultCode = "0600";
					resultMessage = "중복된 닉네임";
				}

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	

	/**
	 * 전화번호 수정
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/updatePhone.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String updatePhone(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String phone = request.getParameter("phone") == null ? "" : request.getParameter("phone").toString();

		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if( "".equals(memberID) || "".equals(phone) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("memberID", memberID);
				paramMap.put("phone", phone);

				memberService.updatePhone(paramMap);
		
				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	/**
	 * 이벤트 혜택 알림여부 수정
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/updateEvent.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String updateEvent(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String eventYn = request.getParameter("eventYn") == null ? "" : request.getParameter("eventYn").toString();

		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if( "".equals(memberID) || "".equals(eventYn) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("memberID", memberID);
				paramMap.put("eventYn", eventYn);

				memberService.updateEvent(paramMap);
		
				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	

	/**
	 * 관심 리뷰 알림 여부 수정
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/updateInterestReviewAlerm.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String updateInterestReviewAlerm(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String interestReviewAlermYn = request.getParameter("interestReviewAlermYn") == null ? "" : request.getParameter("interestReviewAlermYn").toString();

		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if( "".equals(memberID) || "".equals(interestReviewAlermYn) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("memberID", memberID);
				paramMap.put("interestReviewAlermYn", interestReviewAlermYn);

				memberService.updateInterestReviewAlerm(paramMap);
		
				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	/**
	 * 내 정보 가져오기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/getMyInfo.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getMyInfo(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> itemMap = new HashMap<String, Object>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if("".equals(memberID) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("memberID", memberID);
				
				itemMap = memberService.getMyInfo(paramMap);
		
				paramMap.clear();
		
				if(itemMap != null){
					resultCode = "0000";
					resultMessage = "성공";
	
					//log.info("itemMap : " + itemMap);
					model.addAttribute("itemMap", itemMap);


		
				}else{
					resultCode = "0001";
					resultMessage = "요청한 데이터 없음";
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	

	/**
	 * SNS ID로 memberID 가져오기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/getMemberID.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getMemberID(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> itemMap = new HashMap<String, Object>();

		//요청
		String snsId = request.getParameter("snsId") == null ? "" : request.getParameter("snsId").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if("".equals(snsId) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				paramMap.put("snsId", snsId);
				
				itemMap = memberService.getMemberID(paramMap);
		
				paramMap.clear();
		
				if(itemMap != null){
					resultCode = "0000";
					resultMessage = "성공";
	
					//log.info("itemMap : " + itemMap);
				//	model.addAttribute("itemMap", itemMap);
					
//					System.out.println("memberID=[" + itemMap.get("memberID") + "]");
//					System.out.println("isSetConcernTopic=[" + itemMap.get("isSetConcernTopic") + "]");
					
					model.addAttribute("memberID", itemMap.get("memberID"));
					model.addAttribute("isSetConcernTopic", itemMap.get("isSetConcernTopic"));
		
				}else{
					resultCode = "0001";
					resultMessage = "요청한 데이터 없음";
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	/**
	 * 회원가입 SMS 인증
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/reqNewSMS.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String reqNewSMS(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String phone = request.getParameter("phone") == null ? "" : request.getParameter("phone").toString();
			
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		String text = "";
		
		String phoneCnt = "";
		
		try {
			if("".equals(phone)) {
				resultMessage = "필수 파라미터 누락 ";
				
			}else {
				
				
				paramMap.put("phone", phone);
				
				phoneCnt = memberService.checkDuplicatedPhone(paramMap);
				
				
				
				if( phoneCnt != null && !phoneCnt.equals("0")) {
										
					resultCode = "0600";
					resultMessage = "사용중인 핸드폰번호 입니다.";
					 
				}else {
					
					paramMap.put("phone", phone);
					memberService.deleteSMS(paramMap);
					
					Random rd = new Random();
					
					for(int i=0; i<6; i++) {
						text = text + Integer.toString(rd.nextInt(9)+1);
			        }
//					System.out.println("text="+text);
					
					paramMap.put("text", text);
					memberService.insertSMS(paramMap);
					
					
					String code = SendUtil.SendSMS(phone, "인증번호["+text+"]를 입력하세요.");
					paramMap.clear();
	
					if("1".equals(code)) {
						resultCode = "0000";
						resultMessage = "요청 성공";
					}else {
						resultCode = "0002";
						resultMessage = "SMS 요청 실패";
					}
					
					
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	

	/**
	 * SMS 인증
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/reqSMS.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String reqSMS(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String phone = request.getParameter("phone") == null ? "" : request.getParameter("phone").toString();
			
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		String text = "";
		
		String checkSNS = "";
		String checkPhone = "";


		
		try {
			if("".equals(phone)) {
				resultMessage = "필수 파라미터 누락 ";
				
			}else {
				
				paramMap.put("phone", phone);

				checkSNS = memberService.checkSNS(paramMap);
				
//				System.out.println("checkSNS=="+checkSNS);
				
				if( checkSNS != null && !checkSNS.equals("") ) {
					resultCode = "0001";
					resultMessage = "SNS 회원 패스워드 초기화 불가";
					
				}else {
					
					
					paramMap.put("phone", phone);

					checkPhone = memberService.checkPhone(paramMap);
					

					if( checkPhone == null || checkPhone.equals("") ) {
						resultCode = "0001";
						resultMessage = "저장된 핸드폰 번호가 아닙니다.";
						
					}else {
					
						paramMap.put("phone", phone);
						memberService.deleteSMS(paramMap);
						
						Random rd = new Random();
						
						for(int i=0; i<6; i++) {
							text = text + Integer.toString(rd.nextInt(9)+1);
				        }
//						System.out.println("text="+text);
						
						paramMap.put("text", text);
						memberService.insertSMS(paramMap);
						
						
						String code = SendUtil.SendSMS(phone, "인증번호["+text+"]를 입력하세요.");
						paramMap.clear();
		
						if("1".equals(code)) {
							resultCode = "0000";
							resultMessage = "요청 성공";
						}else {
							resultCode = "0002";
							resultMessage = "SMS 요청 실패";
						}
					}
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	/**
	 * SMS 인증 확인
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/getSMS.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getSMS(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String phone = request.getParameter("phone") == null ? "" : request.getParameter("phone").toString();
		String text = request.getParameter("text") == null ? "" : request.getParameter("text").toString();
			
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String check = "";
		
		try {
			if("".equals(phone) || "".equals(text)) {
				resultMessage = "필수 파라미터 누락 ";
				
			}else {
				
				paramMap.put("phone", phone);
				check = memberService.getSMS(paramMap);
				
				if(check.equals(text)) {
					resultCode = "0000";
					resultMessage = "인증성공";
				}else {
					resultCode = "0001";
					resultMessage = "인증실패";
				}

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	/**
	 * 탈퇴
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/deleteMember.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String deleteMember(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String password = request.getParameter("password") == null ? "" : request.getParameter("password").toString();

		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		int check = 0;
		
		try {
			if( "".equals(memberID) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				HashUtil hu = new HashUtil();

				if(!"".equals(password)) {
					password = hu.hashEncode(password);
				}
				
				paramMap.put("memberID", memberID);
				paramMap.put("password", password);

			//	check = memberService.updateMember(paramMap);
				check = memberService.deleteMember(paramMap);
		
				paramMap.clear();
				
				if(check > 0) {
					resultCode = "0000";
					resultMessage = "성공";
				}else {
					resultCode = "0001";
					resultMessage = "비밀번호가 일치하지 않습니다.";
					
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	

	/**
	 * 아이디 찾기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/findId.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String findId(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String phone = request.getParameter("phone") == null ? "" : request.getParameter("phone").toString();
		String text = request.getParameter("text") == null ? "" : request.getParameter("text").toString();
			
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		Map<String, Object> itemMap = new HashMap<String, Object>();
		
		String check = "";
		
		
		

		try {
			if("".equals(phone) || "".equals(text)) {
				resultMessage = "필수 파라미터 누락 ";
				
			}else {
				
			
				
				paramMap.put("phone", phone);
				check = memberService.getSMS(paramMap);
				
				if(check.equals(text)) {
					
					itemMap = memberService.findId(paramMap);
					
					if(itemMap != null){
						resultCode = "0000";
						resultMessage = "성공";
		
						//log.info("itemMap : " + itemMap);
						model.addAttribute("itemMap", itemMap);
					}else {
						resultCode = "0001";
						resultMessage = "요청한 데이터 없음";	
					}
				}else {
					resultCode = "0001";
					resultMessage = "인증실패";
				}

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	/**
	 * 패스워드 찾기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("member/findPassword.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String findPassword(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String phone = request.getParameter("phone") == null ? "" : request.getParameter("phone").toString();
		String text = request.getParameter("text") == null ? "" : request.getParameter("text").toString();
		String email = request.getParameter("email") == null ? "" : request.getParameter("email").toString();
			
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String password  = "";

		HashUtil hu = new HashUtil();
		String check = "";
		
		String checkSNS = "";

		
		Map<String, Object> itemMap = new HashMap<String, Object>();

		
		try {
			if("".equals(phone) || "".equals(text)  || "".equals(email)) {
				resultMessage = "필수 파라미터 누락 ";
				
			}else {
				
				paramMap.put("phone", phone);

				checkSNS = memberService.checkSNS(paramMap);
				
				if( checkSNS != null && !checkSNS.equals("") ) {
					resultCode = "0001";
					resultMessage = "SNS회원입니다. sns으로 로그인하세요.";
					
				}else {
				
					paramMap.put("phone", phone);
					check = memberService.getSMS(paramMap);
					
					if(check.equals(text)) {
						
						Random rd = new Random();
						
						for(int i=0; i<8; i++) {
							password = password + Integer.toString(rd.nextInt(9)+1);
				        }
//						System.out.println("password="+password);
	
						paramMap.put("email", email);
						paramMap.put("phone", phone);
						paramMap.put("password", hu.hashEncode(password));
						
						memberService.updatePassword(paramMap);
						
						itemMap.put("password", password);
						itemMap.put("email", email);
						
						model.addAttribute("itemMap", itemMap);
						
						resultCode = "0000";
						resultMessage = "성공";
					}else {
						resultCode = "0001";
						resultMessage = "인증실패";
					}
				}

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	

}