package com.angryview.member.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.angryview.common.iBatisDaoSupport;

@Repository("memberDao")
public class MemberDao extends iBatisDaoSupport {
	

	public String checkDuplicatedID(Map<String, Object> paramMap) {
		return (String)select("checkDuplicatedID", paramMap);
	}
	
	public String checkDuplicatedNickName(Map<String, Object> paramMap) {
		return (String)select("checkDuplicatedNickName", paramMap);
	}
	
	public String checkDuplicatedSnsId(Map<String, Object> paramMap) {
		return (String)select("checkDuplicatedSnsId", paramMap);
	}
	
	public String checkDuplicatedPhone(Map<String, Object> paramMap) {
		return (String)select("checkDuplicatedPhone", paramMap);
	}
	
	
	
	public void reqSignIn(Map<String, Object> paramMap){
		insert("reqSignIn", paramMap);
	}
	
	public void reqSignInSNS(Map<String, Object> paramMap){
		insert("reqSignInSNS", paramMap);
	}
	
	public void updateMyInfo(Map<String, Object> paramMap){
		update("updateMyInfo", paramMap);
	}
	
	public void updatePhone(Map<String, Object> paramMap){
		update("updatePhone", paramMap);
	}

	public void updateEvent(Map<String, Object> paramMap){
		update("updateEvent", paramMap);
	}
	
	public void updateInterestReviewAlerm(Map<String, Object> paramMap){
		update("updateInterestReviewAlerm", paramMap);
	}
	
	public Map<String, Object> getMyInfo(Map<String, Object> paramMap){
		return (Map<String, Object>)select("getMyInfo", paramMap);
	}
	
	public Map<String, Object> getMemberID(Map<String, Object> paramMap){
		return (Map<String, Object>)select("getMemberID", paramMap);
	}
	
	public void deleteSMS(Map<String, Object> paramMap){
		delete("deleteSMS", paramMap);
	}
	
	public void insertSMS(Map<String, Object> paramMap){
		insert("insertSMS", paramMap);
	}
	
	public String getSMS(Map<String, Object> paramMap) {
		return (String)select("getSMS", paramMap);
	}
	
	
	public int updateMember(Map<String, Object> paramMap){
		return (int) update("updateMember", paramMap);
	}
	
	public int deleteMember(Map<String, Object> paramMap){
		return (int) delete("deleteMember", paramMap);
	}
	
	
	public Map<String, Object> findId(Map<String, Object> paramMap){
		return (Map<String, Object>)select("findId", paramMap);
	}
	

	public void updatePassword(Map<String, Object> paramMap){
		update("updatePassword", paramMap);
	}
	
	
	public String checkSNS(Map<String, Object> paramMap) {
		return (String)select("checkSNS", paramMap);
	}
	
	public String checkPhone(Map<String, Object> paramMap) {
		return (String)select("checkPhone", paramMap);
	}
	
	
}
