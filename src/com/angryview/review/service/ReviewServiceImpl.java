package com.angryview.review.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.angryview.review.dao.ReviewDao;

@Service("reviewService")
public class ReviewServiceImpl implements ReviewService{
	
	@Resource(name="reviewDao")
    private ReviewDao reviewDao;

	public List<Map<String, Object>> reqCategoryList(Map<String, Object> paramMap) throws SQLException {
		return reviewDao.reqCategoryList(paramMap);
	}
	
	public List<Map<String, Object>> getBrandAutoCompleteList(Map<String, Object> paramMap) throws SQLException {
		return reviewDao.getBrandAutoCompleteList(paramMap);
	}
	
	public List<Map<String, Object>> getProductAutoCompleteList(Map<String, Object> paramMap) throws SQLException {
		return reviewDao.getProductAutoCompleteList(paramMap);
	}
	
	public List<Map<String, Object>> getReviewpediaBrandProduct(Map<String, Object> paramMap) throws SQLException {
		return reviewDao.getReviewpediaBrandProduct(paramMap);
	}

	
	
	public String insertBrand(Map<String, Object> paramMap){
		return reviewDao.insertBrand(paramMap);
	}
	
	public String insertProduct(Map<String, Object> paramMap){
		return reviewDao.insertProduct(paramMap);
	}
	
	public String insertReviewpedia(Map<String, Object> paramMap){
		return reviewDao.insertReviewpedia(paramMap);
	}
	
	public String insertReviewpediaMemo(Map<String, Object> paramMap){
		return reviewDao.insertReviewpediaMemo(paramMap);
	}
	
	public void insertReviewpediaPNC1(Map<String, Object> paramMap){
		reviewDao.insertReviewpediaPNC1(paramMap);
	}
	
	public void insertReviewpediaPNC2(Map<String, Object> paramMap){
		reviewDao.insertReviewpediaPNC2(paramMap);
	}
	
	public List<Map<String, Object>> reviewpediaList(Map<String, Object> paramMap) throws SQLException {
		return reviewDao.reviewpediaList(paramMap);
	}
	
	public String reviewpediaListCnt(Map<String, Object> paramMap){
		return reviewDao.reviewpediaListCnt(paramMap);
	}
	
	public List<Map<String, Object>> getReviewpedia(Map<String, Object> paramMap) throws SQLException {
		return reviewDao.getReviewpedia(paramMap);
	}
	
	
	
	public void writeReviewpediaMemo(Map<String, Object> paramMap){
		reviewDao.writeReviewpediaMemo(paramMap);
	}
	
	public void updateReviewpediaMemo(Map<String, Object> paramMap){
		reviewDao.updateReviewpediaMemo(paramMap);
	}
	
	public void deleteReviewpediaMemo(Map<String, Object> paramMap){
		reviewDao.deleteReviewpediaMemo(paramMap);
	}
	
	public void writeReviewpediaPNC(Map<String, Object> paramMap){
		reviewDao.writeReviewpediaPNC(paramMap);
	}
	
	public void updateReviewpediaPNC(Map<String, Object> paramMap){
		reviewDao.updateReviewpediaPNC(paramMap);
	}
	
	public void deleteReviewpediaPNC(Map<String, Object> paramMap){
		reviewDao.deleteReviewpediaPNC(paramMap);
	}
	
	public void insertReviewpediaGood(Map<String, Object> paramMap){
		reviewDao.insertReviewpediaGood(paramMap);
	}
	
	public void deleteReviewpediaGood(Map<String, Object> paramMap){
		reviewDao.deleteReviewpediaGood(paramMap);
	}
	
	public List<Map<String, Object>> reviewpediaPNCList(Map<String, Object> paramMap) throws SQLException {
		return reviewDao.reviewpediaPNCList(paramMap);
	}
	
	
	public String reviewpediaPNCListCnt(Map<String, Object> paramMap){
		return reviewDao.reviewpediaPNCListCnt(paramMap);
	}
	
	
	public void insertReviewpediaPNCGood(Map<String, Object> paramMap){
		reviewDao.insertReviewpediaPNCGood(paramMap);
	}
	
	public void deleteReviewpediaPNCGood(Map<String, Object> paramMap){
		reviewDao.deleteReviewpediaPNCGood(paramMap);
	}
	
	public List<Map<String, Object>> getHotProductList(Map<String, Object> paramMap) throws SQLException {
		return reviewDao.getHotProductList(paramMap);
	}
	
	public List<Map<String, Object>> getHotProductList2(Map<String, Object> paramMap) throws SQLException {
		return reviewDao.getHotProductList2(paramMap);
	}
	
	public List<Map<String, Object>> getReplyList(Map<String, Object> paramMap) throws SQLException {
		return reviewDao.getReplyList(paramMap);
	}

	public void insertReply(Map<String, Object> paramMap){
		reviewDao.insertReply(paramMap);
	}
	
	public void updateReply(Map<String, Object> paramMap){
		reviewDao.updateReply(paramMap);
	}
	
	public void deleteReply(Map<String, Object> paramMap){
		reviewDao.deleteReply(paramMap);
	}
	
	public String insertReview(Map<String, Object> paramMap){
		return reviewDao.insertReview(paramMap);
	}
	
	public void insertReviewHashTag(Map<String, Object> paramMap){
		reviewDao.insertReviewHashTag(paramMap);
	}
	
	public void insertReviewImg(Map<String, Object> paramMap){
		reviewDao.insertReviewImg(paramMap);
	}
	

	public Map<String, Object> getReviewDetail(Map<String, Object> paramMap) throws SQLException {
		return reviewDao.getReviewDetail(paramMap);
	}

	public void deleteReview(Map<String, Object> paramMap){
		reviewDao.deleteReview(paramMap);
	}
	
	public void updateReview(Map<String, Object> paramMap){
		reviewDao.updateReview(paramMap);
	}
	
	public void deleteReviewHashTag(Map<String, Object> paramMap){
		reviewDao.deleteReviewHashTag(paramMap);
	}
	
	public void deleteReviewImg(Map<String, Object> paramMap){
		reviewDao.deleteReviewImg(paramMap);
	}
	
	public String getReviewpediaNo(Map<String, Object> paramMap){
		return reviewDao.getReviewpediaNo(paramMap);
	}
	
	public String getBrandNo(Map<String, Object> paramMap){
		return reviewDao.getBrandNo(paramMap);
	}
	
	public String getProductNo(Map<String, Object> paramMap){
		return reviewDao.getProductNo(paramMap);
	}
	
	

	public String getCategoryNo(Map<String, Object> paramMap){
		return reviewDao.getCategoryNo(paramMap);
	}
	
	
	
}
