package com.angryview.review.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import sun.util.logging.resources.logging; 

public interface ReviewService {
	
	public List<Map<String, Object>> reqCategoryList(Map<String, Object> paramMap) throws SQLException;
	public List<Map<String, Object>> getBrandAutoCompleteList(Map<String, Object> paramMap) throws SQLException;
	public List<Map<String, Object>> getProductAutoCompleteList(Map<String, Object> paramMap) throws SQLException;
	public List<Map<String, Object>> getReviewpediaBrandProduct(Map<String, Object> paramMap) throws SQLException;
	public String insertBrand(Map<String, Object> paramMap) throws SQLException;
	public String insertProduct(Map<String, Object> paramMap) throws SQLException;
	public String insertReviewpedia(Map<String, Object> paramMap) throws SQLException;
	public String insertReviewpediaMemo(Map<String, Object> paramMap) throws SQLException;
	public void insertReviewpediaPNC1(Map<String, Object> paramMap) throws SQLException;
	public void insertReviewpediaPNC2(Map<String, Object> paramMap) throws SQLException;
	public List<Map<String, Object>> reviewpediaList(Map<String, Object> paramMap) throws SQLException;
	public String reviewpediaListCnt(Map<String, Object> paramMap) throws SQLException;

	public List<Map<String, Object>> getReviewpedia(Map<String, Object> paramMap) throws SQLException;

	public void writeReviewpediaMemo(Map<String, Object> paramMap) throws SQLException;
	public void updateReviewpediaMemo(Map<String, Object> paramMap) throws SQLException;
	public void deleteReviewpediaMemo(Map<String, Object> paramMap) throws SQLException;
	public void writeReviewpediaPNC(Map<String, Object> paramMap) throws SQLException;
	public void updateReviewpediaPNC(Map<String, Object> paramMap) throws SQLException;
	public void deleteReviewpediaPNC(Map<String, Object> paramMap) throws SQLException;
	public void insertReviewpediaGood(Map<String, Object> paramMap) throws SQLException;
	public void deleteReviewpediaGood(Map<String, Object> paramMap) throws SQLException;
	public List<Map<String, Object>> reviewpediaPNCList (Map<String, Object> paramMap) throws SQLException;
	public String reviewpediaPNCListCnt(Map<String, Object> paramMap) throws SQLException;
	
	public void insertReviewpediaPNCGood(Map<String, Object> paramMap) throws SQLException;
	public void deleteReviewpediaPNCGood(Map<String, Object> paramMap) throws SQLException;
	public List<Map<String, Object>> getHotProductList(Map<String, Object> paramMap) throws SQLException;
	public List<Map<String, Object>> getHotProductList2(Map<String, Object> paramMap) throws SQLException;
	public List<Map<String, Object>> getReplyList(Map<String, Object> paramMap) throws SQLException;
	public void insertReply(Map<String, Object> paramMap) throws SQLException;
	public void updateReply(Map<String, Object> paramMap) throws SQLException;
	public void deleteReply(Map<String, Object> paramMap) throws SQLException;
	public String insertReview(Map<String, Object> paramMap) throws SQLException;
	public void insertReviewHashTag(Map<String, Object> paramMap) throws SQLException;
	public void insertReviewImg(Map<String, Object> paramMap) throws SQLException;
	public Map<String, Object> getReviewDetail(Map<String, Object> paramMap) throws SQLException;
	public void deleteReview(Map<String, Object> paramMap) throws SQLException;
	public void updateReview(Map<String, Object> paramMap) throws SQLException;
	public void deleteReviewHashTag(Map<String, Object> paramMap) throws SQLException;
	public void deleteReviewImg(Map<String, Object> paramMap) throws SQLException;
	public String getReviewpediaNo(Map<String, Object> paramMap) throws SQLException;
	public String getBrandNo(Map<String, Object> paramMap) throws SQLException;
	public String getProductNo(Map<String, Object> paramMap) throws SQLException;

	public String getCategoryNo(Map<String, Object> paramMap) throws SQLException;

	
	
	
}
