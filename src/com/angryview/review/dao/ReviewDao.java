package com.angryview.review.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.angryview.common.iBatisDaoSupport;

@Repository("reviewDao")
public class ReviewDao extends iBatisDaoSupport {
	
	public List<Map<String, Object>> reqCategoryList(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("reqCategoryList", paramMap);
	}
	
	public List<Map<String, Object>> getBrandAutoCompleteList(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getBrandAutoCompleteList", paramMap);
	}
	
	public List<Map<String, Object>> getProductAutoCompleteList(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getProductAutoCompleteList", paramMap);
	}
	
	public List<Map<String, Object>> getReviewpediaBrandProduct(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getReviewpediaBrandProduct", paramMap);
	}

	public String insertBrand(Map<String, Object> paramMap){
		return (String) insert("insertBrand", paramMap);
	}
	
	public String insertProduct(Map<String, Object> paramMap){
		return (String) insert("insertProduct", paramMap);
	}
	
	public String insertReviewpedia(Map<String, Object> paramMap){
		return (String) insert("insertReviewpedia", paramMap);
	}
	
	public String insertReviewpediaMemo(Map<String, Object> paramMap){
		return (String) insert("insertReviewpediaMemo", paramMap);
	}
	
	public void insertReviewpediaPNC1(Map<String, Object> paramMap){
		insert("insertReviewpediaPNC1", paramMap);
	}
	
	public void insertReviewpediaPNC2(Map<String, Object> paramMap){
		insert("insertReviewpediaPNC2", paramMap);
	}
	
	public List<Map<String, Object>> reviewpediaList(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("reviewpediaList", paramMap);
	}
	
	public String reviewpediaListCnt(Map<String, Object> paramMap) {
		return (String)select("reviewpediaListCnt", paramMap);
	}
	
	public List<Map<String, Object>> getReviewpedia(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getReviewpedia", paramMap);
	}
	
	
	
	
	public void writeReviewpediaMemo(Map<String, Object> paramMap){
		insert("writeReviewpediaMemo", paramMap);
	}
	
	public void updateReviewpediaMemo(Map<String, Object> paramMap){
		update("updateReviewpediaMemo", paramMap);
	}

	public void deleteReviewpediaMemo(Map<String, Object> paramMap){
		delete("deleteReviewpediaMemo", paramMap);
	}
	
	public void writeReviewpediaPNC(Map<String, Object> paramMap){
		insert("writeReviewpediaPNC", paramMap);
	}
	
	public void updateReviewpediaPNC(Map<String, Object> paramMap){
		update("updateReviewpediaPNC", paramMap);
	}

	public void deleteReviewpediaPNC(Map<String, Object> paramMap){
		delete("deleteReviewpediaPNC", paramMap);
	}
	
	public void insertReviewpediaGood(Map<String, Object> paramMap){
		insert("insertReviewpediaGood", paramMap);
	}
	
	public void deleteReviewpediaGood(Map<String, Object> paramMap){
		delete("deleteReviewpediaGood", paramMap);
	}

	public List<Map<String, Object>> reviewpediaPNCList(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("reviewpediaPNCList", paramMap);
	}
	
	public String reviewpediaPNCListCnt(Map<String, Object> paramMap) {
		return (String)select("reviewpediaPNCListCnt", paramMap);
	}
	
	
	public void insertReviewpediaPNCGood(Map<String, Object> paramMap){
		insert("insertReviewpediaPNCGood", paramMap);
	}
	
	public void deleteReviewpediaPNCGood(Map<String, Object> paramMap){
		delete("deleteReviewpediaPNCGood", paramMap);
	}
	
	public List<Map<String, Object>> getHotProductList(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getHotProductList", paramMap);
	}
	
	public List<Map<String, Object>> getHotProductList2(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getHotProductList2", paramMap);
	}
	
	public List<Map<String, Object>> getReplyList(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getReplyList", paramMap);
	}
	
	public void insertReply(Map<String, Object> paramMap){
		insert("insertReply", paramMap);
	}
	
	public void updateReply(Map<String, Object> paramMap){
		update("updateReply", paramMap);
	}

	public void deleteReply(Map<String, Object> paramMap){
		update("deleteReply", paramMap);
	}

	public String insertReview(Map<String, Object> paramMap){
		return (String) insert("insertReview", paramMap);
	}
	
	public void insertReviewHashTag(Map<String, Object> paramMap){
		insert("insertReviewHashTag", paramMap);
	}
	
	public void insertReviewImg(Map<String, Object> paramMap){
		insert("insertReviewImg", paramMap);
	}
	
	public Map<String, Object> getReviewDetail(Map<String, Object> paramMap){
		return (Map<String, Object>)select("getReviewDetail", paramMap);
	}
	
	public void deleteReview(Map<String, Object> paramMap){
		update("deleteReview", paramMap);
	}
	
	public void updateReview(Map<String, Object> paramMap){
		update("updateReview", paramMap);
	}
	
	public void deleteReviewHashTag(Map<String, Object> paramMap){
		delete("deleteReviewHashTag", paramMap);
	}
	
	public void deleteReviewImg(Map<String, Object> paramMap){
		delete("deleteReviewImg", paramMap);
	}
	
	public String getReviewpediaNo(Map<String, Object> paramMap){
		return (String) select("getReviewpediaNo", paramMap);
	}
	
	public String getBrandNo(Map<String, Object> paramMap){
		return (String) select("getBrandNo", paramMap);
	}
	
	public String getProductNo(Map<String, Object> paramMap){
		return (String) select("getProductNo", paramMap);
	}
	
	
	public String getCategoryNo(Map<String, Object> paramMap){
		return (String) select("getCategoryNo", paramMap);
	}
	
	
	
	
}
