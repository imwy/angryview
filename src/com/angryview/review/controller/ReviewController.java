package com.angryview.review.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.FileSystemResource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.angryview.review.service.ReviewService;
import com.angryview.common.CommonConstants;
import com.angryview.util.HashUtil;


import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;




@Controller
@RequestMapping("/*/")
public class ReviewController {

	private Logger log = Logger.getLogger(this.getClass());
	
	@Resource(name = "reviewService")
	private ReviewService reviewService;

	
	/**
	 * 카테고리 목록 불러오기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/reqCategoryList.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String reqCategoryList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {

				
			itemList = reviewService.reqCategoryList(paramMap);
			
			paramMap.clear();
	
			if(itemList != null){
				resultCode = "0000";
				resultMessage = "성공";
				

				//log.info("itemList : " + itemList);
				model.addAttribute("itemList", itemList);
	
			}else{
				resultCode = "0001";
				resultMessage = "요청한 데이터 없음";
			}
	
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	/**
	 * 브랜드 자동완성 목록
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/getBrandAutoCompleteList.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getBrandAutoCompleteList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String id = request.getParameter("id") == null ? "" : request.getParameter("id").toString();
		String text = request.getParameter("text") == null ? "" : request.getParameter("text").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if("".equals(id) || "".equals(text) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				paramMap.put("id", id);
				paramMap.put("text", text);
				itemList = reviewService.getBrandAutoCompleteList(paramMap);
				
				paramMap.clear();
		
				if(itemList != null){
					resultCode = "0000";
					resultMessage = "성공";
					
	
					//log.info("itemList : " + itemList);
					model.addAttribute("itemList", itemList);
		
				}else{
					resultCode = "0001";
					resultMessage = "요청한 데이터 없음";
				}
			}

			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	/**
	 * 상품 자동완성 목록
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/getProductAutoCompleteList.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getProductAutoCompleteList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String brandNo = request.getParameter("brandNo") == null ? "" : request.getParameter("brandNo").toString();
		String text = request.getParameter("text") == null ? "" : request.getParameter("text").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if("".equals(brandNo) || "".equals(text) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				paramMap.put("brandNo", brandNo);
				paramMap.put("text", text);
				itemList = reviewService.getProductAutoCompleteList(paramMap);
				
				paramMap.clear();
		
				if(itemList != null){
					resultCode = "0000";
					resultMessage = "성공";
					
	
					//log.info("itemList : " + itemList);
					model.addAttribute("itemList", itemList);
		
				}else{
					resultCode = "0001";
					resultMessage = "요청한 데이터 없음";
				}
			}

			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	
	

	/**
	 * 리뷰피디아 검색
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/getReviewpediaBrandProduct.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getReviewpediaBrandProduct(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{

		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String brandProduct = request.getParameter("brandProduct") == null ? "" : request.getParameter("brandProduct").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if("".equals(brandProduct) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				paramMap.put("brandProduct", brandProduct);
				
				itemList = reviewService.getReviewpediaBrandProduct(paramMap);
				
				paramMap.clear();
		
				if(itemList != null){
					resultCode = "0000";
					resultMessage = "성공";
					
	
					//log.info("itemList : " + itemList);
					model.addAttribute("itemList", itemList);
		
				}else{
					resultCode = "0001";
					resultMessage = "요청한 데이터 없음";
				}
			}

			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	

	/**
	 * 리뷰피디아 작성
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/writeReviewpedia.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String writeReviewpedia(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String categoryNo = request.getParameter("categoryNo") == null ? "" : request.getParameter("categoryNo").toString();
		String brandName = request.getParameter("brandName") == null ? "" : request.getParameter("brandName").toString();
		String productName = request.getParameter("productName") == null ? "" : request.getParameter("productName").toString();
		//String productCode = request.getParameter("productCode") == null ? "" : request.getParameter("productCode").toString();
		String memo = request.getParameter("memo") == null ? "" : request.getParameter("memo").toString();
		String pros = request.getParameter("pros") == null ? "" : request.getParameter("pros").toString();
		String cons = request.getParameter("cons") == null ? "" : request.getParameter("cons").toString();
		
		//신규 일수도 있음
		String brandNo = request.getParameter("brandNo") == null ? "" : request.getParameter("brandNo").toString();
		String productNo = request.getParameter("productNo") == null ? "" : request.getParameter("productNo").toString();

		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String reviewpediaNo = "";
		String reviewpediaMemoNo = "";

		
		try {
			if("".equals(memberID) || "".equals(categoryNo) || "".equals(brandName)
					|| "".equals(productName) 
					//|| "".equals(productCode) 
					//|| "".equals(memo)
					//|| "".equals(pros)|| "".equals(cons)
			) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
				

			}else {
				
				
				if("".equals(brandNo)) {
		        	
					//카테고리 + 브랜드명으로 brandNo 가져오기..
					paramMap.put("brandName", brandName);
					paramMap.put("categoryNo", categoryNo);
					brandNo = reviewService.getBrandNo(paramMap);
					
//					System.out.println("[brandNo]=["+brandNo+"]");

					if(brandNo==null) {
						//브랜드 새로 등록
						paramMap.put("brandName", brandName);
						paramMap.put("categoryNo", categoryNo);
						brandNo = reviewService.insertBrand(paramMap);
					}
				}
				   
//				System.out.println("brandNo=["+brandNo+"]");
				
				paramMap.put("productName", productName);
				paramMap.put("brandNo", brandNo);
				//리뷰피디아 reviewpediaNo 가져오기
				reviewpediaNo = reviewService.getReviewpediaNo(paramMap);
				
//				System.out.println("reviewpediaNo=["+reviewpediaNo+"]");
				
				//reviewpediaNo
				if( reviewpediaNo == null || "".equals(reviewpediaNo)) {

					if("".equals(productNo)) {
						
						//브랜드 + 상품명 + 상품코드로 productNo 가져오기..
						paramMap.put("brandNo", brandNo);
						paramMap.put("productName", productName);
						productNo = reviewService.getProductNo(paramMap);
						
//						System.out.println("[productNo]=["+productNo+"]");
						
						if(productNo==null) {
							//제품 새로 등록
							paramMap.put("brandNo", brandNo);
							paramMap.put("productName", productName);
							paramMap.put("productCode", "");
				
							productNo = reviewService.insertProduct(paramMap);
						}
						
					}
//					System.out.println("productNo=["+productNo+"]");
					
/*
					//제품 등록
					paramMap.put("productName", productName);
					paramMap.put("productCode", productCode);
					paramMap.put("brandNo", brandNo);
					if("".equals(productNo)) {
						productNo = reviewService.insertProduct(paramMap);
					}
					System.out.println("productNo=["+productNo+"]");
*/
					
					//리뷰피디아등록
					//paramMap.put("productNo", productNo);
					paramMap.put("productName", productName);
					paramMap.put("brandNo", brandNo);
					reviewpediaNo = reviewService.insertReviewpedia(paramMap);
					
					
					if(!"".equals(memo)) {
						//리뷰피디아 제품정보등록
						paramMap.put("memberID", memberID);
						paramMap.put("memo", memo);
						paramMap.put("reviewpediaNo", reviewpediaNo);
	
						reviewpediaMemoNo = reviewService.insertReviewpediaMemo(paramMap);
					}
					if(!"".equals(pros)) {
						//리뷰피디아 장점 등록
						paramMap.put("pros", pros);
						paramMap.put("goodFlag", "1");
						
						reviewService.insertReviewpediaPNC1(paramMap);
					}
					
					if(!"".equals(cons)) {
						//리뷰피디아 단점 등록
						paramMap.put("cons", cons);
						paramMap.put("goodFlag", "2");
						
						reviewService.insertReviewpediaPNC2(paramMap);
					}
					paramMap.clear();
			
					resultCode = "0000";
					resultMessage = "성공";

				}else {
					
//					System.out.println("이미등록된 리뷰피디아 -> 제품정보 + 장점 + 단점 등록");

					//리뷰피디아 제품정보등록
					paramMap.put("memberID", memberID);
					paramMap.put("memo", memo);
					paramMap.put("reviewpediaNo", reviewpediaNo);

					reviewpediaMemoNo = reviewService.insertReviewpediaMemo(paramMap);
					
					//리뷰피디아 장점 등록
					paramMap.put("pros", pros);
					paramMap.put("goodFlag", "1");
					
					reviewService.insertReviewpediaPNC1(paramMap);

					//리뷰피디아 단점 등록
					paramMap.put("cons", cons);
					paramMap.put("goodFlag", "2");
					reviewService.insertReviewpediaPNC2(paramMap);

					paramMap.clear();
			
					resultCode = "0000";
					resultMessage = "성공";
					
					

				}


			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	

	/**
	 * 리뷰피디아 제품정보 리스트
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/getReviewpediaList.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getReviewpediaList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		//String productNo = request.getParameter("productNo") == null ? "" : request.getParameter("productNo").toString();
		String reviewpediaNo = request.getParameter("reviewpediaNo") == null ? "" : request.getParameter("reviewpediaNo").toString();
		String lastRecvID = request.getParameter("lastRecvID") == null ? "" : request.getParameter("lastRecvID").toString();
		String reqCount = request.getParameter("reqCount") == null ? "" : request.getParameter("reqCount").toString();
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String itemListCnt = "0";


		try {
			if("".equals(reviewpediaNo) || "".equals(lastRecvID) || "".equals(reqCount)|| "".equals(memberID)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				
				if(lastRecvID.equals("1")) {
					resultCode = "0800";
					resultMessage = "마지막";
				}else {
					
					if(lastRecvID.equals("first")) {
						lastRecvID = "1";
					}else {
						lastRecvID = String.valueOf(Double.parseDouble(lastRecvID) + 1);
					}
				
					paramMap.put("reviewpediaNo", reviewpediaNo);
					paramMap.put("lastRecvID", lastRecvID);
					paramMap.put("reqCount", reqCount);
					paramMap.put("memberID", memberID);
					
					itemList = reviewService.reviewpediaList(paramMap);
					itemListCnt = reviewService.reviewpediaListCnt(paramMap);
					
					paramMap.clear();
			
					if(itemList != null){
						resultCode = "0000";
						resultMessage = "성공";
						
		
						//log.info("itemList : " + itemList);
						model.addAttribute("itemList", itemList);
						model.addAttribute("itemListCnt", itemListCnt);
			
					}else{
						resultCode = "0001";
						resultMessage = "요청한 데이터 없음";
					}
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	

	/**
	 * 리뷰피디아 
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/getReviewpedia.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getReviewpedia(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String reviewpediaNo = request.getParameter("reviewpediaNo") == null ? "" : request.getParameter("reviewpediaNo").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String itemListCnt = "0";


		try {
			if("".equals(reviewpediaNo)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {

				paramMap.put("reviewpediaNo", reviewpediaNo);

				itemList = reviewService.getReviewpedia(paramMap);
				
				paramMap.clear();
		
				if(itemList != null){
					resultCode = "0000";
					resultMessage = "성공";
					
					//log.info("itemList : " + itemList);
					model.addAttribute("itemList", itemList);
		
				}else{
					resultCode = "0001";
					resultMessage = "요청한 데이터 없음";
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	
	

	/**
	 * 리뷰피디아 제품정보 작성
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/writeReviewpediaMemo.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String writeReviewpediaMemo(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewpediaNo = request.getParameter("reviewpediaNo") == null ? "" : request.getParameter("reviewpediaNo").toString();
		String reviewpediaMemo = request.getParameter("reviewpediaMemo") == null ? "" : request.getParameter("reviewpediaMemo").toString();

		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		

		
		try {
			if("".equals(memberID) || "".equals(reviewpediaNo) || "".equals(reviewpediaMemo) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
				

			}else if("guest".equals(memberID)) {
				resultCode = "0101";
				resultMessage = "로그인 후 이용해 주세요.";
				
			}else {
				
				paramMap.put("memberID", memberID);
				paramMap.put("reviewpediaNo", reviewpediaNo);
				paramMap.put("reviewpediaMemo", reviewpediaMemo);
				reviewService.writeReviewpediaMemo(paramMap);

				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	

	/**
	 * 리뷰피디아 제품정보 수정
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/updateReviewpediaMemo.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String updateReviewpediaMemo(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewpediaMemo = request.getParameter("reviewpediaMemo") == null ? "" : request.getParameter("reviewpediaMemo").toString();
		String reviewpediaMemoNo = request.getParameter("reviewpediaMemoNo") == null ? "" : request.getParameter("reviewpediaMemoNo").toString();

		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		
		try { 
			if("".equals(memberID) || "".equals(reviewpediaMemo) || "".equals(reviewpediaMemoNo) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
				

			}else {
				
				paramMap.put("memberID", memberID);
				paramMap.put("reviewpediaMemo", reviewpediaMemo);
				paramMap.put("reviewpediaMemoNo", reviewpediaMemoNo);
				reviewService.updateReviewpediaMemo(paramMap);

				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	
	


	/**
	 * 리뷰피디아 제품정보 삭제
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/deleteReviewpediaMemo.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String deleteReviewpediaMemo(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewpediaMemoNo = request.getParameter("reviewpediaMemoNo") == null ? "" : request.getParameter("reviewpediaMemoNo").toString();

		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		
		try { 
			if("".equals(memberID) || "".equals(reviewpediaMemoNo) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
				

			}else {
				
				paramMap.put("memberID", memberID);
				paramMap.put("reviewpediaMemoNo", reviewpediaMemoNo);
				reviewService.deleteReviewpediaMemo(paramMap);

				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	


	/**
	 * 리뷰피디아 제품정보 좋아요/싫어요/취소
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/updateReviewpediaMemoGood.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String updateReviewpediaMemoGood(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewpediaMemoNo = request.getParameter("reviewpediaMemoNo") == null ? "" : request.getParameter("reviewpediaMemoNo").toString();
		String goodFlag = request.getParameter("goodFlag") == null ? "" : request.getParameter("goodFlag").toString();

		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String resultSql  = "";

		
		
		try {
			if("".equals(memberID) || "".equals(reviewpediaMemoNo) || "".equals(goodFlag) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
				

			}else {
				
				paramMap.put("memberID", memberID);
				paramMap.put("reviewpediaMemoNo", reviewpediaMemoNo);
				paramMap.put("goodFlag", goodFlag);
				
				
				if("1".equals(goodFlag) || "2".equals(goodFlag)) {
					//좋아요, 싫어요
					
					try {
						reviewService.insertReviewpediaGood(paramMap);
						
						resultCode = "0000";
						resultMessage = "성공";
						
				    } catch (DuplicateKeyException e) {
				       
						reviewService.deleteReviewpediaGood(paramMap);
						
						resultCode = "0000";
						resultMessage = "삭제성공";
				       
				    }
				}
			
				paramMap.clear();

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	


	/**
	 * 리뷰피디아 제품정보 리스트
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/getReviewpediaPNCList.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getReviewpediaPNCList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String reviewpediaNo = request.getParameter("reviewpediaNo") == null ? "" : request.getParameter("reviewpediaNo").toString();
		String lastRecvID = request.getParameter("lastRecvID") == null ? "" : request.getParameter("lastRecvID").toString();
		String reqCount = request.getParameter("reqCount") == null ? "" : request.getParameter("reqCount").toString();
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String goodFlag = request.getParameter("goodFlag") == null ? "" : request.getParameter("goodFlag").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";
		String itemListCnt = "0";


		try {
			if("".equals(reviewpediaNo) || "".equals(lastRecvID) || "".equals(reqCount)|| "".equals(memberID)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				
				if(lastRecvID.equals("1")) {
					resultCode = "0800";
					resultMessage = "마지막";
				}else {
					
					if(lastRecvID.equals("first")) {
						lastRecvID = "1";
					}else {
						lastRecvID = String.valueOf(Double.parseDouble(lastRecvID) + 1);
					}
				
					paramMap.put("reviewpediaNo", reviewpediaNo);
					paramMap.put("lastRecvID", lastRecvID);
					paramMap.put("reqCount", reqCount);
					paramMap.put("memberID", memberID);
					paramMap.put("goodFlag", goodFlag);
					
					itemList = reviewService.reviewpediaPNCList(paramMap);
					
					itemListCnt = reviewService.reviewpediaPNCListCnt(paramMap);
					
					paramMap.clear();
			
					if(itemList != null){
						resultCode = "0000";
						resultMessage = "성공";
						
		
						//log.info("itemList : " + itemList);
						model.addAttribute("itemList", itemList);
						model.addAttribute("itemListCnt", itemListCnt);
			
					}else{
						resultCode = "0001";
						resultMessage = "요청한 데이터 없음";
					}
				}
			}

			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	


	/**
	 * 리뷰피디아 장/단점 작성
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/writeReviewpediaPNC.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String writeReviewpediaPNC(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewpediaNo = request.getParameter("reviewpediaNo") == null ? "" : request.getParameter("reviewpediaNo").toString();
		String reviewpediaPNCMemo = request.getParameter("reviewpediaPNCMemo") == null ? "" : request.getParameter("reviewpediaPNCMemo").toString();
		String reviewpediaPNCGood = request.getParameter("reviewpediaPNCGood") == null ? "" : request.getParameter("reviewpediaPNCGood").toString();

		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		

		
		try {
			if("".equals(memberID) || "".equals(reviewpediaNo) || "".equals(reviewpediaPNCMemo) || "".equals(reviewpediaPNCGood) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
				

			}else if("guest".equals(memberID)) {
				resultCode = "0101";
				resultMessage = "로그인 후 이용해 주세요.";
				
			}else {
				
				paramMap.put("memberID", memberID);
				paramMap.put("reviewpediaNo", reviewpediaNo);
				paramMap.put("reviewpediaPNCMemo", reviewpediaPNCMemo);
				paramMap.put("reviewpediaPNCGood", reviewpediaPNCGood);
				reviewService.writeReviewpediaPNC(paramMap);

				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	

	/**
	 * 리뷰피디아 장/단점  수정
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/updateReviewpediaPNC.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String updateReviewpediaPNC(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewpediaPNCNo = request.getParameter("reviewpediaPNCNo") == null ? "" : request.getParameter("reviewpediaPNCNo").toString();
		String reviewpediaPNCMemo = request.getParameter("reviewpediaPNCMemo") == null ? "" : request.getParameter("reviewpediaPNCMemo").toString();
		String reviewpediaPNCGood = request.getParameter("reviewpediaPNCGood") == null ? "" : request.getParameter("reviewpediaPNCGood").toString();
		String reviewpediaNo = request.getParameter("reviewpediaNo") == null ? "" : request.getParameter("reviewpediaNo").toString();

		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		
		try { 
			if("".equals(memberID) || "".equals(reviewpediaPNCNo) || "".equals(reviewpediaPNCMemo) 
					|| "".equals(reviewpediaPNCGood) || "".equals(reviewpediaNo) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
				

			}else {
				
				paramMap.put("memberID", memberID);
				paramMap.put("reviewpediaPNCNo", reviewpediaPNCNo);
				paramMap.put("reviewpediaPNCMemo", reviewpediaPNCMemo);
				paramMap.put("reviewpediaPNCGood", reviewpediaPNCGood);
				paramMap.put("reviewpediaNo", reviewpediaNo);
				reviewService.updateReviewpediaPNC(paramMap);

				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	


	/**
	 * 리뷰피디아 장/단점  삭제
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/deleteReviewpediaPNC.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String deleteReviewpediaPNC(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewpediaNo = request.getParameter("reviewpediaNo") == null ? "" : request.getParameter("reviewpediaNo").toString();
		String reviewpediaPNCGood = request.getParameter("reviewpediaPNCGood") == null ? "" : request.getParameter("reviewpediaPNCGood").toString();
		String reviewpediaPNCNo = request.getParameter("reviewpediaPNCNo") == null ? "" : request.getParameter("reviewpediaPNCNo").toString();

		
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		
		try { 
			if("".equals(memberID) || "".equals(reviewpediaNo) || "".equals(reviewpediaPNCGood) || "".equals(reviewpediaPNCNo) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
				

			}else {
				
				paramMap.put("memberID", memberID);
				paramMap.put("reviewpediaNo", reviewpediaNo);
				paramMap.put("reviewpediaPNCGood", reviewpediaPNCGood);
				paramMap.put("reviewpediaPNCNo", reviewpediaPNCNo);
				reviewService.deleteReviewpediaPNC(paramMap);

				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}

	/**
	 * 리뷰피디아 장단점 좋아요/싫어요/취소
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/updateReviewpediaPNCGood.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String updateReviewpediaPNCGood(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewpediaPNCNo = request.getParameter("reviewpediaPNCNo") == null ? "" : request.getParameter("reviewpediaPNCNo").toString();
		String goodFlag = request.getParameter("goodFlag") == null ? "" : request.getParameter("goodFlag").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String resultSql  = "";

		try {
			if("".equals(memberID) || "".equals(reviewpediaPNCNo) || "".equals(goodFlag)  ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {			
				paramMap.put("memberID", memberID);
				paramMap.put("reviewpediaPNCNo", reviewpediaPNCNo);
				paramMap.put("goodFlag", goodFlag);
				if("1".equals(goodFlag) || "2".equals(goodFlag)) {
					//좋아요, 싫어요
					try {
						reviewService.insertReviewpediaPNCGood(paramMap);
						resultCode = "0000";
						resultMessage = "성공";
				    } catch (DuplicateKeyException e) {
						reviewService.deleteReviewpediaPNCGood(paramMap);	
						resultCode = "0000";
						resultMessage = "삭제성공";
				    }
				}
				paramMap.clear();
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	

	/**
	 * 리뷰피디아 인기검색어
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/getHotProductList.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getHotProductList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		List<Map<String, Object>> categotyList = new ArrayList<Map<String, Object>>();
		
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			//인기검색어 나열하는 로직
			//1단계) 접속 유저 관심분야 선택 카테고리
			//2단계) 작성일 1개월 내 최신글 존재
			//3단계) 작성 유저 수
			
			paramMap.put("memberID", memberID);
			
			itemList = reviewService.getHotProductList(paramMap);
			
			
//			System.out.println("itemList"+ itemList);
	//		System.out.println("itemList.size()" + itemList.size());

			if(itemList.size() < 3){
				
				itemList = reviewService.getHotProductList2(paramMap);

			}

			
			paramMap.clear();
	
			if(itemList != null){
				resultCode = "0000";
				resultMessage = "성공";
				

				//log.info("itemList : " + itemList);
				model.addAttribute("itemList", itemList);
	
			}else{
				resultCode = "0001";
				resultMessage = "요청한 데이터 없음";
			}


			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	/**
	 * 댓글 답글 가져오기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/getReplyList.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getReplyList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewNo = request.getParameter("reviewNo") == null ? "" : request.getParameter("reviewNo").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String originNo = "0";

		try {
			if("".equals(memberID) || "".equals(reviewNo)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				List<Map<String, Object>> replyList = new ArrayList<Map<String, Object>>();
				
				paramMap.put("memberID", memberID);
				paramMap.put("reviewNo", reviewNo);
				paramMap.put("originNo", originNo);
				
				replyList = reviewService.getReplyList(paramMap);
				
				for(Map<String, Object> map : replyList) {
					String replyNo = (String)String.valueOf( map.get("replyNo"));
					getSubReplyList(map,paramMap, replyNo);
				}
				
				paramMap.clear();
		
				if(replyList != null){
					resultCode = "0000";
					resultMessage = "성공";
					
					//log.info("itemList : " + replyList);
					model.addAttribute("itemList", replyList);
		
				}else{
					resultCode = "0001";
					resultMessage = "요청한 데이터 없음";
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	void getSubReplyList(Map<String, Object> replyMap, Map<String, Object> paramMap,  String inReplyNo)
	throws Exception{
		

		Map<String, Object> pMap = new HashMap<String, Object>(paramMap);
		pMap.put("originNo", inReplyNo);

		List<Map<String, Object>> replyList = reviewService.getReplyList(pMap);
		replyMap.put("replyList", replyList);
		
		
		for(Map<String, Object> map : replyList) {
			String replyNo = (String)String.valueOf( map.get("replyNo"));
			getSubReplyList (map, paramMap, replyNo);
		}
		
		
	}
	
	
	/**
	 * 댓글 답글 쓰기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */

	@RequestMapping("review/writeReply.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String writeReply(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewNo = request.getParameter("reviewNo") == null ? "" : request.getParameter("reviewNo").toString();
		String memo = request.getParameter("memo") == null ? "" : request.getParameter("memo").toString();
		String originNo = request.getParameter("originNo") == null ? "" : request.getParameter("originNo").toString();
		String replyMemberNo = request.getParameter("replyMemberNo") == null ? "" : request.getParameter("replyMemberNo").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		try {
			if("".equals(memberID) || "".equals(reviewNo) || "".equals(memo) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
				
			}else {
				
				if("".equals(originNo)) {
					originNo = "0";
				}
					
				paramMap.put("memberID", memberID);
				paramMap.put("reviewNo", reviewNo);
				paramMap.put("memo", memo);
				paramMap.put("originNo", originNo);
				paramMap.put("replyMemberNo", replyMemberNo);
				
				reviewService.insertReply(paramMap);
				
				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	

	/**
	 * 댓글 답글 쓰기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception

	@RequestMapping("review/writeReply.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String writeReply(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewNo = request.getParameter("reviewNo") == null ? "" : request.getParameter("reviewNo").toString();
		String replyNo = request.getParameter("replyNo") == null ? "" : request.getParameter("replyNo").toString();
		String groupLayer = request.getParameter("groupLayer") == null ? "" : request.getParameter("groupLayer").toString();
		String memo = request.getParameter("memo") == null ? "" : request.getParameter("memo").toString();

		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		

		
		try {
			if("".equals(memberID) || "".equals(reviewNo) || "".equals(replyNo) || "".equals(groupLayer) || "".equals(memo) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
				

			}else {
				
				if("0".equals(groupLayer)) {
					//댓글추가 신규 replyNo = originNo / groupOrder 0 
					
					paramMap.put("memberID", memberID);
					paramMap.put("reviewNo", reviewNo);
					paramMap.put("groupLayer", groupLayer);
					paramMap.put("memo", memo);

					reviewService.insertNewReply(paramMap);
					
				}else if ("1".equals(groupLayer)) {
					//답글추가 replyNo->originNo / groupOrder + 1
					paramMap.put("memberID", memberID);
					paramMap.put("reviewNo", reviewNo);
					paramMap.put("replyNo", replyNo);
					paramMap.put("groupLayer", groupLayer);
					paramMap.put("memo", memo);
					reviewService.insertReply(paramMap);
				}
				
				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
*/	
	

	/**
	 * 댓글 답글 수정
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/updateReply.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String updateReply(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewNo = request.getParameter("reviewNo") == null ? "" : request.getParameter("reviewNo").toString();
		String replyNo = request.getParameter("replyNo") == null ? "" : request.getParameter("replyNo").toString();
		String memo = request.getParameter("memo") == null ? "" : request.getParameter("memo").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";

		try {
			if("".equals(memberID) || "".equals(reviewNo) || "".equals(replyNo) || "".equals(memo) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
				
			}else {
				
				paramMap.put("memberID", memberID);
				paramMap.put("reviewNo", reviewNo);
				paramMap.put("replyNo", replyNo);
				paramMap.put("memo", memo);
				reviewService.updateReply(paramMap);

				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	

	/**
	 * 댓글 답글 삭제 ->상태값 N
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/deleteReply.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String deleteReply(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewNo = request.getParameter("reviewNo") == null ? "" : request.getParameter("reviewNo").toString();
		String replyNo = request.getParameter("replyNo") == null ? "" : request.getParameter("replyNo").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";

		try {
			if("".equals(memberID) || "".equals(reviewNo) || "".equals(replyNo) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
				
			}else {
				
				paramMap.put("memberID", memberID);
				paramMap.put("reviewNo", reviewNo);
				paramMap.put("replyNo", replyNo);
				reviewService.deleteReply(paramMap);

				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}


/*
	@RequestMapping("review/writeMyReview.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String writeMyReview(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{

		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();
		
        // MultipartHttpServletRequest 생성 
        MultipartHttpServletRequest mhsr = (MultipartHttpServletRequest) request; 
        Iterator iter = mhsr.getFileNames(); 
        MultipartFile mfile = null; 
        String fieldName = ""; 
        List resultList = new ArrayList(); 
        

		//요청
		String categoryNo = request.getParameter("categoryNo") == null ? "" : request.getParameter("categoryNo").toString();
		String brandNo = request.getParameter("brandNo") == null ? "" : request.getParameter("brandNo").toString();
		String brandName = request.getParameter("brandName") == null ? "" : request.getParameter("brandName").toString();
		String productNo = request.getParameter("productNo") == null ? "" : request.getParameter("productNo").toString();
		String productName = request.getParameter("productNo") == null ? "" : request.getParameter("productNo").toString();
		String productCode = request.getParameter("productCode") == null ? "" : request.getParameter("productCode").toString();
		String reviewpediaNo = request.getParameter("reviewpediaNo") == null ? "" : request.getParameter("reviewpediaNo").toString();
		String contents = request.getParameter("contents") == null ? "" : request.getParameter("contents").toString();
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String hashTag = request.getParameter("hashTag") == null ? "" : request.getParameter("hashTag").toString();
       
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String path ="/home/ec2-user/server/tomcat7/webapps/angryview/r/resources/img";
		
				
		try {
			if("".equals(brandNo) || "".equals(productNo) || "".equals(contents) || "".equals(memberID) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				// 이미지 업로드
				try {

					// 디레토리가 없다면 생성
					File dir = new File(path);
					if (!dir.isDirectory()) {
						dir.mkdirs();
					}

					// 값이 나올때까지
					while (iter.hasNext()) {
						fieldName = (String) iter.next(); // 내용을 가져와서
						mfile = mhsr.getFile(fieldName);
						String origName;
						origName = new String(mfile.getOriginalFilename().getBytes("8859_1"), "UTF-8"); // 한글꺠짐 방지

						System.out.println("origName: " + origName);
						// 파일명이 없다면
						if ("".equals(origName)) {
							continue;
						}

						// 파일 명 변경(uuid로 암호화)
						// String ext = origName.substring(origName.lastIndexOf('.')); // 확장자
						// String saveFileName = getUuid() + ext;
						String saveFileName = origName;

						System.out.println("saveFileName : " + saveFileName);

						// 설정한 path에 파일저장
						File serverFile = new File(path + File.separator + saveFileName);
						mfile.transferTo(serverFile);

						Map file = new HashMap();
						file.put("origName", origName);
						file.put("sfile", serverFile);
						resultList.add(file);
					}

				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				if("".equals(brandNo)) {
					//브랜드 새로 등록
					paramMap.put("brandName", brandName);
					paramMap.put("categoryNo", categoryNo);

					brandNo = reviewService.insertBrand(paramMap);
				}
				System.out.println("brandNo=["+brandNo+"]");

				if("".equals(productNo)) {
					//제품 새로 등록
					paramMap.put("brandNo", brandNo);
					paramMap.put("productName", productName);
					paramMap.put("productCode", productCode);

					productNo = reviewService.insertProduct(paramMap);
					
				}
				System.out.println("productNo=["+productNo+"]");
			
				paramMap.put("brandNo", brandNo);
				paramMap.put("productNo", productNo);
				paramMap.put("reviewpediaNo", reviewpediaNo);
				paramMap.put("contents", contents);
				paramMap.put("memberID", memberID);
				
				reviewService.writeMyReview(paramMap);
				
				paramMap.put("hashTag", hashTag);

				reviewService.insertHashTag(paramMap);

		
				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
    }
	*/
	

	@RequestMapping("review/writeMyReview.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String writeMyReview(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{

		
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		MultipartHttpServletRequest mhsr = (MultipartHttpServletRequest) request; 
        List<MultipartFile> fileList = mhsr.getFiles("file");
        
		//요청
		String categoryNo = request.getParameter("categoryNo") == null ? "" : request.getParameter("categoryNo").toString();
		String brandNo = request.getParameter("brandNo") == null ? "" : request.getParameter("brandNo").toString();
		String brandName = request.getParameter("brandName") == null ? "" : request.getParameter("brandName").toString();
		String productNo = request.getParameter("productNo") == null ? "" : request.getParameter("productNo").toString();
		String productName = request.getParameter("productName") == null ? "" : request.getParameter("productName").toString();
		String productCode = request.getParameter("productCode") == null ? "" : request.getParameter("productCode").toString();
		//String reviewpediaNo = request.getParameter("reviewpediaNo") == null ? "" : request.getParameter("reviewpediaNo").toString();
		String contents = request.getParameter("contents") == null ? "" : request.getParameter("contents").toString();
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String hashTag = request.getParameter("hashTag") == null ? "" : request.getParameter("hashTag").toString();
		
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String reviewNo = "";
		String reviewpediaNo = "";

		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();
		
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String yyyymmddhhmmss = sdf.format(now);
               
        String path ="/home/ec2-user/server/tomcat7-angryview/webapps/angryview/r/resources/img/";
        int fileListSize = fileList.size();
                
//        System.out.println("fileList.size() = " + fileListSize );
        
        try {
			if("".equals(categoryNo) || "".equals(brandName) || "".equals(productName)
					//|| "".equals(productCode)
			|| "".equals(contents) || "".equals(memberID) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				

		        if("".equals(brandNo)) {
		        	
					//카테고리 + 브랜드명으로 brandNo 가져오기..
					paramMap.put("brandName", brandName);
					paramMap.put("categoryNo", categoryNo);
					brandNo = reviewService.getBrandNo(paramMap);
					
					/*
					System.out.println("[brandNo]=["+brandNo+"]");
					System.out.println(brandNo==null);
					System.out.println(brandNo!=null);
					System.out.println("".equals(brandNo));
					*/
					
					if(brandNo==null) {
						//브랜드 새로 등록
						paramMap.put("brandName", brandName);
						paramMap.put("categoryNo", categoryNo);
						brandNo = reviewService.insertBrand(paramMap);
					}
				}
			//	System.out.println("brandNo=["+brandNo+"]");
		
				if("".equals(productNo)) {
					
					//브랜드 + 상품명 + 상품코드로 productNo 가져오기..
					paramMap.put("brandNo", brandNo);
					paramMap.put("productName", productName);
					paramMap.put("productCode", productCode);
					productNo = reviewService.getProductNo(paramMap);
					
					/*
					System.out.println("[productNo]=["+productNo+"]");
					System.out.println(productNo==null);
					System.out.println(productNo!=null);
					System.out.println("".equals(productNo));
					 */
					
					if(productNo==null) {
						//제품 새로 등록
						paramMap.put("brandNo", brandNo);
						paramMap.put("productName", productName);
						paramMap.put("productCode", productCode);
			
						productNo = reviewService.insertProduct(paramMap);
					}
					
				}
//				System.out.println("productNo=["+productNo+"]");
				

				paramMap.put("productName", productName);
				paramMap.put("brandNo", brandNo);
				//리뷰피디아 reviewpediaNo 가져오기
				reviewpediaNo = reviewService.getReviewpediaNo(paramMap);
				
//				System.out.println("reviewpediaNo=["+reviewpediaNo+"]");
				
				//reviewpediaNo
				if( reviewpediaNo == null || "".equals(reviewpediaNo)) {
					reviewpediaNo = reviewService.insertReviewpedia(paramMap);
				}
				
			
				paramMap.put("brandNo", brandNo);
				paramMap.put("productNo", productNo);
				//paramMap.put("reviewpediaNo", reviewpediaNo);
				paramMap.put("contents", contents);
				paramMap.put("memberID", memberID);
				
				
				paramMap.put("reviewpediaNo", reviewpediaNo);

				reviewNo = reviewService.insertReview(paramMap);
				

				
				if(!"".equals(hashTag)) {
					paramMap.put("reviewNo", reviewNo);
					paramMap.put("hashTag", hashTag);
	
					reviewService.insertReviewHashTag(paramMap);
				}
				
				resultCode = "0000";
				resultMessage = "성공";
								
				//이미지 업로드
				int index = 1;
		        if(fileListSize < 6 && fileList != null && fileListSize > 0) {

		            // 디레토리가 없다면 생성 
		            File dir = new File(path); 
		            if (!dir.isDirectory()) { 
		                dir.mkdirs(); 
		            } 
		    		
		    		for (MultipartFile mf : fileList) {
		    			
		                String originalFileName =  new String(mf.getOriginalFilename().getBytes("8859_1"), "UTF-8"); //한글깨짐 방지 
		                long fileSize = mf.getSize(); // 파일 사이즈

		   //             System.out.println("originalFileName : " + originalFileName);
		     //           System.out.println("fileSize : " + fileSize);
		       //         System.out.println("path : " + path);
		                
		                if ("".equals(originalFileName)) {
		                    continue; 
		                } 
		                
		                String ext = originalFileName.substring(originalFileName.lastIndexOf('.')); // 확장자 
		                String saveFileName = sdf.format(now) + getRandomString() + ext;

//		                System.out.println("saveFileName : " + saveFileName);
		                
						paramMap.put("originalFileName", originalFileName);
						paramMap.put("path", path);
						paramMap.put("saveFileName", saveFileName);
						paramMap.put("index", index);


		                try {
		                    mf.transferTo(new File(path + saveFileName));
		                    
		    				reviewService.insertReviewImg(paramMap);
		    				
		    				resultCode = "0000";
		    				resultMessage = "성공";

		                } catch (IllegalStateException e) {
		                    // TODO Auto-generated catch block
		                    e.printStackTrace();
		                    
		    				resultCode = "0500";
		    				resultMessage = "실패.IllegalStateException";

		                } catch (IOException e) {
		                    // TODO Auto-generated catch block
		                    e.printStackTrace();
		                    
		    				resultCode = "0500";
		    				resultMessage = "실패.IOException";
		                }

		                index++;
		            }		
		            
		        }else {
//		        	System.out.println("파일 업로드 skip!!");
		        }


			}

		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";
		
		}
	}
	
	
	/**
	 * 리뷰 상세보기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/getReviewDetail.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getReviewDetail(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> itemMap = new HashMap<String, Object>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewNo = request.getParameter("reviewNo") == null ? "" : request.getParameter("reviewNo").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";


		try {
			if("".equals(reviewNo) || "".equals(memberID)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {


				paramMap.put("memberID", memberID);
				paramMap.put("reviewNo", reviewNo);

				itemMap = reviewService.getReviewDetail(paramMap);
				
				paramMap.clear();
		
				if(itemMap != null){
					resultCode = "0000";
					resultMessage = "성공";
					
	
					//log.info("itemMap : " + itemMap);
					model.addAttribute("itemMap", itemMap);
		
				}else{
					resultCode = "0001";
					resultMessage = "요청한 데이터 없음";
				}
				
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	


	/**
	 * 리뷰 삭제
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/deleteReview.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String deleteReview(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String reviewNo = request.getParameter("reviewNo") == null ? "" : request.getParameter("reviewNo").toString();

		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		
		try { 
			if("".equals(memberID) || "".equals(reviewNo) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
				

			}else {
				
				paramMap.put("memberID", memberID);
				paramMap.put("reviewNo", reviewNo);
				reviewService.deleteReview(paramMap);

				paramMap.clear();
		
				resultCode = "0000";
				resultMessage = "성공";

			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	
	
	

	@RequestMapping("review/updateReview.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String updateReview(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{

		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		MultipartHttpServletRequest mhsr = (MultipartHttpServletRequest) request; 
        List<MultipartFile> fileList = mhsr.getFiles("file");
        
		//요청
		String reviewNo = request.getParameter("reviewNo") == null ? "" : request.getParameter("reviewNo").toString();
		String contents = request.getParameter("contents") == null ? "" : request.getParameter("contents").toString();
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String hashTag = request.getParameter("hashTag") == null ? "" : request.getParameter("hashTag").toString();
		String imgStatus = request.getParameter("imgStatus") == null ? "" : request.getParameter("imgStatus").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		

		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();
		
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String yyyymmddhhmmss = sdf.format(now);
               
        String path ="/home/ec2-user/server/tomcat7-angryview/webapps/angryview/r/resources/img/";
        int fileListSize = fileList.size();
                
//        System.out.println("fileList.size() = " + fileListSize );
        
        try {
			if("".equals(reviewNo) || "".equals(contents) || "".equals(memberID)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				paramMap.put("memberID", memberID);

				paramMap.put("reviewNo", reviewNo);
				paramMap.put("contents", contents);
				reviewService.updateReview(paramMap);

				
				paramMap.put("hashTag", hashTag);
				
				reviewService.deleteReviewHashTag(paramMap);
				
				if(!"".equals(hashTag)){
					reviewService.insertReviewHashTag(paramMap);
				}
				
				resultCode = "0000";
				resultMessage = "성공";

				
				if("1".equals(imgStatus)) {
					
					reviewService.deleteReviewImg(paramMap);

				
					//이미지 업로드
					int index = 1;
			        if(fileListSize < 6 && fileList != null && fileListSize > 0) {
	
			            // 디레토리가 없다면 생성 
			            File dir = new File(path); 
			            if (!dir.isDirectory()) { 
			                dir.mkdirs(); 
			            } 
			    		
			    		for (MultipartFile mf : fileList) {
			    			
			                String originalFileName =  new String(mf.getOriginalFilename().getBytes("8859_1"), "UTF-8"); //한글깨짐 방지 
			                long fileSize = mf.getSize(); // 파일 사이즈
	
			//                System.out.println("originalFileName : " + originalFileName);
			 //               System.out.println("fileSize : " + fileSize);
			  //              System.out.println("path : " + path);
			                
			                if ("".equals(originalFileName)) {
			                    continue; 
			                } 
			                
			                String ext = originalFileName.substring(originalFileName.lastIndexOf('.')); // 확장자 
			                String saveFileName = sdf.format(now) + getRandomString() + ext;
	
//			                System.out.println("saveFileName : " + saveFileName);
			                
							paramMap.put("originalFileName", originalFileName);
							paramMap.put("path", path);
							paramMap.put("saveFileName", saveFileName);
							paramMap.put("index", index);
	
	
			                try {
			                    mf.transferTo(new File(path + saveFileName));
			                    
			    				reviewService.insertReviewImg(paramMap);
			    				
			    				resultCode = "0000";
			    				resultMessage = "성공";
	
			                } catch (IllegalStateException e) {
			                    // TODO Auto-generated catch block
			                    e.printStackTrace();
			                    
			    				resultCode = "0500";
			    				resultMessage = "실패.IllegalStateException";
	
			                } catch (IOException e) {
			                    // TODO Auto-generated catch block
			                    e.printStackTrace();
			                    
			    				resultCode = "0500";
			    				resultMessage = "실패.IOException";
			                }
	
			                index++;
			            }		
			            
			        }else {
//			        	System.out.println("파일 업로드 skip!!");
			        }
				}
			}

		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";
		
		}
	}
	

	

	@RequestMapping("review/fileUploadTest.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String fileUploadTest(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
        
		//파일이 저장될 path 설정 
        // String path = req.getSession().getServletContext().getRealPath("") + "\\resources";    // 웹프로젝트 경로 위치
        String path ="/home/ec2-user/server/tomcat7-angryview/webapps/angryview/r/resources/img";
        
//        System.out.println("path : " + path);
        

		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}

		
        // MultipartHttpServletRequest 생성 
        MultipartHttpServletRequest mhsr = (MultipartHttpServletRequest) request; 
        Iterator iter = mhsr.getFileNames(); 
        MultipartFile mfile = null; 
        String fieldName = ""; 
        List resultList = new ArrayList(); 
        
        Date now = new Date(); 
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss"); 
    //    System.out.println("현재시간  : " +sdf.format(now)); 
        
        String yyyymmddhhmmss = sdf.format(now);
        
     //   System.out.println("yyyymmddhhmmss  : " + yyyymmddhhmmss); 

        
        try {
            
            // 디레토리가 없다면 생성 
            File dir = new File(path); 
            if (!dir.isDirectory()) { 
                dir.mkdirs(); 
            } 
            
            // 값이 나올때까지 
            while (iter.hasNext()) { 
                fieldName = (String) iter.next(); // 내용을 가져와서 
                mfile = mhsr.getFile(fieldName); 
                String origName; 
                origName = new String(mfile.getOriginalFilename().getBytes("8859_1"), "UTF-8"); //한글꺠짐 방지 
                
//                System.out.println("origName: " + origName);
                // 파일명이 없다면 
                if ("".equals(origName)) {
                    continue; 
                } 
                
                // 파일 명 변경
                String ext = origName.substring(origName.lastIndexOf('.')); // 확장자 
                String saveFileName = sdf.format(now) + getRandomString() + ext;
                
//                System.out.println("saveFileName : " + saveFileName);
                
                // 설정한 path에 파일저장 
                File serverFile = new File(path + File.separator + saveFileName);
                mfile.transferTo(serverFile);
                
                Map file = new HashMap();
                file.put("origName", origName); 
                file.put("sfile", serverFile);
                resultList.add(file);
                
                
            }
            

            
            } catch (UnsupportedEncodingException e) { 
                // TODO Auto-generated catch block 
                e.printStackTrace(); 
            }catch (IllegalStateException e) { 
            	// TODO Auto-generated catch block 
                e.printStackTrace();
            } catch (IOException e) { 
            	// TODO Auto-generated catch block
                e.printStackTrace();
            }finally {
    			
    			model.addAttribute("files", resultList);
    			model.addAttribute("params", mhsr.getParameterMap());
    						
    			return "jsonView";

    		}
        
    }
	
	
	public static String getRandomString(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	
	
	/**
	 * 네이버 API 구현
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("review/reqNaver.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String reqNaver(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String req = request.getParameter("req") == null ? "" : request.getParameter("req").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";

		
		try {
			if("".equals(req)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				
				String clientId = "yWKoIE2vXapke0wKUErd"; //애플리케이션 클라이언트 아이디값"
		        String clientSecret = "cGPafTgSxC"; //애플리케이션 클라이언트 시크릿값"

		        String text = null;
		        try {
		            text = URLEncoder.encode(req, "UTF-8");
		        } catch (UnsupportedEncodingException e) {
		            throw new RuntimeException("검색어 인코딩 실패",e);
		        }

		        String apiURL = "https://openapi.naver.com/v1/search/shop?query=" + text;    // json 결과

		        Map<String, String> requestHeaders = new HashMap<>();
		        requestHeaders.put("X-Naver-Client-Id", clientId);
		        requestHeaders.put("X-Naver-Client-Secret", clientSecret);
		        String responseBody = get(apiURL,requestHeaders);

//		        System.out.println(responseBody);
		        
		        
		        try {
			        JSONParser jsonParser = new JSONParser();

					JSONObject jsonObject = (JSONObject)jsonParser.parse(responseBody);

//			        System.out.println("jsonObject"+jsonObject);
			        
			        JSONArray array = (JSONArray)jsonObject.get("items");

//			        System.out.println("array"+array);
			        
			        
//			        System.out.println("++++++++++++++++++++++++++++++++++");
//			        System.out.println("array.size()"+array.size());
/*
			        for(int i=0; i<array.size(); i++){
			        	
				        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<");

			            JSONObject row = (JSONObject)array.get(i);
			   
			            
			            String title = (String)row.get("title");
			            System.out.println(title);
			            
			            String link = (String)row.get("link");
			            System.out.println(link);
			            
			            String maker = (String)row.get("maker");
			            System.out.println(maker);
			            
			            String mallName = (String)row.get("mallName");
			            System.out.println(mallName);
			          
			            String brand = (String)row.get("brand");
			            System.out.println(brand);
			            
			            String category1 = (String)row.get("category1");
			            System.out.println(category1);
			            
			            String category2 = (String)row.get("category2");
			            System.out.println(category2);
			            
			            String category3 = (String)row.get("category3");
			            System.out.println(category3);
			            
			            String category4 = (String)row.get("category4");
			            System.out.println(category4);

			            
				        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>");

			       
			        }
*/
					model.addAttribute("naver", array);


				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


				if(itemList != null){
					resultCode = "0000";
					resultMessage = "성공";
					
	
					//log.info("itemList : " + itemList);
					model.addAttribute("itemList", itemList);
		
				}else{
					resultCode = "0001";
					resultMessage = "요청한 데이터 없음";
				}
			}

			
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	

		/**
		 * 제품 조회 (네이버 API 연동)
		 * @param request
		 * @param response
		 * @param model
		 * @throws Exception
		 */
		@RequestMapping("review/getNProductList.do")
		@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
		public String getNProductList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
			
			Enumeration em = request.getParameterNames();
			while (em.hasMoreElements()) {
				String key = (String) em.nextElement();
				log.info("key : " + key);
				log.info("value : " + request.getParameter(key));
			}
			
			List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

			//요청
			String req = request.getParameter("req") == null ? "" : request.getParameter("req").toString();

			//응답
			String resultCode     = "";
			String resultMessage  = "";

			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			Map<String, Object> itemMap = new HashMap<String, Object>();
			
			try {
				if("".equals(req)) {
					
					resultCode = "0101";
					resultMessage = "필수 파라미터 누락 ";
					
				}else {
					
					String clientId = "yWKoIE2vXapke0wKUErd"; //애플리케이션 클라이언트 아이디값"
			        String clientSecret = "cGPafTgSxC"; //애플리케이션 클라이언트 시크릿값"

			        String text = null;
			        
			        try {
			            text = URLEncoder.encode(req, "UTF-8");
			        } catch (UnsupportedEncodingException e) {
			            throw new RuntimeException("검색어 인코딩 실패", e);
			        }

			        String apiURL = "https://openapi.naver.com/v1/search/shop?query=" + text;    // json 결과

			        Map<String, String> requestHeaders = new HashMap<>();
			        requestHeaders.put("X-Naver-Client-Id", clientId);
			        requestHeaders.put("X-Naver-Client-Secret", clientSecret);
			        String responseBody = get(apiURL, requestHeaders);

//			        System.out.println("responseBody["+responseBody+"]");
			        try {
			  
			        	//JSONArray로 변환
				        JSONParser jsonParser = new JSONParser();
						JSONObject jsonObject = (JSONObject)jsonParser.parse(responseBody);
				        JSONArray array = (JSONArray)jsonObject.get("items");

				        if (array.size() > 0) {
					        //List로 변환
					        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
					        if( array != null ){
					            int jsonSize = array.size();
					            for( int i = 0; i < jsonSize; i++ ){
					                Map<String, Object> map = getMapFromJsonObject( ( JSONObject ) array.get(i) );
					                list.add( map );
					            }
					        }
					        
					        if( list != null ){
					            int listSize = list.size();
					            for( int i = 0; i < listSize; i++ ){
					            	String productName = (String) list.get(i).get("title");
					            	
					            	productName = productName.replace("<b>", "");
					            	productName = productName.replace("</b>", "");
					            	
					            	String categoryNo = "";
					            	String categoryName = "";
					            	
					            	categoryName = (String) list.get(i).get("category1");
					            	
					            	list.get(i).put("rownum", i);
					            	list.get(i).put("productName", productName);
					            	list.get(i).put("categoryName", categoryName);
					            	paramMap.put("categoryName", categoryName);
					            	categoryNo = reviewService.getCategoryNo(paramMap);
					            	list.get(i).put("categoryNo", categoryNo);
					            	
					            	String brand = (String) list.get(i).get("brand");
					            	
					            	if(brand.equals("")) {
					            		brand = (String) list.get(i).get("maker");
					            	}
					            	
					            	list.get(i).put("brand", brand);

					            	list.get(i).remove("mallName");
					            	list.get(i).remove("category1");
					            	list.get(i).remove("category2");
					            	list.get(i).remove("category3");
					            	list.get(i).remove("category4");
					            	list.get(i).remove("image");
					            	list.get(i).remove("productId");
					            	list.get(i).remove("link");
					            	list.get(i).remove("maker");
					            	list.get(i).remove("title");
					            	list.get(i).remove("lprice");
					            	list.get(i).remove("hprice");
					            	list.get(i).remove("productType");
					            					            	
					            }
					        }
	
					    	if(itemList != null){
								resultCode = "0000";
								resultMessage = "성공";
								
								//log.info("itemList : " + itemList);
								model.addAttribute("itemList", list);
					
							}else{
								resultCode = "0001";
								resultMessage = "요청한 데이터 없음";
							}
				        }else {
				        	resultCode = "0001";
							resultMessage = "요청한 데이터 없음";
				        }
	
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}catch(Exception e){
				resultCode = "0500";
				resultMessage = "관리자에게 문의하세요";
				e.printStackTrace();
			}finally {
				
				model.addAttribute("resultCode", resultCode);
				model.addAttribute("resultMessage", resultMessage);
							
				return "jsonView";

			}
		}
		
		
		/**
		 * 네이버 20개 불러와서 mallname 네이버만 내려주는 api
		 * @param request
		 * @param response
		 * @param model
		 * @throws Exception
		 */
		@RequestMapping("review/getNProductList20.do")
		@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
		public String getNProductList20(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
			
			Enumeration em = request.getParameterNames();
			while (em.hasMoreElements()) {
				String key = (String) em.nextElement();
				log.info("key : " + key);
				log.info("value : " + request.getParameter(key));
			}
			
			List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

			//요청
			String req = request.getParameter("req") == null ? "" : request.getParameter("req").toString();

			//응답
			String resultCode     = "";
			String resultMessage  = "";

			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			Map<String, Object> itemMap = new HashMap<String, Object>();
			
			try {
				if("".equals(req)) {
					
					resultCode = "0101";
					resultMessage = "필수 파라미터 누락 ";
					
				}else {
					
					String clientId = "yWKoIE2vXapke0wKUErd"; //애플리케이션 클라이언트 아이디값"
			        String clientSecret = "cGPafTgSxC"; //애플리케이션 클라이언트 시크릿값"

			        String text = null;
			        
			        try {
			            text = URLEncoder.encode(req, "UTF-8");
			        } catch (UnsupportedEncodingException e) {
			            throw new RuntimeException("검색어 인코딩 실패", e);
			        }

			        String apiURL = "https://openapi.naver.com/v1/search/shop?display=20&query=" + text;    // json 결과

			        Map<String, String> requestHeaders = new HashMap<>();
			        requestHeaders.put("X-Naver-Client-Id", clientId);
			        requestHeaders.put("X-Naver-Client-Secret", clientSecret);
			        String responseBody = get(apiURL, requestHeaders);

//			        System.out.println("responseBody["+responseBody+"]");
			        try {
			  
			        	//JSONArray로 변환
				        JSONParser jsonParser = new JSONParser();
						JSONObject jsonObject = (JSONObject)jsonParser.parse(responseBody);
				        JSONArray array = (JSONArray)jsonObject.get("items");

				        if (array.size() > 0) {
					        //List로 변환
					        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
					        if( array != null ){
					            int jsonSize = array.size();
					            for( int i = 0; i < jsonSize; i++ ){
					                Map<String, Object> map = getMapFromJsonObject( ( JSONObject ) array.get(i) );
					                list.add( map );
					            }
					        }
					        
					        if( list != null ){
					            int listSize = list.size();
					            for( int i = 0; i < listSize; i++ ){
					            	String productName = (String) list.get(i).get("title");
					            	
					            	productName = productName.replace("<b>", "");
					            	productName = productName.replace("</b>", "");
					            	
					            	String categoryNo = "";
					            	String categoryName = "";
					            	
					            	categoryName = (String) list.get(i).get("category1");
					            	
					            	list.get(i).put("rownum", i);
					            	list.get(i).put("productName", productName);
					            	list.get(i).put("categoryName", categoryName);
					            	paramMap.put("categoryName", categoryName);
					            	categoryNo = reviewService.getCategoryNo(paramMap);
					            	list.get(i).put("categoryNo", categoryNo);
					            	
					            	String brand = (String) list.get(i).get("brand");
					            	
					            	if(brand.equals("")) {
					            		brand = (String) list.get(i).get("maker");
					            	}
					            	
					            	list.get(i).put("brand", brand);

					            	//list.get(i).remove("mallName");
					            	list.get(i).remove("category1");
					            	list.get(i).remove("category2");
					            	list.get(i).remove("category3");
					            	list.get(i).remove("category4");
					            	list.get(i).remove("image");
					            	list.get(i).remove("productId");
					            	list.get(i).remove("link");
					            	list.get(i).remove("maker");
					            	list.get(i).remove("title");
					            	list.get(i).remove("lprice");
					            	list.get(i).remove("hprice");
					            	list.get(i).remove("productType");
					            	
					            	
					            	String mallName = (String) list.get(i).get("mallName");
					            	
					            					            	
					            }
					        }
	
					    	if(itemList != null){
								resultCode = "0000";
								resultMessage = "성공";
								
								//log.info("itemList : " + itemList);
								model.addAttribute("itemList", list);
					
							}else{
								resultCode = "0001";
								resultMessage = "요청한 데이터 없음";
							}
				        }else {
				        	resultCode = "0001";
							resultMessage = "요청한 데이터 없음";
				        }
	
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}catch(Exception e){
				resultCode = "0500";
				resultMessage = "관리자에게 문의하세요";
				e.printStackTrace();
			}finally {
				
				model.addAttribute("resultCode", resultCode);
				model.addAttribute("resultMessage", resultMessage);
							
				return "jsonView";

			}
		}

		
		


	    public static Map<String, Object> getMapFromJsonObject( JSONObject jsonObj ) {
	    	
	        Map<String, Object> map = null;
	        try {
	        	
	            map = new ObjectMapper().readValue(jsonObj.toJSONString(), Map.class) ;
	            
	        } catch (JsonParseException e) {
	            e.printStackTrace();
	        } catch (JsonMappingException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	 
	        return map;
	    }

	    

	    private static String get(String apiUrl, Map<String, String> requestHeaders){
	        HttpURLConnection con = connect(apiUrl);
	        try {
	            con.setRequestMethod("GET");
	            for(Map.Entry<String, String> header :requestHeaders.entrySet()) {
	                con.setRequestProperty(header.getKey(), header.getValue());
	            }

	            int responseCode = con.getResponseCode();
	            if (responseCode == HttpURLConnection.HTTP_OK) { // 정상 호출
	                return readBody(con.getInputStream());
	            } else { // 에러 발생
	                return readBody(con.getErrorStream());
	            }
	        } catch (IOException e) {
	            throw new RuntimeException("API 요청과 응답 실패", e);
	        } finally {
	            con.disconnect();
	        }
	    }

	    private static HttpURLConnection connect(String apiUrl){
	        try {
	            URL url = new URL(apiUrl);
	            return (HttpURLConnection)url.openConnection();
	        } catch (MalformedURLException e) {
	            throw new RuntimeException("API URL이 잘못되었습니다. : " + apiUrl, e);
	        } catch (IOException e) {
	            throw new RuntimeException("연결이 실패했습니다. : " + apiUrl, e);
	        }
	    }

	    private static String readBody(InputStream body){
	        InputStreamReader streamReader = new InputStreamReader(body);

	        try (BufferedReader lineReader = new BufferedReader(streamReader)) {
	            StringBuilder responseBody = new StringBuilder();

	            String line;
	            while ((line = lineReader.readLine()) != null) {
	                responseBody.append(line);
	            }

	            return responseBody.toString();
	        } catch (IOException e) {
	            throw new RuntimeException("API 응답을 읽는데 실패했습니다.", e);
	        }
	    }
	    
	    

		 public static void main(String[] args) {
		        String clientId = "yWKoIE2vXapke0wKUErd"; //애플리케이션 클라이언트 아이디값"
		        String clientSecret = "cGPafTgSxC"; //애플리케이션 클라이언트 시크릿값"

		        String text = null;
		        try {
		            text = URLEncoder.encode("나이키", "UTF-8");
		        } catch (UnsupportedEncodingException e) {
		            throw new RuntimeException("검색어 인코딩 실패",e);
		        }

		        String apiURL = "https://openapi.naver.com/v1/search/shop?query=" + text;    // json 결과

		        Map<String, String> requestHeaders = new HashMap<>();
		        requestHeaders.put("X-Naver-Client-Id", clientId);
		        requestHeaders.put("X-Naver-Client-Secret", clientSecret);
		        String responseBody = get(apiURL,requestHeaders);

//		        System.out.println(responseBody);
		        
		        
		        try {
			        JSONParser jsonParser = new JSONParser();

					JSONObject jsonObject = (JSONObject)jsonParser.parse(responseBody);

//			        System.out.println("jsonObject"+jsonObject);
			        
			        JSONArray array = (JSONArray)jsonObject.get("items");

//			        System.out.println("array"+array);
			        
			        
//			        System.out.println("++++++++++++++++++++++++++++++++++");
	//		        System.out.println("array.size()"+array.size());

			        for(int i=0; i<array.size(); i++){
			        	
//				        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<");

			            JSONObject row = (JSONObject)array.get(i);
			   
			            
			            String title = (String)row.get("title");
	//		            System.out.println(title);
			            
			            String link = (String)row.get("link");
			//            System.out.println(link);
			            
			            String maker = (String)row.get("maker");
			       //     System.out.println(maker);
			            
			            String mallName = (String)row.get("mallName");
			          //  System.out.println(mallName);
			          
			            String brand = (String)row.get("brand");
			        //    System.out.println(brand);
			            
			            String category1 = (String)row.get("category1");
			          //  System.out.println(category1);
			            
			            String category2 = (String)row.get("category2");
			          //  System.out.println(category2);
			            
			            String category3 = (String)row.get("category3");
			          //  System.out.println(category3);
			            
			            String category4 = (String)row.get("category4");
			          //  System.out.println(category4);

			            
//				        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>");

			       
			        }


				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        
		    }
	
}