package com.angryview.common;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class PreControllerInterceptor implements HandlerInterceptor {
	
	private Logger log = Logger.getLogger(this.getClass());
	
//	Controller 수행전
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {
		
		boolean flag = true;
		String method = request.getMethod();
		String uri= request.getRequestURI();
		String remoteIp = request.getRemoteAddr() == null ?"":request.getRemoteAddr();
		
		log.info("[Controller Info]");
		log.info("[Controller : "+uri+"]");
		log.info("[Method :"+method+"]");
		log.info("[remoteIp :"+remoteIp+"]");
	
//		Enumeration em = request.getParameterNames();
//		while (em.hasMoreElements()) {
//			String key = (String) em.nextElement();
//			log.info(key+" : " + request.getParameter(key).toString().trim());
//		}
		
		return flag;
	}

//	Controller 수행완료
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		log.info("||||||||||||||||||||||||||||||||||||| Controller 수행완료");

	}

//	View 완료
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		log.info("||||||||||||||||||||||||||||||||||||| View 완료");

	}

}
