package com.angryview.common;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.ibatis.sqlmap.client.SqlMapClient;

public abstract class iBatisDaoSupport extends SqlMapClientDaoSupport{
	  @Resource(name="sqlMapClient")
	  public void setSuperSqlMapClient(SqlMapClient sqlMapClient)
	  {
	    super.setSqlMapClient(sqlMapClient);
	  }
	 
	  public Object insert(String queryId, Object parameterObject)
	  {
	    return getSqlMapClientTemplate().insert(queryId, parameterObject);
	  }	 
	  
	  public int update(String queryId, Object parameterObject)
	  {
	    return getSqlMapClientTemplate().update(queryId, parameterObject);
	  }

	  public int delete(String queryId, Object parameterObject)
	  {
	    return getSqlMapClientTemplate().delete(queryId, parameterObject);
	  }

	  public Object select(String queryId, Object parameterObject)
	  {
	    return getSqlMapClientTemplate().queryForObject(queryId, 
	      parameterObject);
	  }

	  public List list(String queryId, Object parameterObject)
	  {
	    return getSqlMapClientTemplate().queryForList(queryId, parameterObject);
	  }
}
