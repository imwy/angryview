package com.angryview.common;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.angryview.util.PropertiesUtil;


/**
 * hostName 에 따라 설정이 변하는 값들을 세팅한다.
 * @author yjsong
 *
 */
public class ConfigProperties {
	
	private Logger log = Logger.getLogger(this.getClass());
	
	private String username; 
	private String password; 
	private String url;
	private String hostName;
	private String serverType;
	
	

	/* 클래스 로드 시 해당서버 hostName을 setting 해주고 프로퍼티들을 세팅해준다  */
	public  ConfigProperties() throws IOException {
		PropertiesUtil pu = new PropertiesUtil();
		String type = "";
		setHostName();
		//log.info("[ hostName ] : " + getHostName());

		
		ClassLoader cl;
		cl = Thread.currentThread().getContextClassLoader();
		if(cl == null){
			cl = ClassLoader.getSystemClassLoader();
		}
	
		type = "dev.";
		log.info(pu.get(type+"db.username").toString());
		log.info(pu.get(type+"db.password").toString());
		log.info(pu.get(type+"db.url").toString());
		
		System.out.println(pu.get(type+"db.username").toString());
		System.out.println(pu.get(type+"db.password").toString());
		System.out.println(pu.get(type+"db.url").toString());
		
		setUsername(pu.get(type+"db.username").toString());
		setPassword(pu.get(type+"db.password").toString());
		setUrl(pu.get(type+"db.url").toString());
		setServerType(type);
	}


	/**  getter,setter **/
	public String getHostName() {
		return hostName;
	}
	public void setHostName() {
		InetAddress address  = null;
		try {
			address = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.hostName = address.getCanonicalHostName();
	}
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public String getUrl() {
		return url;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public String getServerType() {
		return serverType;
	}
	public void setServerType(String serverType) {
		this.serverType = serverType;
	}
}
