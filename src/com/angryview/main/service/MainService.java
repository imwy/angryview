package com.angryview.main.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import sun.util.logging.resources.logging; 

public interface MainService {
	
	public List<Map<String, Object>> getConcernTopic(Map<String, Object> paramMap) throws SQLException;
	public String getConcernTopicYn(Map<String, Object> paramMap);
	public void deleteConcernTopic(Map<String, Object> paramMap);
	public void insertConcernTopic(Map<String, Object> paramMap);
	
	public String getSearchLike(Map<String, Object> paramMap) throws SQLException;
	public List<Map<String, Object>> getReviewList(Map<String, Object> paramMap) throws SQLException;
	public String getReviewListCnt(Map<String, Object> paramMap);

	public List<Map<String, Object>> getMyReviewpediaList(Map<String, Object> paramMap) throws SQLException;
	public String getMyReviewpediaListCnt(Map<String, Object> paramMap);
	
	public List<Map<String, Object>> getAngryViewList(Map<String, Object> paramMap) throws SQLException;
	public String getAngryViewListCnt(Map<String, Object> paramMap);
	
	public List<Map<String, Object>> getMyReplyList(Map<String, Object> paramMap) throws SQLException;
	public String getMyReplyListCnt(Map<String, Object> paramMap);

	public ArrayList<String> getCategoryNoList(Map<String, Object> paramMap) throws SQLException;	
	public String getCategoryNoListCnt(Map<String, Object> paramMap);

	public List<Map<String, Object>> getRecommendReviewList(Map<String, Object> paramMap) throws SQLException;
	public String getRecommendReviewListCnt(Map<String, Object> paramMap);

	
	
	
	
}
