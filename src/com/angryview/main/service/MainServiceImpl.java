package com.angryview.main.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.angryview.main.dao.MainDao;

@Service("mainService")
public class MainServiceImpl implements MainService{
	
	@Resource(name="mainDao")
    private MainDao mainDao;

	public List<Map<String, Object>> getConcernTopic(Map<String, Object> paramMap) throws SQLException {
		return mainDao.getConcernTopic(paramMap);
	}
	
	public String getConcernTopicYn(Map<String, Object> paramMap) {
		return mainDao.getConcernTopicYn(paramMap);
	}
	
	public void deleteConcernTopic(Map<String, Object> paramMap){
		mainDao.deleteConcernTopic(paramMap);
	}
	
	public void insertConcernTopic(Map<String, Object> paramMap){
		mainDao.insertConcernTopic(paramMap);
	}
	
	
	public String getSearchLike(Map<String, Object> paramMap){
		return mainDao.getSearchLike(paramMap);
	}
	
	public List<Map<String, Object>> getReviewList(Map<String, Object> paramMap) throws SQLException {
		return mainDao.getReviewList(paramMap);
	}
	
	public String getReviewListCnt(Map<String, Object> paramMap) {
		return mainDao.getReviewListCnt(paramMap);
	}
	
	public List<Map<String, Object>> getMyReviewpediaList(Map<String, Object> paramMap) throws SQLException {
		return mainDao.getMyReviewpediaList(paramMap);
	}
	
	public String getMyReviewpediaListCnt(Map<String, Object> paramMap) {
		return mainDao.getMyReviewpediaListCnt(paramMap);
	}

	
	public List<Map<String, Object>> getAngryViewList(Map<String, Object> paramMap) throws SQLException {
		return mainDao.getAngryViewList(paramMap);
	}
	
	public String getAngryViewListCnt(Map<String, Object> paramMap) {
		return mainDao.getAngryViewListCnt(paramMap);
	}
	
	
	public List<Map<String, Object>> getMyReplyList(Map<String, Object> paramMap) throws SQLException {
		return mainDao.getMyReplyList(paramMap);
	}
	
	public String getMyReplyListCnt(Map<String, Object> paramMap) {
		return mainDao.getMyReplyListCnt(paramMap);
	}
	
	public ArrayList<String> getCategoryNoList(Map<String, Object> paramMap) throws SQLException {
		return mainDao.getCategoryNoList(paramMap);
	}
	
	public String getCategoryNoListCnt(Map<String, Object> paramMap) {
		return mainDao.getCategoryNoListCnt(paramMap);
	}
	
	public List<Map<String, Object>> getRecommendReviewList(Map<String, Object> paramMap) throws SQLException {
		return mainDao.getRecommendReviewList(paramMap);
	}

	public String getRecommendReviewListCnt(Map<String, Object> paramMap) {
		return mainDao.getRecommendReviewListCnt(paramMap);
	}
	
}
