package com.angryview.main.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.angryview.common.iBatisDaoSupport;

@Repository("mainDao")
public class MainDao extends iBatisDaoSupport {
	
	public List<Map<String, Object>> getConcernTopic(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getConcernTopic", paramMap);
	}

	public String getConcernTopicYn(Map<String, Object> paramMap) {
		return (String)select("getConcernTopicYn", paramMap);
	}
	
	public void deleteConcernTopic(Map<String, Object> paramMap){
		delete("deleteConcernTopic", paramMap);
	}
	
	public void insertConcernTopic(Map<String, Object> paramMap){
		insert("insertConcernTopic", paramMap);
	}
	
	public String getSearchLike(Map<String, Object> paramMap){
		return (String)select("getSearchLike", paramMap);
	}
	
	public List<Map<String, Object>> getReviewList(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getReviewList", paramMap);
	}
	
	public String getReviewListCnt(Map<String, Object> paramMap) {
		return (String)select("getReviewListCnt", paramMap);
	}
	
	public List<Map<String, Object>> getMyReviewpediaList(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getMyReviewpediaList", paramMap);
	}

	public String getMyReviewpediaListCnt(Map<String, Object> paramMap) {
		return (String)select("getMyReviewpediaListCnt", paramMap);
	}
	
	public List<Map<String, Object>> getAngryViewList(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getAngryViewList", paramMap);
	}
	public String getAngryViewListCnt(Map<String, Object> paramMap) {
		return (String)select("getAngryViewListCnt", paramMap);
	}
	
	public List<Map<String, Object>> getMyReplyList(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getMyReplyList", paramMap);
	}
	
	public String getMyReplyListCnt(Map<String, Object> paramMap) {
		return (String)select("getMyReplyListCnt", paramMap);
	}
	
	public ArrayList<String> getCategoryNoList(Map<String, Object> paramMap){
		return (ArrayList<String>)list("getCategoryNoList", paramMap);
	}
	
	public String getCategoryNoListCnt(Map<String, Object> paramMap) {
		return (String)select("getCategoryNoListCnt", paramMap);
	}

	public List<Map<String, Object>> getRecommendReviewList(Map<String, Object> paramMap){
		return (List<Map<String, Object>>)list("getRecommendReviewList", paramMap);
	}
	
	public String getRecommendReviewListCnt(Map<String, Object> paramMap) {
		return (String)select("getRecommendReviewListCnt", paramMap);
	}
	
	
	
	
}

