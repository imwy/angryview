package com.angryview.main.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.angryview.main.service.MainService;
import com.angryview.util.HashUtil;


@Controller
@RequestMapping("/*/")
public class MainController {

	private Logger log = Logger.getLogger(this.getClass());
	
	@Resource(name = "mainService")
	private MainService mainService;

	
	/**
	 * 9.1 관심사 목록 불러오기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("main/getConcernTopic.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getConcernTopic(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> mainList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";
		String isSetConcernTopic = "";

		
		try {
			if("".equals(memberID)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				paramMap.put("memberNo", memberID);

				isSetConcernTopic = mainService.getConcernTopicYn(paramMap);
				
				//log.info("isSetConcernTopic : " + isSetConcernTopic);
				model.addAttribute("isSetConcernTopic", isSetConcernTopic);
		
				
				mainList = mainService.getConcernTopic(paramMap);
		
				paramMap.clear();
		
				if(mainList != null){
					resultCode = "0000";
					resultMessage = "성공";
	
					//log.info("mainList : " + mainList);
					model.addAttribute("itemList", mainList);
		
				}else{
					resultCode = "0001";
					resultMessage = "요청한 데이터 없음";
				}
			}
			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	

	/**
	 * 9.2 관심사 설정
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("main/setConcernTopic.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String setConcernTopic(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
	
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> boardList = new ArrayList<Map<String, Object>>();

		//요청
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();
		String isSetConcernTopic = request.getParameter("isSetConcernTopic") == null ? "" : request.getParameter("isSetConcernTopic").toString();
		String isCheckedId = request.getParameter("isCheckedId") == null ? "" : request.getParameter("isCheckedId").toString();
		
		//응답
		String resultCode     = "";
		String resultMessage  = "";

		try {
			if("".equals(memberID) || "".equals(isSetConcernTopic)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
			
				if("Y".equals(isSetConcernTopic)) {
					
					if("".equals(isCheckedId)) {
						resultCode = "0101";
						resultMessage = "필수 파라미터 누락 ";
					}else {
					
						StringTokenizer str = new StringTokenizer(isCheckedId, "-");
	
						int count;
						count = str.countTokens();
						
						paramMap.put("memberNo", memberID);
						mainService.deleteConcernTopic(paramMap);

						while (str.hasMoreTokens()) {
							paramMap.put("id", str.nextToken());
						
							mainService.insertConcernTopic(paramMap);
						}

						paramMap.clear();
				
						resultCode = "0000";
						resultMessage = "성공";
					}
				}
				
			}
			
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
		
	}
	


	/**
	 * 9.3 나의 앵그리뷰 보기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("main/getMyReviewList.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getMyReviewList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();
		
		String itemListCnt = "0";

		//요청
		String lastRecvID = request.getParameter("lastRecvID") == null ? "" : request.getParameter("lastRecvID").toString();
		String reqCount = request.getParameter("reqCount") == null ? "" : request.getParameter("reqCount").toString();
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";


		try {
			if("".equals(lastRecvID) || "".equals(reqCount)|| "".equals(memberID)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				
				if(lastRecvID.equals("1")) {
					resultCode = "0800";
					resultMessage = "마지막";
				}else {
					
					if(lastRecvID.equals("first")) {
						lastRecvID = "1";
					}else {
						lastRecvID = String.valueOf(Double.parseDouble(lastRecvID) + 1);
					}
										
					paramMap.put("lastRecvID", lastRecvID);
					paramMap.put("reqCount", reqCount);
					paramMap.put("memberID", memberID);
					itemList = mainService.getReviewList(paramMap);
					itemListCnt = mainService.getReviewListCnt(paramMap);
							
					model.addAttribute("itemListCnt", itemListCnt);
					
					paramMap.clear();
			
					if(itemList != null){
						resultCode = "0000";
						resultMessage = "성공";
						
		
						//log.info("itemList : " + itemList);
						model.addAttribute("itemList", itemList);
			
					}else{
						resultCode = "0001";
						resultMessage = "요청한 데이터 없음";
					}
				}
			}

			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	
	
	/**
	 * 9.4 나의 리뷰피디아 보기
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("main/getMyReviewpediaList.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getMyReviewpediaList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String lastRecvID = request.getParameter("lastRecvID") == null ? "" : request.getParameter("lastRecvID").toString();
		String reqCount = request.getParameter("reqCount") == null ? "" : request.getParameter("reqCount").toString();
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String itemListCnt = "0";


		try {
			if("".equals(lastRecvID) || "".equals(reqCount)|| "".equals(memberID)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				
				if(lastRecvID.equals("1")) {
					resultCode = "0800";
					resultMessage = "마지막";
				}else {
					
					if(lastRecvID.equals("first")) {
						lastRecvID = "1";
					}else {
						lastRecvID = String.valueOf(Double.parseDouble(lastRecvID) + 1);
					}
				
					paramMap.put("lastRecvID", lastRecvID);
					paramMap.put("reqCount", reqCount);
					paramMap.put("memberID", memberID);
					itemList = mainService.getMyReviewpediaList(paramMap);
					
					itemListCnt = mainService.getMyReviewpediaListCnt(paramMap);
					
					paramMap.clear();
			
					if(itemList != null){
						resultCode = "0000";
						resultMessage = "성공";
						
		
						log.info("itemList : " + itemList);
						model.addAttribute("itemList", itemList);
						model.addAttribute("itemListCnt", itemListCnt);
			
					}else{
						resultCode = "0001";
						resultMessage = "요청한 데이터 없음";
					}
				}
			}

			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	


	/**
	 * 9.5 앵그리뷰 검색
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("main/getAngryViewList.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getAngryViewList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String lastRecvID = request.getParameter("lastRecvID") == null ? "" : request.getParameter("lastRecvID").toString();
		String reqCount = request.getParameter("reqCount") == null ? "" : request.getParameter("reqCount").toString();
		String categoryNo = request.getParameter("categoryNo") == null ? "" : request.getParameter("categoryNo").toString();
		String productNameCode = request.getParameter("productNameCode") == null ? "" : request.getParameter("productNameCode").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String itemListCnt = "0";


		try {
			if("".equals(lastRecvID) || "".equals(reqCount)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				
				if(lastRecvID.equals("1")) {
					resultCode = "0800";
					resultMessage = "마지막";
				}else {
					
					
					if(lastRecvID.equals("first")) {
						lastRecvID = "1";
					}else {
						lastRecvID = String.valueOf(Double.parseDouble(lastRecvID) + 1);
					}

					paramMap.put("lastRecvID", lastRecvID);
					paramMap.put("reqCount", reqCount);
					paramMap.put("categoryNo", categoryNo);
					
					
					if(! "".equals(productNameCode)) {
						paramMap.put("search", productNameCode);
						String searchLike = mainService.getSearchLike(paramMap);
						//System.out.println("searchLike["+searchLike +"]");

						if(searchLike != null) {
							productNameCode = productNameCode + "|"+ searchLike;
							//System.out.println("brandProduct["+productNameCode +"]");
						}
					}
					
					paramMap.put("productNameCode", productNameCode);

					
					
					itemList = mainService.getAngryViewList(paramMap);
					
					itemListCnt = mainService.getAngryViewListCnt(paramMap);
					
					model.addAttribute("itemListCnt", itemListCnt);
										
					paramMap.clear();
			
					if(itemList != null){
						resultCode = "0000";
						resultMessage = "성공";
						
		
						//log.info("itemList : " + itemList);
						model.addAttribute("itemList", itemList);
			
					}else{
						resultCode = "0001";
						resultMessage = "요청한 데이터 없음";
					}
				}
			}

			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	

	
	/**
	 * 9.6 나의 댓글 보기 
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("main/getMyReplyList.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getMyReplyList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();

		//요청
		String lastRecvID = request.getParameter("lastRecvID") == null ? "" : request.getParameter("lastRecvID").toString();
		String reqCount = request.getParameter("reqCount") == null ? "" : request.getParameter("reqCount").toString();
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";
		
		String itemListCnt = "0";


		try {
			if("".equals(lastRecvID) || "".equals(reqCount)|| "".equals(memberID)) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				
				if(lastRecvID.equals("1")) {
					resultCode = "0800";
					resultMessage = "마지막";
				}else {
					
					if(lastRecvID.equals("first")) {
						lastRecvID = "1";
					}else {
						lastRecvID = String.valueOf(Double.parseDouble(lastRecvID) + 1);
					}
				
					paramMap.put("lastRecvID", lastRecvID);
					paramMap.put("reqCount", reqCount);
					paramMap.put("memberID", memberID);
					itemList = mainService.getMyReplyList(paramMap);
					
					itemListCnt = mainService.getMyReplyListCnt(paramMap);
					
					paramMap.clear();
			
					if(itemList != null){
						resultCode = "0000";
						resultMessage = "성공";
						
		
						//log.info("itemList : " + itemList);
						model.addAttribute("itemList", itemList);
						model.addAttribute("itemListCnt", itemListCnt);
			
					}else{
						resultCode = "0001";
						resultMessage = "요청한 데이터 없음";
					}
				}
			}

			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	


	/**
	 * 9.7 추천리뷰 리스트
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("main/getRecommendReviewList.do")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public String getRecommendReviewList(HttpServletRequest request,HttpServletResponse response, HttpSession session, Model model) throws Exception, SQLException{
		
		Enumeration em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = (String) em.nextElement();
			log.info("key : " + key);
			log.info("value : " + request.getParameter(key));
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();
		
		
		//List<Map<String, Object>> valueList = new ArrayList<Map<String, Object>>();
		ArrayList<String> valueList = new ArrayList<String>();


		//요청
		String lastRecvID = request.getParameter("lastRecvID") == null ? "" : request.getParameter("lastRecvID").toString();
		String reqCount = request.getParameter("reqCount") == null ? "" : request.getParameter("reqCount").toString();
		String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").toString();

		//응답
		String resultCode     = "";
		String resultMessage  = "";

		String itemListCnt = "0";

		try {
			if("".equals(lastRecvID) || "".equals(reqCount) || "".equals(memberID) ) {
				resultCode = "0101";
				resultMessage = "필수 파라미터 누락 ";
			}else {
				
				
				if(lastRecvID.equals("1")) {
					resultCode = "0800";
					resultMessage = "마지막";
				}else {
					
					if(lastRecvID.equals("first")) {
						lastRecvID = "1";
					}else {
						lastRecvID = String.valueOf(Double.parseDouble(lastRecvID) + 1);
					}
					
					paramMap.put("memberID", memberID);

					valueList = mainService.getCategoryNoList(paramMap);
					
					paramMap.put("lastRecvID", lastRecvID);
					paramMap.put("reqCount", reqCount);
					paramMap.put("valueList", valueList);
					
					itemList = mainService.getRecommendReviewList(paramMap);
					
					itemListCnt = mainService.getRecommendReviewListCnt(paramMap);
					
					//System.out.println("itemList="+itemList);
					
					paramMap.clear();
					
					//System.out.println("itemList="+itemList);
					//System.out.println("itemList.size="+itemList.size());

			
					if( (itemList != null) && (!"0".equals(itemList.size())) ){
						resultCode = "0000";
						resultMessage = "성공";
						
		
						//log.info("itemList : " + itemList);
						model.addAttribute("itemList", itemList);
						model.addAttribute("itemListCnt", itemListCnt);
						
						
			
					}else{
						resultCode = "0001";
						resultMessage = "요청한 데이터 없음";
					}
				}
			}

			
		}catch(SQLException se){
			resultCode = "0400";
			resultMessage = "알수없는 DB 에러";
			se.printStackTrace();
		}catch(Exception e){
			resultCode = "0500";
			resultMessage = "관리자에게 문의하세요";
			e.printStackTrace();
		}finally {
			
			model.addAttribute("resultCode", resultCode);
			model.addAttribute("resultMessage", resultMessage);
						
			return "jsonView";

		}
	}
	


	
	
}